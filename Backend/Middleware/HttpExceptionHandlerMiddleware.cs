﻿using Backend.Exceptions;
using Backend.Extensions.Serializer;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Backend.Middleware
{
    /// <summary>
    /// https://docs.microsoft.com/ru-ru/aspnet/core/fundamentals/middleware/extensibility
    /// </summary>
    public class HttpExceptionHandlerMiddleware : IMiddleware
    {
        private IHostingEnvironment _env { get; }
        private ILogger _logger { get; }

        public HttpExceptionHandlerMiddleware(IHostingEnvironment env, ILogger<HttpExceptionHandlerMiddleware> logger)
        {
            _env = env;
            _logger = logger;
        }

        private string LogMessage(Exception exception, string urlRoute, string method, int argsCount)
        {
            var stackTrace = string.IsNullOrEmpty(exception.StackTrace) ? /*Environment.StackTrace*/null : exception.StackTrace.Split(Environment.NewLine);
            var match = stackTrace?.Length > 0 ? Regex.Match(stackTrace[stackTrace.Length - 1], @"[^\s.]+\.?[^\s.]+\(") : null;
            method = match?.Success == true ? match.Value : method + "(";

            switch (argsCount)
            {
                case 0:
                    return method + ")";
                case 1:
                    return method + "{@Request})";
                case 2:
                    return method + "{@Request}, {@arg1})";
                case 3:
                    return method + "{@Request}, {@arg1}, {@arg2})";
                default:
                {
                    var names = new string[argsCount - 1];
                    for (int i = 1; i < names.Length; i++)
                    {
                        names[i] = "{@arg" + i + "}";
                    }
                    return method + "{@Request}, " + string.Join(", ", names) + ")";
                }
            }
        }

        // https://stackoverflow.com/a/38935583
        // https://docs.microsoft.com/ru-ru/aspnet/core/web-api/handle-errors
        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            try
            {
                await next(context);
            }
            catch (Exception exception)
            {
                var isProduction = _env.IsProduction();
                var urlRoute = context.Request.Path + context.Request.QueryString;

                var message = string.Empty;

                if (exception is HttpResponseException rEx)
                {
                    if (rEx.InnerException is IHttpException == false)
                    {
                        // логгируем только ненамеренные ошибки
                        _logger.Log(rEx.Level, rEx.InnerException, LogMessage(rEx.InnerException, urlRoute, rEx.Method, rEx.Arguments.Length), rEx.Arguments);
                    }
                    exception = rEx.InnerException;
                }

                // Если ошибка намеренная
                if (exception is IHttpException httpException)
                {
                    context.Response.StatusCode = httpException.StatusCode;
                    message = exception.Message?.Length > 0 ? exception.Message : httpException.DefaultMessage;
                }
                else // Ошибка работы кода
                {
                    _logger.LogCritical(exception, $"Unhandled error ({context.Request.Method}) {urlRoute}");

                    context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                    message = "Internal Server Error (500)";
                }

                context.Response.ContentType = "application/json";
                var json = JsonConvert.SerializeObject(
                    value: new
                    {
                        Message = message,
                        Details = new ProblemDetails() {
                            Type = exception.GetType().FullName,
                            Title = exception.Message,
                            Status = context.Response.StatusCode,
                            Detail = isProduction ? ShortStackTrace(exception) : exception.StackTrace,
                            Instance = urlRoute
                        },
                        Source = isProduction ? null : exception.Source,
                        Data = isProduction ? null : exception.Data,
                        HelpLink = exception.HelpLink
                    },
                    formatting: isProduction ? Formatting.None : Formatting.Indented,
                    settings: new JsonCamelCaseSerializerSettings() {
                        Formatting = isProduction ? Formatting.None : Formatting.Indented
                    }
                );
                await context.Response.Body.WriteAsync(Encoding.UTF8.GetBytes(json));
            }
        }

        private string ShortStackTrace(Exception exception)
        {
            var stackTrace = exception.GetType().FullName + ": " + exception.Message;
            if (exception.InnerException == null)
            {
                return stackTrace;
            }
            else
            {
                var builder = new StringBuilder(stackTrace);
                while (exception.InnerException != null)
                {
                    exception = exception.InnerException;
                    builder.AppendLine().Append('\t').Append(exception.GetType().Name).Append(": ").AppendLine(exception.Message);
                }
                return builder.ToString();
            }
        }
    }
}
