﻿using System.Collections.Generic;
using System.Linq;

namespace Backend.Utils.Collections.Generic
{
    public class PaginatedCollection<T>
    {
        /// <summary>
        /// Текущая страница.
        /// </summary>
        public int Page;
        /// <summary>
        /// Количество страниц.
        /// </summary>
        public int Count;
        /// <summary>
        /// Количество объектов на странице.
        /// </summary>
        public int Limit;

        public List<T> List;

        public PaginatedCollection(int page, int count, int limit, List<T> list)
        {
            Page = page;
            Count = count;
            Limit = limit;
            List = list;
        }

        public PaginatedCollection(IQueryable<T> query, int page, int limit)
        {
            Limit = limit > 0 ? limit : 1;
            Count = query.Count() / Limit + 1;
            Page = page > 0 ? (page > Count ? Count : page) : 1;
            List = query.Skip((Page - 1) * Limit).Take(Limit).ToList();
        }
    }
}
