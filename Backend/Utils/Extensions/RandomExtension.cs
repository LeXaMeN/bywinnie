﻿using System;

namespace Backend.Utils.Extensions
{
    public static class RandomExtension
    {
        private const string Chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        public static void NextChars(this Random random, char[] chars)
        {
            for (var i = 0; i < chars.Length; i++)
                chars[i] = Chars[random.Next(Chars.Length)];
        }

        public static string NextString(this Random random, int length)
        {
            var chars = new char[length];

            for (var i = 0; i < chars.Length; i++)
                chars[i] = Chars[random.Next(Chars.Length)];

            return new string(chars);
        }
    }
}