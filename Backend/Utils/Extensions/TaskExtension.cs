﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Backend.Utils.Extensions
{
    public static class TaskExtension
    {
        public static Task<bool> Delay(this CancellationToken cancellationToken, int millisecondsDelay)
        {
            return Task.Delay(millisecondsDelay, cancellationToken).ContinueWith(task =>
            {
                // https://stackoverflow.com/a/32768637
                if (task.Exception?.InnerException is OperationCanceledException)
                {
                    return false;
                }
                return true;
            });
        }
    }
}
