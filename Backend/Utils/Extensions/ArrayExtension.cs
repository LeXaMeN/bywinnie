﻿using System;

namespace Backend.Utils.Extensions
{
    public static class ArrayExtension
    {
        // https://stackoverflow.com/questions/311165/how-do-you-convert-a-byte-array-to-a-hexadecimal-string-and-vice-versa/24343727#24343727
        private static readonly uint[] Lookup32 = new uint[256];

        static ArrayExtension()
        {
            for (int i = 0; i < 256; i++)
            {
                var str = i.ToString("X2");
                Lookup32[i] = ((uint)str[0]) + ((uint)str[1] << 16);
            }
        }

        public static string ToHex(this byte[] bytes)
        {
            var result = new char[bytes.Length * 2];
            for (int i = 0; i < bytes.Length; i++)
            {
                var val = Lookup32[bytes[i]];
                result[2 * i] = (char)val;
                result[2 * i + 1] = (char)(val >> 16);
            }
            return new string(result);
        }

        public static byte[] FromHex(this string hex)
        {
            var bytes = new byte[hex.Length / 2];
            for (int i = 0; i < bytes.Length; i++)
            {
                bytes[i] = Convert.ToByte(hex.Substring(i * 2, 2), 16);
            }
            return bytes;
        }

        public static bool HasDuplicates(this Array array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                for (int j = 0; j < array.Length; j++)
                {
                    if (i == j)
                        continue;
                    if (array.GetValue(i).Equals(array.GetValue(j)))
                        return true;
                }
            }
            return false;
        }
    }
}
