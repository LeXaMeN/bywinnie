﻿using System;

namespace Backend.Utils.Extensions
{
    public static class MathD
    {
        public static double Round(double value, int digits) => Math.Round(value, digits);
        public static decimal Round(decimal d, int decimals) => Math.Round(d, decimals);
        // https://stackoverflow.com/a/1426902
        public static double Ceiling(double value, int digits)
        {
            var adjustment = 10;
            for (int i = 0; i < digits; i++)
            {
                adjustment *= 10;
            }
            return Math.Round(value + (5d / adjustment), digits);
        }
        public static decimal Ceiling(decimal d, int decimals)
        {
            var adjustment = 10;
            for (int i = 0; i < decimals; i++)
            {
                adjustment *= 10;
            }
            return Math.Round(d + (5m / adjustment), decimals);
        }

        public static double Floor(double value, int digits)
        {
            var adjustment = 10;
            for (int i = 0; i < digits; i++)
            {
                adjustment *= 10;
            }
            return Math.Round(value - (5d / adjustment), digits);
        }
        public static decimal Floor(decimal d, int decimals)
        {
            var adjustment = 10;
            for (int i = 0; i < decimals; i++)
            {
                adjustment *= 10;
            }
            return Math.Round(d - (5m / adjustment), decimals);
        }

        public static double Truncate(double value, int digits)
        {
            var adjustment = 10;
            for (int i = 0; i < digits; i++)
            {
                adjustment *= 10;
            }
            return Math.Round(value > 0 ? value - (5d / adjustment) : value + (5d / adjustment), digits);
        }
        public static decimal Truncate(decimal d, int decimals)
        {
            var adjustment = 10;
            for (int i = 0; i < decimals; i++)
            {
                adjustment *= 10;
            }
            return Math.Round(d > 0 ? d - (5m / adjustment) : d + (5m / adjustment), decimals);
        }

        /// <summary>
        /// Returns a specified number raised to the specified power.
        /// </summary>
        /// <param name="x">A number to be raised to a power.</param>
        /// <param name="y">A number that specifies a power.</param>
        /// <returns>The number x raised to the power y.</returns>
        public static decimal Pow(int x, int y)
        {
            if (y == 0)
            {
                return 1;
            }
            if (y > 0)
            {
                var result = x;
                for (int i = y; i > 1; i--)
                {
                    result *= x;
                }
                return result;
            }
            // http://www.algebraclass.ru/otricatelnaya-stepen/
            //if (y < 0)
            {
                var divider = x;
                for (int i = y; i < -1; i++)
                {
                    divider *= x;
                }
                return 1m / divider;
            }
        }
    }
}
