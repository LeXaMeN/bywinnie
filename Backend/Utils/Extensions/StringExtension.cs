﻿using System;
using System.Text;

namespace Backend.Utils.Extensions
{
    public static class StringExtension
    {
        public static string ToHexadecimal(this string value)
        {
            return Encoding.UTF8.GetBytes(value).ToHex();
        }

        public static string FromHexadecimal(this string hex)
        {
            return Encoding.UTF8.GetString(hex.FromHex());
        }

        public static bool EqualsIgnoreCase(this string @string, string value)
        {
            if (@string == null || value == null)
                return @string == value;
            else
                return @string.Equals(value, StringComparison.InvariantCultureIgnoreCase);
        }

        public static bool HasValue(this string value, bool ignoreWhiteSpace = true)
        {
            return ignoreWhiteSpace ? string.IsNullOrWhiteSpace(value) : string.IsNullOrEmpty(value);
        }
    }
}
