﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Backend.Exceptions
{
    public class HttpInternalServerException : Exception, IHttpException
    {
        public int StatusCode => (int)HttpStatusCode.InternalServerError;
        public string DefaultMessage => "Internal Server Error.";

        public HttpInternalServerException(string message) : base(message) { }
        public HttpInternalServerException(string message, Exception innerException) : base(message, innerException) { }
    }
}
