﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend.Data.Context;
using Backend.Extensions.Cors;
using Backend.Extensions.JwtBearer;
using Backend.Extensions.Localization;
using Backend.Extensions.Serializer;
using Backend.Extensions.StartupTask;
using Backend.Extensions.StartupTask.Tasks;
using Backend.Managers;
using Backend.Middleware;
using Backend.Providers;
using Backend.Services;
using Backend.Services.Background;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Backend
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public ILoggerFactory LoggerFactory { get; }

        public Startup(IConfiguration configuration, ILoggerFactory loggerFactory)
        {
            Configuration = configuration;
            LoggerFactory = loggerFactory;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // https://stackoverflow.com/a/48444206
            services.AddDbContextPool<DatabaseContext>(options => options
                .UseLoggerFactory(LoggerFactory)
#if (DEBUG)
                .EnableSensitiveDataLogging() // https://stackoverflow.com/a/44207235
                //.ConfigureWarnings(bulder => bulder.Throw(RelationalEventId.QueryClientEvaluationWarning))
#endif
                .UseMySql(Configuration.GetConnectionString("DefaultConnection"))
            );

            // Middleware
            services.AddTransient<HttpExceptionHandlerMiddleware>();

            // Managers
            services.AddSingleton<CategoryManager>();
            services.AddSingleton<ItemManager>();
            services.AddSingleton<ProductFileManager>();
            services.AddSingleton<ProductManager>();
            services.AddSingleton<ProductDigiManager>();
            services.AddSingleton<ProductPartManager>();
            services.AddSingleton<ProductRatingManager>();
            services.AddSingleton<SiteManager>();
            services.AddSingleton<UserManager>();
            services.AddSingleton<UserTokenManager>();

            // Providers
            services.AddSingleton<DigisellerProvider>();
            services.AddSingleton<IdentityTokenProvider>();
            services.AddSingleton<ReCaptchaProvider>();
            services.AddSingleton<UserRoleProvider>();

            // Services
            services.AddSingleton<CategoryService>();
            services.AddSingleton<ItemService>();
            services.AddSingleton<ProductService>();
            services.AddSingleton<ProductDigiService>();
            services.AddSingleton<ProductRatingService>();
            services.AddSingleton<UserService>();
            services.AddSingleton<UserTokenService>();

            // Background services
            services.AddHostedService<ProductRatingBackgroundService>();

            // Token autorization
            services.AddJwtBearerAuthenticationExtension(Configuration);

            // https://stackoverflow.com/a/39113342
            services.AddRouting(options => options.LowercaseUrls = true);

            services.AddMvc()
                .AddJsonCamelCaseOptionsExtension()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddCorsExtension(Configuration);

            // Startup tasks
            services.AddStartupTask<MigrationStartupTask>();
            services.AddStartupTask<GeoIpsStartupTask>();
            services.AddStartupTask<UserStartupTask>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseAuthentication();
#if (DEBUG)
            app.UseHttpsRedirection();
            app.UseHsts();
#endif
            // https://www.jerriepelser.com/blog/aspnetcore-geo-location-from-ip-address/
            app.UseForwardedHeaders(new ForwardedHeadersOptions {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto,
                ForwardLimit = 2
            });
            app.UseRequestLocalizationExtension();
            app.UseMiddleware<HttpExceptionHandlerMiddleware>();
            app.UseMvc();
        }
    }
}
