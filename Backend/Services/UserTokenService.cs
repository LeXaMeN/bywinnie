﻿using Backend.Data.Context;
using Backend.Data.Entities;
//using Backend.DTO.User;
using Backend.DTO.UserToken;
using Backend.Exceptions;
using Backend.Managers;
using Backend.Models;
using Backend.Providers;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Net.Http.Headers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
//using Novel.Enums;
//using Novel.Exceptions;
//using Novel.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Services
{
    public class UserTokenService
    {
        public IConfiguration Configuration { get; }

        public UserManager Manager { get; }
        public UserTokenManager TokenManager { get; }

        private ReCaptchaProvider ReCaptchaProvider { get; }
        public IdentityTokenProvider IdentityTokenProvider { get; }

        public UserTokenService(IConfiguration configuration, UserManager userManager, UserTokenManager userTokenManager, ReCaptchaProvider reCaptchaProvider, IdentityTokenProvider identityTokenProvider)
        {
            Configuration = configuration;
            Manager = userManager;
            TokenManager = userTokenManager;
            ReCaptchaProvider = reCaptchaProvider;
            IdentityTokenProvider = identityTokenProvider;
        }

        public IdentityToken Create(DatabaseContext db, UserTokenDTO dto, HttpContext httpContext)
        {
            if (ReCaptchaProvider.IsValid(dto.Captcha, ReCaptchaType.Invisible) == false)
                throw new HttpBadRequestException("Invalid captcha.");

            var user = Manager.ReadonlyOrNull(db, dto.Login);
            if (user == null || Manager.IsValidPassword(user.Password, dto.Password) == false)
                throw new HttpBadRequestException("Invalid login or password.");

            return IdentityTokenProvider.Create(db, user.Id, user.Login, user.Roles, httpContext.Connection.RemoteIpAddress.ToString());
        }
    }
}
