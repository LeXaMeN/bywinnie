﻿using Backend.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Services
{
    public class CategoryService
    {
        public CategoryManager Manager { get; }

        public CategoryService(CategoryManager manager)
        {
            Manager = manager;
        }
    }
}
