﻿using Backend.Data.Context;
using Backend.Managers;
using Backend.Providers;
using Backend.Utils.Extensions;
using Destructurama.Attributed;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Backend.Services.Background
{
    public class ProductRatingBackgroundService : BackgroundService
    {
        private IConfiguration Configuration { get; }
        private ILogger<ProductRatingBackgroundService> Logger { get; }
        private IServiceProvider ServiceProvider { get; }

        private ProductRatingManager ProductRatingManager { get; }
        private ProductDigiManager ProductDigiManager { get; }
        private DigisellerProvider DigisellerProvider { get; }

        public ProductRatingBackgroundService(IConfiguration configuration, ILogger<ProductRatingBackgroundService> logger, IServiceProvider serviceProvider, ProductRatingManager productRatingManager, ProductDigiManager productDigiManager, DigisellerProvider digisellerProvider)
        {
            Configuration = configuration;
            Logger = logger;
            ServiceProvider = serviceProvider;
            ProductRatingManager = productRatingManager;
            ProductDigiManager = productDigiManager;
            DigisellerProvider = digisellerProvider;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (stoppingToken.IsCancellationRequested == false)
            {
                var waitMilliseconds = 2 * 60 * 60 * 1000; // 2 hours

                try
                {
                    var interval = TimeSpan.Parse(Configuration["Digiseller:RatingInterval"]);
                    var dateFinish = DateTime.UtcNow.AddHours(3); // UTC+3 //TimeZoneInfo.ConvertTime(DateTime.UtcNow, TimeZoneInfo.FindSystemTimeZoneById("Europe/Moscow"));
                    var dateStart = dateFinish - interval;
                    var response = DigisellerProvider.GetStatisticsAgentSales(dateStart, dateFinish, returned: 1);
                    if (response.retval != 0)
                    {
                        Logger.LogError(new ArgumentOutOfRangeException(nameof(response.retval), response.retval, response.retdesc), "DigisellerProvider.GetStatisticsAgentSales");
                        waitMilliseconds = 5 * 60 * 1000; // 5 minutes
                    }
                    else if (response.rows?.Count > 0)
                    {
                        var ratings = response.rows.GroupBy(r => r.product_id).Select(g => new
                        {
                            DigiId = g.Key,
                            ReceivedUSD = g.Sum(r => r.amount_in_usd * (r.partner_percent / 100m)),
                            SalesCount = g.Count()
                        });

                        using (var scope = ServiceProvider.CreateScope())
                        {
                            var db = scope.ServiceProvider.GetRequiredService<DatabaseContext>();
                            var productRatings = new Dictionary<int, (decimal ReceivedUSD, int SalesCount)>();

                            foreach (var rating in ratings)
                            {
                                if (stoppingToken.IsCancellationRequested)
                                    break;

                                var productId = ProductDigiManager.ReadProductIdOrNull(db, rating.DigiId);
                                if (productId == null)
                                    continue;

                                if (productRatings.TryGetValue(productId.Value, out var productRating))
                                {
                                    productRating.ReceivedUSD += rating.ReceivedUSD;
                                    productRating.SalesCount += rating.SalesCount;
                                    // struct => need resave
                                    productRatings[productId.Value] = productRating;
                                }
                                else
                                {
                                    productRatings[productId.Value] = (rating.ReceivedUSD, rating.SalesCount);
                                }
                            }

                            foreach (var productRating in productRatings)
                            {
                                ProductRatingManager.CreateOrUpdate(db, productRating.Key, productRating.Value.ReceivedUSD, productRating.Value.SalesCount > ushort.MaxValue ? ushort.MaxValue : (ushort)productRating.Value.SalesCount);
                            }

                            ProductRatingManager.Delete(db, olderThanDate: DateTime.UtcNow - interval);
                        }
                    }
                }
                catch (Exception exception)
                {
                    Logger.LogCritical(exception, $"{nameof(ProductRatingBackgroundService)}.{nameof(ExecuteAsync)}");
                    waitMilliseconds = 5 * 60 * 1000; // 5 minutes
                }

                await stoppingToken.Delay(waitMilliseconds);
            }
        }
    }
}
