﻿using Backend.Data.Context;
using Backend.Data.Entities;
using Backend.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Services
{
    public class ProductRatingService
    {
        public ProductRatingManager Manager { get; }
        private ProductDigiManager ProductDigiManager { get; }
        private ProductFileManager ProductFileManager { get; }

        public ProductRatingService(ProductRatingManager manager, ProductDigiManager productDigiManager, ProductFileManager productFileManager)
        {
            Manager = manager;
            ProductDigiManager = productDigiManager;
            ProductFileManager = productFileManager;
        }

        public List<Product> ReadonlySortedByRatingList(DatabaseContext db, int? categoryId, int limit)
        {
            var products = Manager.ReadonlyList(db, categoryId, limit).Select(pr => pr.Product).ToList();
            if (products.Count < limit)
            {
                var sortedProducts = ProductDigiManager.ReadonlySortedByAgencyFeeProductList(db, categoryId, limit: limit - products.Count);
                if (sortedProducts.Any() && products.Count > 0)
                {
                    // исключаем повторения
                    sortedProducts = sortedProducts.Where(sp => products.Any(p => p.Id == sp.Id) == false).ToList();
                }
                if (sortedProducts.Any())
                {
                    products.AddRange(sortedProducts);
                }
            }
            for (int i = 0; i < products.Count; i++)
            {
                products[i].Files = ProductFileManager.ReadonlyList(db, products[i].Id, ProductFileType.Header);
            }
            return products;
        }
    }
}
