﻿using Backend.Data.Context;
using Backend.Data.Entities;
using Backend.DTO.Product;
using Backend.Exceptions;
using Backend.Managers;
using Backend.Providers;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Backend.Services
{
    public class ProductDigiService
    {
        public ProductDigiManager Manager { get; }
        public ProductManager ProductManager { get; }
        private DigisellerProvider DigisellerProvider { get; }

        public ProductDigiService(ProductDigiManager productDigiManager, ProductManager productManager, DigisellerProvider digisellerProvider)
        {
            Manager = productDigiManager;
            ProductManager = productManager;
            DigisellerProvider = digisellerProvider;
        }

        public ProductDigi Create(DatabaseContext db, int digiId, int productId, ProductDigiDTO dto)
        {
            var pDigi = Manager.ReadonlyOrNull(db, digiId);
            if (pDigi != null)
                throw new HttpBadRequestException($"{nameof(ProductDigi)} #{digiId} \"{pDigi.Name}\" already exists in product #{pDigi.ProductId}.");

            //var dList = GetDigiProductList(digiId);
            //if (dList.Count == 0 || dList[0].id != digiId)
            //    throw new HttpInternalServerException($"dList.Count == {dList.Count} || dList[0].id != {digiId}");

            return Manager.Create(db, digiId, productId, /*dList[0].*/dto.Name, dto.AgencyFee);
        }

        public ProductDigi UpdateAnonymously(DatabaseContext db, int digiId, int productId, ProductDigiDTO dto)
        {
            var digi = Manager.ReadOrNull(db, digiId);
            if (digi != null && /* защита */ (digi.Name == dto.Name && dto.AgencyFee == digi.AgencyFee))
            {
                if (dto.AgencyFeeChanged == true && digi.AgencyFeeChanged == false)
                {
                    var dInfo = DigisellerProvider.GetDigiProductInfo(digiId);
                    if (dInfo.retval == 0)
                    {
                        if (byte.Parse(dInfo.product.agency_fee) < digi.AgencyFee)
                        {
                            digi.AgencyFeeChanged = true;
                        }
                        if (digi.Name != HttpUtility.HtmlDecode(dInfo.product.name)) // всякие html символы типа &nbsp; пишутся кодом
                        {
                            digi.NameChanged = true;
                        }
                        db.SaveChanges();
                    }
                }
                else if (dto.NameChanged == true && digi.NameChanged == false)
                {
                    var dList = DigisellerProvider.GetDigiProductList(digi.Id);
                    if (dList.Count > 0 && digi.Name != dList[0].name) // всякие html символы типа &nbsp; НЕ пишутся кодом
                    {
                        digi.NameChanged = true;
                        db.SaveChanges();
                    }
                }
            }
            return digi;
        }
    }
}
