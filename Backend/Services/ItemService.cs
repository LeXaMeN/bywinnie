﻿using Backend.Data.Context;
using Backend.Data.Entities;
using Backend.DTO.Item;
using Backend.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Services
{
    public class ItemService
    {
        public ItemManager Manager { get; }

        public ItemService(ItemManager manager)
        {
            Manager = manager;
        }
    }
}
