﻿using Backend.Data.Context;
using Backend.Data.Entities;
//using Backend.DTO.User;
using Backend.Exceptions;
using Backend.Managers;
using Backend.Providers;
using Backend.Utils.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
//using Novel.Collections.Generic;
//using Novel.Enums;
//using Novel.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Services
{
    public class UserService
    {
        private IConfiguration Configuration { get; }

        public UserManager Manager { get; }
        public UserTokenManager TokenManager { get; }

        private ReCaptchaProvider ReCaptchaProvider { get; }
        private IdentityTokenProvider IdentityTokenProvider { get; }
        private UserRoleProvider UserRoleProvider { get; }

        public UserService(IConfiguration configuration, UserManager userManager, UserTokenManager userTokenManager, ReCaptchaProvider reCaptchaProvider, IdentityTokenProvider identityTokenProvider, UserRoleProvider userRoleProvider)
        {
            Configuration = configuration;
            Manager = userManager;
            TokenManager = userTokenManager;
            ReCaptchaProvider = reCaptchaProvider;
            IdentityTokenProvider = identityTokenProvider;
            UserRoleProvider = userRoleProvider;
        }

        #region Roles
        //public bool CreateRole(DatabaseContext db, int userId, string role) => ChangeRoles(db, userId, role, true);
        //public bool RemoveRole(DatabaseContext db, int userId, string role) => ChangeRoles(db, userId, role, false);
        //private bool ChangeRoles(DatabaseContext db, int userId, string role, bool add)
        //{
        //    if (UserRoleProvider.IsValid(role) == false)
        //        throw new HttpBadRequestException("Invalid role.");
        //    // установка/удаление роли админа и изменение ролей админа хоста запрещено
        //    if (role == UserRole.Admin || db.Users.Where(u => u.Id == userId && u.Nickname == $"admin@{Configuration["Domains:Host"]}").Any())
        //        return false;

        //    var roles = UserRoleProvider.ReadonlyList(db, userId);
        //    if (add)
        //    {
        //        if (roles.Contains(role) == true)
        //        {
        //            return false;
        //        }
        //        roles.Add(role);
        //    }
        //    else
        //    {
        //        if (roles.Contains(role) == false)
        //        {
        //            return false;
        //        }
        //        roles.Remove(role);
        //    }
        //    if (UserRoleProvider.Update(db, userId, roles) == true)
        //    {
        //        // очищаем кэш админов при изменении какой-либо роли админа
        //        //if (role == UserRole.Admin) { UserRoleProvider._adminsIds = null; }
        //        // удаляем все токены, чтобы при авторизации подгрузились обновлённые роли
        //        TokenManager.DeleteAll(db, userId, UserTokenType.RefreshToken);
        //        return true;
        //    }
        //    return false;
        //}
        #endregion
    }
}
