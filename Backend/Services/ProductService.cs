﻿using Backend.Data.Context;
using Backend.Data.Entities;
using Backend.DTO.Product;
using Backend.DTO.ProductPart;
using Backend.Exceptions;
using Backend.Managers;
using Backend.Providers;
using Backend.Utils.Collections.Generic;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Services
{
    public class ProductService
    {
        private IConfiguration Configuration { get; }

        public ProductManager Manager { get; }
        public ProductFileManager FileManager { get; }
        public ProductPartManager PartManager { get; }
        private CategoryManager CategoryManager { get; }
        private ProductDigiManager DigiManager { get; }
        private ItemManager ItemManager { get; }
        private DigisellerProvider DigisellerProvider { get; }

        public ProductService(IConfiguration configuration, ProductManager productManager, ProductFileManager productFileManager, ProductPartManager productPartManager, CategoryManager categoryManager, ProductDigiManager productDigiManager, ItemManager itemManager, DigisellerProvider digisellerProvider)
        {
            Configuration = configuration;
            Manager = productManager;
            FileManager = productFileManager;
            PartManager = productPartManager;
            CategoryManager = categoryManager;
            DigiManager = productDigiManager;
            ItemManager = itemManager;
            DigisellerProvider = digisellerProvider;
        }

        public PaginatedCollection<Product> ReadCollection(DatabaseContext db, ProductCollectionDTO dto, int page, int limit)
        {
            var collection = default(PaginatedCollection<Product>);
            switch (dto.OrderBy)
            {
                case ProductCollectionDTO.OrderEnum.Id:
                    collection = Manager.ReadCollection(db, dto.Id, dto.CategoryId, dto.Name, dto.PriceMin, dto.PriceMax, dto.Filters, nameof(Product.Id), dto.OrderByDesc, page, limit);
                    break;
                case ProductCollectionDTO.OrderEnum.Category:
                    collection = Manager.ReadCollection(db, dto.Id, dto.CategoryId, dto.Name, dto.PriceMin, dto.PriceMax, dto.Filters, nameof(Product.Category), dto.OrderByDesc, page, limit);
                    break;
                case ProductCollectionDTO.OrderEnum.Name:
                    collection = Manager.ReadCollection(db, dto.Id, dto.CategoryId, dto.Name, dto.PriceMin, dto.PriceMax, dto.Filters, nameof(Product.Name), dto.OrderByDesc, page, limit);
                    break;
                case ProductCollectionDTO.OrderEnum.NameRus:
                    collection = Manager.ReadCollection(db, dto.Id, dto.CategoryId, dto.Name, dto.PriceMin, dto.PriceMax, dto.Filters, nameof(Product.NameRus), dto.OrderByDesc, page, limit);
                    break;
                case ProductCollectionDTO.OrderEnum.Price:
                    collection = Manager.ReadCollection(db, dto.Id, dto.CategoryId, dto.Name, dto.PriceMin, dto.PriceMax, dto.Filters, nameof(Product.Price), dto.OrderByDesc, page, limit);
                    break;
                default:
                    collection = Manager.ReadCollection(db, dto.Id, dto.CategoryId, dto.Name, dto.PriceMin, dto.PriceMax, dto.Filters, null, false, page, limit);
                    break;
            }
            for (int i = 0; i < collection.List.Count; i++)
            {
                collection.List[i].Files = FileManager.ReadonlyList(db, collection.List[i].Id, ProductFileType.Header);
            }
            return collection;
        }

        public Product ReadonlyOrNull(DatabaseContext db, int id)
        {
            var product = Manager.ReadonlyOrNull(db, id);
            if (product?.Category.ParentId != null)
            {
                product.Category.Parent = CategoryManager.ReadonlyOrNull(db, product.Category.ParentId.Value);
            }
            return product;
        }

        public ProductPartSequenceDataDTO ReadonlySequenceData(DatabaseContext db, int categoryId)
        {
            var category = CategoryManager.ReadonlyOrNull(db, categoryId) ?? throw new HttpNotFoundException(nameof(Category), categoryId);
            var productIds = Manager.ReadonlyIdList(db, categoryId);

            return new ProductPartSequenceDataDTO() {
                Category = category,
                Categories = PartManager.ReadonlySequenceCategories(db, productIds).ToList(),
                Dictionary = PartManager.ReadonlySequenceItems(db, productIds)
                    .GroupBy(tuple => tuple.Item1).ToDictionary(
                        g => g.Key,
                        g => g.Select(tuple => tuple.Item2).OrderBy(i => i.Name).ToList()
                    )
            };
        }

        public ProductPartSequenceDataDTO ReadonlySequenceDataOrNull(DatabaseContext db, int categoryId)
        {
            var sequence = PartManager.ReadonlySequenceOrNull(db, categoryId);
            if (sequence == null)
                return null;

            return new ProductPartSequenceDataDTO() {
                Category = CategoryManager.ReadonlyOrNull(db, sequence.CategoryId),
                Categories = PartManager.ReadonlyCategoryList(db, sequence.Categories),
                Dictionary = sequence.Dictionary.ToDictionary(p => p.Key, p => ItemManager.ReadonlyList(db, p.Value))
            };
        }

        public decimal? UpdatePriceAnonymously(DatabaseContext db, int productId, string productIndex, decimal currentPrice)
        {
            var price = Manager.ReadonlyPriceOrNull(db, productId, productIndex);
            if (price != currentPrice)
                return null;

            var ids = DigiManager.ReadonlyIdList(db, productId, onlyAvailable: true);
            if (ids.Count == 0)
            {
                Manager.Update(db, productId, 0m);
                return 0m;
            }
            var dList = DigisellerProvider.GetDigiProductList(ids);
            if (dList.Count == 0)
                return null;

            var dListInStock = dList.Where(d => d.in_stock > 0);
            // если есть мои, то только их
            var sellerId = Configuration.GetValue<int>("Digiseller:Id");
            if (dListInStock.Any(d => d.id_seller == sellerId))
                dList = dListInStock.Where(d => d.id_seller == sellerId).ToList();
            // если все in stock, то не перезаписываем
            else if (dList.Count != dListInStock.Count())
                dList = dListInStock.ToList();

            var min = dList.Count > 0 ? dList.Min(d => d.price_rub) : 0m;
            Manager.Update(db, productId, min);
            return min;
        }
    }
}
