﻿using Backend.Data.Context;
using Backend.Data.Entities;
using Backend.Exceptions;
using Backend.Extensions.Database;
using Backend.Utils.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Z.EntityFramework.Plus;

namespace Backend.Managers
{
    public class ProductManager
    {
        public Product Create(DatabaseContext db, int categoryId, string index, string name, string nameRus, string description, string descriptionRus)
        {
            if (db.Products.Any(c => c.Index == index))
                throw new HttpBadRequestException($"Index \"{index}\" already exists.");

            var product = new Product() {
                CategoryId = categoryId,
                Index = index,
                Name = name,
                NameRus = nameRus,
                Description = description,
                DescriptionRus = descriptionRus,
                Price = 0m,
            };
            db.Products.Add(product);
            db.SaveChanges();
            return product;
        }

        public bool Update(DatabaseContext db, int id, int categoryId, string index, string name, string nameRus, string description, string descriptionRus, decimal? price)
        {
            if (db.Products.Any(p => p.Index == index && p.Id != id))
            {
                return false;
            }
            return price.HasValue
                ? db.Products.Where(p => p.Id == id).Update(p => new Product() { CategoryId = categoryId, Index = index, Name = name, NameRus = nameRus, Description = description, DescriptionRus = descriptionRus, Price = price.Value }) > 0
                : db.Products.Where(p => p.Id == id).Update(p => new Product() { CategoryId = categoryId, Index = index, Name = name, NameRus = nameRus, Description = description, DescriptionRus = descriptionRus }) > 0;
        }

        public bool Update(DatabaseContext db, int id, decimal price)
        {
            return db.Products
                .Where(p => p.Id == id)
                .Update(p => new Product() { Price = price }) > 0;
        }

        public decimal? ReadonlyPriceOrNull(DatabaseContext db, int id, string index)
        {
            return db.Products.AsNoTracking().Where(p => p.Id == id && p.Index == index).Select(p => new { p.Price }).FirstOrDefault()?.Price;
        }

        public Product ReadonlyOrNull(DatabaseContext db, int id)
        {
            return db.Products.AsNoTracking()
                .Where(p => p.Id == id)
                .Include(p => p.Category)
                .Include(p => p.Digis)
                .Include(p => p.Files)
                .OrderBy(p => p.Id).FirstOrDefault();
        }

        //public List<Product> ReadonlyList(DatabaseContext db, int categoryId)
        //{
        //    return db.Products.AsNoTracking().Where(p => p.CategoryId == categoryId).Include(p => p.Category).OrderBy(p => p.Name).ToList();
        //}

        public List<int> ReadonlyIdList(DatabaseContext db, int categoryId)
        {
            return db.Products.Where(p => p.CategoryId == categoryId).Select(p => p.Id).ToList();
        }

        public PaginatedCollection<Product> ReadCollection(DatabaseContext db, int? id, int? categoryId, string name, decimal? priceMin, decimal? priceMax, List<int[]> filters, string orderByField, bool orderByDesc, int page, int limit)
        {
            var query = db.Products.AsNoTracking();

            if (id.HasValue)
                query = query.Where(p => p.Id == id.Value);
            if (categoryId.HasValue)
                query = query.Where(p => p.CategoryId == categoryId.Value);
            if (priceMin.HasValue && priceMax.HasValue && priceMin.Value == priceMax.Value)
            {
                query = query.Where(p => p.Price == priceMin.Value);
            }
            else
            {
                if (priceMin.HasValue)
                    query = query.Where(p => p.Price >= priceMin.Value);
                if (priceMax.HasValue)
                    query = query.Where(p => p.Price <= priceMax.Value);
            }
            if (filters?.Count > 0)
            {
                foreach (var filter in filters)
                {
                    if (filter.Length < 2)
                        continue;
                    var itemType = filter[0];
                    if (itemType < sbyte.MinValue || itemType > sbyte.MaxValue || Enum.IsDefined(typeof(ItemType), (sbyte)itemType) == false)
                        continue;

                    var ivId = filter[1];
                    var ivIds = filter.Length > 2 ? filter.Skip(1).ToList() : null;
                    if (ivId <= 0 || (ivIds != null && ivIds.Exists(itemValueId => itemValueId <= 0)))
                        continue;

                    switch ((ItemType)itemType)
                    {
                        case ItemType.Boolean:
                            if (ivIds?.Count > 1)
                                continue;
                            else
                                query = query.Where(p => db.ProductPartBooleans.Any(ppv => ppv.ItemValueId == ivId && ppv.ProductId == p.Id));
                            break;
                        case ItemType.Date:
                            if (ivIds?.Count > 1)
                                query = query.Where(p => db.ProductPartDates.Any(ppv => ivIds.Contains(ppv.ItemValueId) && ppv.ProductId == p.Id));
                            else
                                query = query.Where(p => db.ProductPartDates.Any(ppv => ppv.ItemValueId == ivId && ppv.ProductId == p.Id));
                            break;
                        case ItemType.Decimal:
                            if (ivIds?.Count > 1)
                                query = query.Where(p => db.ProductPartDecimals.Any(ppv => ivIds.Contains(ppv.ItemValueId) && ppv.ProductId == p.Id));
                            else
                                query = query.Where(p => db.ProductPartDecimals.Any(ppv => ppv.ItemValueId == ivId && ppv.ProductId == p.Id));
                            break;
                        case ItemType.Enum:
                            if (ivIds?.Count > 1)
                                query = query.Where(p => db.ProductPartEnums.Any(ppv => ivIds.Contains(ppv.ItemValueId) && ppv.ProductId == p.Id));
                            else
                                query = query.Where(p => db.ProductPartEnums.Any(ppv => ppv.ItemValueId == ivId && ppv.ProductId == p.Id));
                            break;
                        case ItemType.Integer:
                            if (ivIds?.Count > 1)
                                query = query.Where(p => db.ProductPartIntegers.Any(ppv => ivIds.Contains(ppv.ItemValueId) && ppv.ProductId == p.Id));
                            else
                                query = query.Where(p => db.ProductPartIntegers.Any(ppv => ppv.ItemValueId == ivId && ppv.ProductId == p.Id));
                            break;
                    }
                }
            }

            query = query.Include(p => p.Category);

            if (name?.Length >= 3)
            {
                //query = query.Where(p => EF.Functions.Like(p.Name, $"%{name}%") || EF.Functions.Like(p.NameRus, $"%{name}%"));
                query = query
                    .Where(p => MySqlTextFunctions.MatchAgainst(new { p.Name, p.Description }, name) > 0.5 || MySqlTextFunctions.MatchAgainst(new { p.NameRus, p.DescriptionRus }, name) > 0.5)
                    .Select(p => new
                    {
                        Product = p,
                        Relevance = MySqlTextFunctions.MatchAgainst(new { p.Name, p.Description }, name),
                        RelevanceRus = MySqlTextFunctions.MatchAgainst(new { p.NameRus, p.DescriptionRus }, name)
                    })
                    .OrderByDescending(g => g.Relevance + g.RelevanceRus)
                    .Select(g => g.Product);
            }
            else
            {
                switch (orderByField)
                {
                    case nameof(Product.Id):
                        query = orderByDesc ? query.OrderByDescending(p => p.Id) : query.OrderBy(p => p.Id);
                        break;
                    case nameof(Product.Category):
                        query = orderByDesc ? query.OrderByDescending(p => p.Category.Name) : query.OrderBy(p => p.Category.Name);
                        break;
                    case nameof(Product.Name):
                    default:
                        query = orderByDesc ? query.OrderByDescending(p => p.Name) : query.OrderBy(p => p.Name);
                        break;
                    case nameof(Product.NameRus):
                        query = orderByDesc ? query.OrderByDescending(p => p.NameRus) : query.OrderBy(p => p.NameRus);
                        break;
                    case nameof(Product.Price):
                        query = orderByDesc ? query.OrderByDescending(p => p.Price) : query.OrderBy(p => p.Price);
                        break;
                }
            }

            return new PaginatedCollection<Product>(query, page, limit);
        }

        public bool Delete(DatabaseContext db, int id)
        {
            return db.ProductPartBooleans.Any(ppv => ppv.ProductId == id) == false
                && db.ProductPartDates.Any(ppv => ppv.ProductId == id) == false
                && db.ProductPartDecimals.Any(ppv => ppv.ProductId == id) == false
                && db.ProductPartEnums.Any(ppv => ppv.ProductId == id) == false
                && db.ProductPartIntegers.Any(ppv => ppv.ProductId == id) == false
                && db.ProductPartStrings.Any(ppv => ppv.ProductId == id) == false
                && db.ProductDigis.Any(pd => pd.ProductId == id) == false
                && db.ProductFiles.Any(pf => pf.ProductId == id) == false
                && db.Products.Where(p => p.Id == id).Delete() > 0;
        }
    }
}
