﻿using Backend.Data.Context;
using Backend.Data.Entities;
using Backend.Exceptions;
using Backend.Extensions.Database;
using Backend.Utils.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Z.EntityFramework.Plus;

namespace Backend.Managers
{
    public class ItemManager
    {
        //private static readonly IReadOnlyDictionary<Type, ItemType> ItemValueTypes = new Dictionary<Type, ItemType>() {
        //    { typeof(ItemBoolean), ItemType.Boolean },
        //    { typeof(ItemDate), ItemType.Date },
        //    { typeof(ItemDecimal), ItemType.Decimal },
        //    { typeof(ItemEnum), ItemType.Enum },
        //    { typeof(ItemInteger), ItemType.Integer },
        //    { typeof(ItemString), ItemType.String }
        //};
        //private ItemType GetItemValueType<TItemValue>() where TItemValue : IItemValue => ItemValueTypes[typeof(TItemValue)];
        //private ItemType GetItemValueType(IItemValue itemValue) => ItemValueTypes[itemValue.GetType()];


        public Item Create(DatabaseContext db, ItemType type, string name, string nameRus)
        {
            var item = new Item() {
                Type = type,
                Name = name,
                NameRus = nameRus
            };
            db.Items.Add(item);
            db.SaveChanges();

            return item;
        }

        public bool Update(DatabaseContext db, int id, string name, string nameRus)
        {
            return db.Items.Where(i => i.Id == id).Update(i => new Item() { Name = name, NameRus = nameRus }) > 0;
        }

        public Item ReadonlyOrNull(DatabaseContext db, int id)
        {
            return db.Items.AsNoTracking().Where(i => i.Id == id).OrderBy(i => i.Id).FirstOrDefault();
        }

        public List<Item> ReadonlyList(DatabaseContext db, List<int> ids)
        {
            if (ids.Count == 0)
            {
                return new List<Item>();
            }
            var list = ids.Select(id => ReadonlyOrNull(db, id)).Where(i => i != null && i.Type != ItemType.String).ToList();
            for (int i = 0; i < list.Count; i++)
            {
                list[i].Values = ReadonlyValueList(db, list[i].Id, list[i].Type);
            }
            return list;
        }

        public PaginatedCollection<Item> ReadCollection(DatabaseContext db, int? id, ItemType? type, string name, int page, int limit)
        {
            var query = db.Items.AsNoTracking();

            if (id.HasValue)
                query = query.Where(i => i.Id == id.Value);
            if (type.HasValue)
                query = query.Where(i => i.Type == type.Value);
            if (name?.Length > 2)
                query = query.Where(i => EF.Functions.Like(i.Name, $"%{name}%") || EF.Functions.Like(i.NameRus, $"%{name}%"));

            return new PaginatedCollection<Item>(query.OrderByDescending(i => i.Id), page, limit);
        }

        public bool Delete(DatabaseContext db, int id, ItemType type)
        {
            switch (type)
            {
                case ItemType.Boolean:
                    if (db.ItemBooleans.Any(iv => iv.ItemId == id)) return false;
                    break;
                case ItemType.Date:
                    if (db.ItemDates.Any(iv => iv.ItemId == id)) return false;
                    break;
                case ItemType.Decimal:
                    if (db.ItemDecimals.Any(iv => iv.ItemId == id)) return false;
                    break;
                case ItemType.Enum:
                    if (db.ItemEnums.Any(iv => iv.ItemId == id)) return false;
                    break;
                case ItemType.Integer:
                    if (db.ItemIntegers.Any(iv => iv.ItemId == id)) return false;
                    break;
                case ItemType.String:
                    if (db.ItemStrings.Any(iv => iv.ItemId == id)) return false;
                    break;
            }
            return db.Items.Where(i => i.Id == id && i.Type == type).Delete() > 0;
        }

        private TValue CreateValue<TValue>(DatabaseContext db, int itemId, TValue value) where TValue : class, IItemValue
        {
            if (db.Items.Any(i => i.Id == itemId && i.Type == value.Type) == false)
                throw new HttpNotFoundException(nameof(Item), itemId);

            db.Set<TValue>().Add(value);
            db.SaveChanges();
            return value;
        }
        public ItemBoolean CreateBooleanValue(DatabaseContext db, int itemId, bool value) => !ExistAnyBooleanValue(db, itemId, value)
            ? CreateValue(db, itemId, new ItemBoolean() { ItemId = itemId, Value = value })
            : throw new HttpFoundException(nameof(ItemBoolean), value);
        public ItemDate CreateDateValue(DatabaseContext db, int itemId, DateTime value) => !ExistAnyDateValue(db, itemId, value)
            ? CreateValue(db, itemId, new ItemDate() { ItemId = itemId, Value = value })
            : throw new HttpFoundException(nameof(ItemDate), value);
        public ItemDecimal CreateDecimalValue(DatabaseContext db, int itemId, decimal value) => !ExistAnyDecimalValue(db, itemId, value)
            ? CreateValue(db, itemId, new ItemDecimal() { ItemId = itemId, Value = value })
            : throw new HttpFoundException(nameof(ItemDecimal), value);
        public ItemEnum CreateEnumValue(DatabaseContext db, int itemId, string value, string valueRus, short order) => !ExistAnyEnumValue(db, itemId, value, valueRus)
            ? CreateValue(db, itemId, new ItemEnum() { ItemId = itemId, Value = value, ValueRus = valueRus, Order = order })
            : throw new HttpFoundException(nameof(ItemEnum), value);
        public ItemInteger CreateIntegerValue(DatabaseContext db, int itemId, int value) => !ExistAnyIntegerValue(db, itemId, value)
            ? CreateValue(db, itemId, new ItemInteger() { ItemId = itemId, Value = value })
            : throw new HttpFoundException(nameof(ItemInteger), value);
        public ItemString CreateStringValue(DatabaseContext db, int itemId, string value, string valueRus) => !ExistAnyStringValue(db, itemId, value, valueRus)
            ? CreateValue(db, itemId, new ItemString() { ItemId = itemId, Value = value, ValueRus = valueRus })
            : throw new HttpFoundException(nameof(ItemString), null);

        private bool ExistAnyBooleanValue(DatabaseContext db, int itemId, bool value) => db.ItemBooleans
            .Any(iv => iv.ItemId == itemId && iv.Value == value);
        private bool ExistAnyDateValue(DatabaseContext db, int itemId, DateTime value) => db.ItemDates
            .Any(iv => iv.ItemId == itemId && iv.Value == value);
        private bool ExistAnyDecimalValue(DatabaseContext db, int itemId, decimal value) => db.ItemDecimals
            .Any(iv => iv.ItemId == itemId && iv.Value == value);
        private bool ExistAnyEnumValue(DatabaseContext db, int itemId, string value, string valueRus) => db.ItemEnums
            .Any(iv => iv.ItemId == itemId && (iv.Value == value || iv.ValueRus == valueRus));
        private bool ExistAnyIntegerValue(DatabaseContext db, int itemId, int value) => db.ItemIntegers
            .Any(iv => iv.ItemId == itemId && iv.Value == value);
        private bool ExistAnyStringValue(DatabaseContext db, int itemId, string value, string valueRus) => db.ItemStrings
            .Any(iv => iv.ItemId == itemId && (iv.Value == value || iv.ValueRus == valueRus));

        public bool UpdateBooleanValue(DatabaseContext db, int itemValueId, bool value) => db.ItemBooleans
            .Where(iv => iv.Id == itemValueId && db.Items.Any(i => i.Id == iv.ItemId && i.Type == ItemType.Boolean))
            .Update(iv => new ItemBoolean() { Value = value }) > 0;
        public bool UpdateDateValue(DatabaseContext db, int itemValueId, DateTime value) => db.ItemDates
            .Where(iv => iv.Id == itemValueId && db.Items.Any(i => i.Id == iv.ItemId && i.Type == ItemType.Date))
            .Update(iv => new ItemDate() { Value = value }) > 0;
        public bool UpdateDecimalValue(DatabaseContext db, int itemValueId, decimal value) => db.ItemDecimals
            .Where(iv => iv.Id == itemValueId && db.Items.Any(i => i.Id == iv.ItemId && i.Type == ItemType.Decimal))
            .Update(iv => new ItemDecimal() { Value = value }) > 0;
        public bool UpdateEnumValue(DatabaseContext db, int itemValueId, string value, string valueRus, short order) => db.ItemEnums
            .Where(iv => iv.Id == itemValueId && db.Items.Any(i => i.Id == iv.ItemId && i.Type == ItemType.Enum))
            .Update(iv => new ItemEnum() { Value = value, ValueRus = valueRus, Order = order }) > 0;
        public bool UpdateIntegerValue(DatabaseContext db, int itemValueId, int value) => db.ItemIntegers
            .Where(iv => iv.Id == itemValueId && db.Items.Any(i => i.Id == iv.ItemId && i.Type == ItemType.Integer))
            .Update(iv => new ItemInteger() { Value = value }) > 0;
        public bool UpdateStringValue(DatabaseContext db, int itemValueId, string value, string valueRus) => db.ItemStrings
            .Where(iv => iv.Id == itemValueId && db.Items.Any(i => i.Id == iv.ItemId && i.Type == ItemType.String))
            .Update(iv => new ItemString() { Value = value, ValueRus = valueRus }) > 0;

        public IList ReadonlyValueList(DatabaseContext db, int itemId, ItemType type)
        {
            switch (type)
            {
                case ItemType.Boolean:
                    return db.ItemBooleans.AsNoTracking().Where(iv => iv.ItemId == itemId).OrderByDescending(iv => iv.Value).ToList();
                case ItemType.Date:
                    return db.ItemDates.AsNoTracking().Where(iv => iv.ItemId == itemId).OrderBy(iv => iv.Value).ToList();
                case ItemType.Decimal:
                    return db.ItemDecimals.AsNoTracking().Where(iv => iv.ItemId == itemId).OrderBy(iv => iv.Value).ToList();
                case ItemType.Enum:
                    return db.ItemEnums.AsNoTracking().Where(iv => iv.ItemId == itemId).OrderBy(iv => iv.Order).ThenBy(iv => iv.Value).ToList();
                case ItemType.Integer:
                    return db.ItemIntegers.AsNoTracking().Where(iv => iv.ItemId == itemId).OrderBy(iv => iv.Value).ToList();
                case ItemType.String:
                    return db.ItemStrings.AsNoTracking().Where(iv => iv.ItemId == itemId)/*.OrderBy(iv => iv.Value)*/.ToList();
                default:
                    return new List<object>();
            }
        }

        public bool DeleteValue(DatabaseContext db, int itemValueId, ItemType type)
        {
            switch (type)
            {
                case ItemType.Boolean:
                    return db.ProductPartBooleans.Any(pp => pp.ItemValueId == itemValueId) == false
                        && db.ItemBooleans.Where(iv => iv.Id == itemValueId).Delete() > 0;
                case ItemType.Date:
                    return db.ProductPartDates.Any(pp => pp.ItemValueId == itemValueId) == false
                        && db.ItemDates.Where(iv => iv.Id == itemValueId).Delete() > 0;
                case ItemType.Decimal:
                    return db.ProductPartDecimals.Any(pp => pp.ItemValueId == itemValueId) == false
                        && db.ItemDecimals.Where(iv => iv.Id == itemValueId).Delete() > 0;
                case ItemType.Enum:
                    return db.ProductPartEnums.Any(pp => pp.ItemValueId == itemValueId) == false
                        && db.ItemEnums.Where(iv => iv.Id == itemValueId).Delete() > 0;
                case ItemType.Integer:
                    return db.ProductPartIntegers.Any(pp => pp.ItemValueId == itemValueId) == false
                        && db.ItemIntegers.Where(iv => iv.Id == itemValueId).Delete() > 0;
                case ItemType.String:
                    return db.ProductPartStrings.Any(pp => pp.ItemValueId == itemValueId) == false
                        && db.ItemStrings.Where(iv => iv.Id == itemValueId).Delete() > 0;
                default:
                    return false;
            }
        }
    }
}
