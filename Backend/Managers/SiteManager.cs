﻿using Backend.Data.Context;
using Backend.Data.Entities;
using Backend.Exceptions;
using Backend.Utils.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Z.EntityFramework.Plus;

namespace Backend.Managers
{
    public class SiteManager
    {
        public const string RegexPattern = @"^([a-zA-Z0-9][a-zA-Z0-9-_]*[a-zA-Z0-9]\.)+[a-zA-Z]{2,11}$";

        public Site Create(DatabaseContext db, string domain, bool allowed)
        {
            if (db.Sites.Any(s => s.Domain == domain))
                throw new HttpBadRequestException($"Site {domain} already exists.");

            var site = new Site() {
                Domain = domain,
                Allowed = allowed
            };
            db.Sites.Add(site);
            db.SaveChanges();
            return site;
        }

        public bool Update(DatabaseContext db, int id, string domain, bool allowed)
        {
            if (db.Sites.Any(s => s.Id != id && s.Domain == domain))
                throw new HttpBadRequestException($"Site {domain} already exists.");

            return db.Sites.Where(s => s.Id == id).Update(s => new Site() { Allowed = allowed }) > 0;
        }

        public List<string> ReadonlyDomainList(DatabaseContext db, bool allowed)
        {
            return db.Sites.Where(s => s.Allowed == allowed).Select(s => s.Domain).ToList();
        }

        public PaginatedCollection<Site> ReadCollection(DatabaseContext db, int? id, string domain, bool? allowed, int page, int limit)
        {
            var query = db.Sites.AsNoTracking();

            if (id.HasValue)
                query = query.Where(s => s.Id == id.Value);
            if (allowed.HasValue)
                query = query.Where(s => s.Allowed == allowed.Value);
            if (domain?.Length > 2)
                query = query.Where(s => EF.Functions.Like(s.Domain, $"%{domain}%"));

            return new PaginatedCollection<Site>(query.OrderBy(i => i.Domain), page, limit);
        }

        public bool Delete(DatabaseContext db, int id)
        {
            return db.Sites.Where(s => s.Id == id).Delete() > 0;
        }
    }
}
