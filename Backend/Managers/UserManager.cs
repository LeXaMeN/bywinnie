﻿using Backend.Data.Context;
using Backend.Data.Entities;
using Backend.Exceptions;
using Backend.Utils.Collections.Generic;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
//using Novel.Collections.Generic;
//using Novel.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Z.EntityFramework.Plus;

namespace Backend.Managers
{
    public class UserManager
    {
        private readonly IConfiguration Configuration;

        public UserManager(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public User Create(DatabaseContext db, string login, string password, bool adminRole)
        {
            if (db.Users.Any(u => u.Login == login))
                throw new ArgumentException($"Nickname {login} already exists.");

            var user = new User() {
                Login = login,
                Password = new PasswordHasher<User>().HashPassword(null, password),
                RolesJson = adminRole ? $"[\"{UserRole.User}\",\"{UserRole.Admin}\"]" : $"[\"{UserRole.User}\"]"
            };

            db.Users.Add(user);
            db.SaveChanges();

            return user;
        }

        public bool UpdatePassword(DatabaseContext db, int id, string newPassword)
        {
            var hash = new PasswordHasher<User>().HashPassword(null, newPassword);
            return db.Users.Where(u => u.Id == id).Update(u => new User() { Password = hash }) > 0;
        }

        public bool IsValidPassword(string hashedPassword, string password)
        {
            return new PasswordHasher<User>().VerifyHashedPassword(null, hashedPassword, password) != PasswordVerificationResult.Failed;
        }

        public bool ExistAny(DatabaseContext db, string login, out int userId)
        {
            userId = db.Users.Where(u => u.Login == login).Select(u => u.Id).FirstOrDefault();
            return userId > 0;
        }

        public bool ExistAny(DatabaseContext db, int id, string password)
        {
            var hashedPassword = db.Users.Where(u => u.Id == id).Select(u => u.Password).FirstOrDefault();
            return hashedPassword?.Length > 0 && IsValidPassword(hashedPassword, password);
        }

        public bool ExistAny(DatabaseContext db, int id)
        {
            return db.Users.Any(u => u.Id == id);
        }

        public string ReadLogin(DatabaseContext db, int id)
        {
            var login = db.Users.Where(u => u.Id == id).Select(u => u.Login).FirstOrDefault();

            if (string.IsNullOrEmpty(login))
                throw new HttpNotFoundException(nameof(User), id);

            return login;
        }

        public User ReadonlyOrNull(DatabaseContext db, int id, string password = null)
        {
            var user = db.Users.AsNoTracking().Where(u => u.Id == id).OrderBy(u => u.Id).FirstOrDefault();
            if (user != null && password?.Length > 0 && new PasswordHasher<User>().VerifyHashedPassword(null, user.Password, password) == PasswordVerificationResult.Failed)
            {
                return null;
            }
            return user;
        }

        public User ReadonlyOrNull(DatabaseContext db, string login, string password = null)
        {
            var user = db.Users.AsNoTracking().Where(u => u.Login == login).OrderBy(u => u.Login).FirstOrDefault();
            if (user != null && password?.Length > 0 && new PasswordHasher<User>().VerifyHashedPassword(null, user.Password, password) == PasswordVerificationResult.Failed)
            {
                return null;
            }
            return user;
        }

        public User Readonly(DatabaseContext db, int id)
        {
            var user = ReadonlyOrNull(db, id);

            if (user == null)
                throw new HttpNotFoundException(nameof(User), id);

            return user;
        }

        public User ReadonlyAsPublic(DatabaseContext db, int id)
        {
            var user = db.Users.AsNoTracking().Where(u => u.Id == id).OrderBy(u => u.Id).Select(u => new User() {
                Id = u.Id,
                Login = u.Login
            }).FirstOrDefault();

            return user ?? throw new HttpNotFoundException(nameof(User), id); ;
        }

        public PaginatedCollection<User> ReadCollection(DatabaseContext db, int? userId, string login, string role, int page, int limit)
        {
            var query = db.Users.AsNoTracking();

            if (userId.HasValue)
                query = query.Where(u => u.Id == userId.Value);
            if (!string.IsNullOrWhiteSpace(login))
                query = query.Where(u => u.Login.Contains(login));
            if (UserRole.IsValid(role))
                query = query.Where(u => u.RolesJson.Contains($"\"{role}\""));

            return new PaginatedCollection<User>(query, page, limit);
        }
    }
}
