﻿using Backend.Data.Context;
using Backend.Data.Entities;
using Backend.Extensions.Database;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Z.EntityFramework.Plus;

namespace Backend.Managers
{
    public class ProductRatingManager
    {
        private static readonly object Lock = new object();

        public ProductRating CreateOrUpdate(DatabaseContext db, int productId, decimal receivedUSD, ushort salesCount)
        {
            var entity = db.Database.Transaction(() =>
            {
                var rating = db.ProductRatings
                    .FromSql($"SELECT * FROM `ProductRating` WHERE ProductId = {productId} LIMIT 1 FOR UPDATE")
                    .OrderBy(pr => pr.ProductId).FirstOrDefault();

                if (rating != null)
                {
                    rating.ReceivedUSD = receivedUSD;
                    rating.SalesCount = salesCount;
                    rating.UpdatedAt = DateTime.UtcNow;
                    //rating.RemovedAt = null;
                    db.SaveChanges();
                }

                return rating;
            });

            if (entity == null)
            {
                lock (Lock)
                {
                    entity = db.ProductRatings
                        .FromSql($"SELECT * FROM `ProductRating` WHERE ProductId = {productId} LIMIT 1 LOCK IN SHARE MODE")
                        .OrderBy(pr => pr.ProductId).FirstOrDefault();

                    if (entity == null)
                    {
                        entity = new ProductRating() {
                            ProductId = productId,
                            ReceivedUSD = receivedUSD,
                            SalesCount = salesCount,
                            UpdatedAt = DateTime.UtcNow
                        };
                        db.ProductRatings.Add(entity);
                    }
                    db.SaveChanges();
                }
            }

            return entity;
        }

        public List<ProductRating> ReadonlyList(DatabaseContext db, int? mainCategoryId, int limit)
        {
            var query = db.ProductRatings.AsNoTracking();

            query = query.Include(pr => pr.Product).ThenInclude(p => p.Category);
            if (mainCategoryId.HasValue)
                query = query.Where(pr => pr.Product.Category.ParentId == mainCategoryId.Value);

            return query.OrderByDescending(pr => pr.SalesCount).Take(limit).ToList();
        }

        public bool Delete(DatabaseContext db, DateTime olderThanDate)
        {
            //return db.ProductRatings.Where(pr => pr.UpdatedAt < olderThanDate && pr.RemovedAt == null).Update(pr => new ProductRating() { RemovedAt = DateTime.UtcNow }) > 0;
            return db.ProductRatings.Where(pr => pr.UpdatedAt < olderThanDate).Delete() > 0;
        }
    }
}
