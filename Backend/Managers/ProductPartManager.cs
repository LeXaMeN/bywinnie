﻿using Backend.Data.Context;
using Backend.Data.Entities;
using Backend.Extensions.Database;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Z.EntityFramework.Plus;

namespace Backend.Managers
{
    public class ProductPartManager
    {
        public ProductPartCategory CreateCategory(DatabaseContext db, string name, string nameRus)
        {
            var category = new ProductPartCategory() { Name = name, NameRus = nameRus };
            db.ProductPartCategories.Add(category);
            db.SaveChanges();
            return category;
        }

        public ProductPartCategory ReadonlyCategoryOrNull(DatabaseContext db, int productPartCategoryId)
        {
            return db.ProductPartCategories.AsNoTracking().Where(ppc => ppc.Id == productPartCategoryId).OrderBy(ppc => ppc.Id).FirstOrDefault();
        }

        public List<ProductPartCategory> ReadonlyCategoryList(DatabaseContext db)
        {
            return db.ProductPartCategories.AsNoTracking().OrderBy(ppc => ppc.Name).ToList();
        }

        public List<ProductPartCategory> ReadonlyCategoryList(DatabaseContext db, List<int> ids)
        {
            return ids.Count > 0 ? ids.Select(id => ReadonlyCategoryOrNull(db, id)).Where(ppc => ppc != null).ToList() : new List<ProductPartCategory>();
        }

        public bool UpdateCategory(DatabaseContext db, int productPartCategoryId, string name, string nameRus)
        {
            return db.ProductPartCategories.Where(ppc => ppc.Id == productPartCategoryId).Update(ppc => new ProductPartCategory() { Name = name, NameRus = nameRus }) > 0;
        }

        public bool DeleteCategory(DatabaseContext db, int productPartCategoryId)
        {
            return db.ProductPartBooleans.Any(ppv => ppv.CategoryId == productPartCategoryId) == false
                && db.ProductPartDates.Any(ppv => ppv.CategoryId == productPartCategoryId) == false
                && db.ProductPartDecimals.Any(ppv => ppv.CategoryId == productPartCategoryId) == false
                && db.ProductPartEnums.Any(ppv => ppv.CategoryId == productPartCategoryId) == false
                && db.ProductPartIntegers.Any(ppv => ppv.CategoryId == productPartCategoryId) == false
                && db.ProductPartStrings.Any(ppv => ppv.CategoryId == productPartCategoryId) == false
                && db.ProductPartCategories.Where(ppc => ppc.Id == productPartCategoryId).Delete() > 0;
        }

        //private static readonly IReadOnlyDictionary<Type, ItemType> ItemValueTypes = new Dictionary<Type, ItemType>() {
        //    { typeof(ProductPartBoolean), ItemType.Boolean },
        //    { typeof(ProductPartDate), ItemType.Date },
        //    { typeof(ProductPartDecimal), ItemType.Decimal },
        //    { typeof(ProductPartEnum), ItemType.Enum },
        //    { typeof(ProductPartInteger), ItemType.Integer },
        //    { typeof(ProductPartString), ItemType.String }
        //};
        //private ItemType GetItemValueType<TProductPartValue>() where TProductPartValue : IProductPartValue => ItemValueTypes[typeof(TProductPartValue)];
        //private ItemType GetItemValueType(IProductPartValue productPartValue) => ItemValueTypes[productPartValue.GetType()];

        public IProductPartValue CreateValue(DatabaseContext db, int productId, int productPartCategoryId, int ItemValueId, ItemType itemType)
        {
            var value = default(IProductPartValue);
            switch (itemType)
            {
                case ItemType.Boolean:
                    value = db.ProductPartBooleans.Add(new ProductPartBoolean() { ProductId = productId, CategoryId = productPartCategoryId, ItemValueId = ItemValueId }).Entity;
                    break;
                case ItemType.Date:
                    value = db.ProductPartDates.Add(new ProductPartDate() { ProductId = productId, CategoryId = productPartCategoryId, ItemValueId = ItemValueId }).Entity;
                    break;
                case ItemType.Decimal:
                    value = db.ProductPartDecimals.Add(new ProductPartDecimal() { ProductId = productId, CategoryId = productPartCategoryId, ItemValueId = ItemValueId }).Entity;
                    break;
                case ItemType.Enum:
                    value = db.ProductPartEnums.Add(new ProductPartEnum() { ProductId = productId, CategoryId = productPartCategoryId, ItemValueId = ItemValueId }).Entity;
                    break;
                case ItemType.Integer:
                    value = db.ProductPartIntegers.Add(new ProductPartInteger() { ProductId = productId, CategoryId = productPartCategoryId, ItemValueId = ItemValueId }).Entity;
                    break;
                case ItemType.String:
                    value = db.ProductPartStrings.Add(new ProductPartString() { ProductId = productId, CategoryId = productPartCategoryId, ItemValueId = ItemValueId }).Entity;
                    break;
                default:
                    return null;
            }
            db.SaveChanges();
            return value;
        }

        public Dictionary<ItemType, IList> ReadonlyValueDictionary(DatabaseContext db, int productId)
        {
            return new Dictionary<ItemType, IList>() {
                { ItemType.Boolean, db.ProductPartBooleans.AsNoTracking().Where(ppv => ppv.ProductId == productId).Include(ppv => ppv.Category).Include(ppv => ppv.ItemValue).ThenInclude(iv => iv.Item).ToList() },
                { ItemType.Date, db.ProductPartDates.AsNoTracking().Where(ppv => ppv.ProductId == productId).Include(ppv => ppv.Category).Include(ppv => ppv.ItemValue).ThenInclude(iv => iv.Item).ToList() },
                { ItemType.Decimal, db.ProductPartDecimals.AsNoTracking().Where(ppv => ppv.ProductId == productId).Include(ppv => ppv.Category).Include(ppv => ppv.ItemValue).ThenInclude(iv => iv.Item).ToList() },
                { ItemType.Enum, db.ProductPartEnums.AsNoTracking().Where(ppv => ppv.ProductId == productId).Include(ppv => ppv.Category).Include(ppv => ppv.ItemValue).ThenInclude(iv => iv.Item).ToList() },
                { ItemType.Integer, db.ProductPartIntegers.AsNoTracking().Where(ppv => ppv.ProductId == productId).Include(ppv => ppv.Category).Include(ppv => ppv.ItemValue).ThenInclude(iv => iv.Item).ToList() },
                { ItemType.String, db.ProductPartStrings.AsNoTracking().Where(ppv => ppv.ProductId == productId).Include(ppv => ppv.Category).Include(ppv => ppv.ItemValue).ThenInclude(iv => iv.Item).ToList() }
            };
        }

        public List<IProductPartValue> ReadonlyValueList(DatabaseContext db, int productId)
        {
            var values = new List<IProductPartValue>();
            values.AddRange(db.ProductPartBooleans.AsNoTracking().Where(ppv => ppv.ProductId == productId).Include(ppv => ppv.Category).Include(ppv => ppv.ItemValue).ThenInclude(iv => iv.Item).ToList());
            values.AddRange(db.ProductPartDates.AsNoTracking().Where(ppv => ppv.ProductId == productId).Include(ppv => ppv.Category).Include(ppv => ppv.ItemValue).ThenInclude(iv => iv.Item).ToList());
            values.AddRange(db.ProductPartDecimals.AsNoTracking().Where(ppv => ppv.ProductId == productId).Include(ppv => ppv.Category).Include(ppv => ppv.ItemValue).ThenInclude(iv => iv.Item).ToList());
            values.AddRange(db.ProductPartEnums.AsNoTracking().Where(ppv => ppv.ProductId == productId).Include(ppv => ppv.Category).Include(ppv => ppv.ItemValue).ThenInclude(iv => iv.Item).ToList());
            values.AddRange(db.ProductPartIntegers.AsNoTracking().Where(ppv => ppv.ProductId == productId).Include(ppv => ppv.Category).Include(ppv => ppv.ItemValue).ThenInclude(iv => iv.Item).ToList());
            values.AddRange(db.ProductPartStrings.AsNoTracking().Where(ppv => ppv.ProductId == productId).Include(ppv => ppv.Category).Include(ppv => ppv.ItemValue).ThenInclude(iv => iv.Item).ToList());
            return values;
        }

        public bool DeleteValue(DatabaseContext db, int productPartValueId, int productId, int productPartCategoryId, int ItemValueId, ItemType itemType)
        {
            switch (itemType)
            {
                case ItemType.Boolean:
                    return db.ProductPartBooleans.Where(ppv => ppv.Id == productPartValueId && ppv.ProductId == productId && ppv.CategoryId == productPartCategoryId && ppv.ItemValueId == ItemValueId).Delete() > 0;
                case ItemType.Date:
                    return db.ProductPartDates.Where(ppv => ppv.Id == productPartValueId && ppv.ProductId == productId && ppv.CategoryId == productPartCategoryId && ppv.ItemValueId == ItemValueId).Delete() > 0;
                case ItemType.Decimal:
                    return db.ProductPartDecimals.Where(ppv => ppv.Id == productPartValueId && ppv.ProductId == productId && ppv.CategoryId == productPartCategoryId && ppv.ItemValueId == ItemValueId).Delete() > 0;
                case ItemType.Enum:
                    return db.ProductPartEnums.Where(ppv => ppv.Id == productPartValueId && ppv.ProductId == productId && ppv.CategoryId == productPartCategoryId && ppv.ItemValueId == ItemValueId).Delete() > 0;
                case ItemType.Integer:
                    return db.ProductPartIntegers.Where(ppv => ppv.Id == productPartValueId && ppv.ProductId == productId && ppv.CategoryId == productPartCategoryId && ppv.ItemValueId == ItemValueId).Delete() > 0;
                case ItemType.String:
                    return db.ProductPartStrings.Where(ppv => ppv.Id == productPartValueId && ppv.ProductId == productId && ppv.CategoryId == productPartCategoryId && ppv.ItemValueId == ItemValueId).Delete() > 0;
                default:
                    return false;
            }
        }

        private static readonly object Lock = new object();

        public ProductPartSequence CreateOrUpdateSequence(DatabaseContext db, int categoryId, List<int> categories, Dictionary<int, List<int>> productPartCategoryIdDictionaryWithItemIdLists)
        {
            var entity = db.Database.Transaction(() =>
            {
                var sequence = db.ProductPartSequences
                    .FromSql($"SELECT * FROM `ProductPartSequence` WHERE CategoryId = {categoryId} LIMIT 1 FOR UPDATE")
                    .OrderBy(pps => pps.CategoryId).FirstOrDefault();

                if (sequence != null)
                {
                    sequence.Categories = categories;
                    sequence.Dictionary = productPartCategoryIdDictionaryWithItemIdLists;
                    db.SaveChanges();
                }
                return sequence;
            });

            if (entity == null)
            {
                lock (Lock)
                {
                    entity = db.ProductPartSequences
                        .FromSql($"SELECT * FROM `ProductPartSequence` WHERE CategoryId = {categoryId} LIMIT 1 LOCK IN SHARE MODE")
                        .OrderBy(pps => pps.CategoryId).FirstOrDefault();

                    if (entity == null)
                    {
                        entity = new ProductPartSequence() {
                            CategoryId = categoryId,
                            Categories = categories,
                            Dictionary = productPartCategoryIdDictionaryWithItemIdLists
                        };
                        db.ProductPartSequences.Add(entity);
                        db.SaveChanges();
                    }
                }
            }

            return entity;
        }

        public ProductPartSequence ReadonlySequenceOrNull(DatabaseContext db, int categoryId)
        {
            return db.ProductPartSequences.AsNoTracking().Where(pps => pps.CategoryId == categoryId).OrderBy(pps => pps.CategoryId).FirstOrDefault(); ;
        }

        public IEnumerable<ProductPartCategory> ReadonlySequenceCategories(DatabaseContext db, List<int> productIds)
        {
            var ppcs = db.ProductPartBooleans.AsNoTracking().Where(ppv => productIds.Contains(ppv.ProductId)).Select(ppv => ppv.Category).GroupBy(ppc => ppc.Id).Select(g => g.First()).ToList();
            ppcs.AddRange(db.ProductPartDates.AsNoTracking().Where(ppv => productIds.Contains(ppv.ProductId)).Select(ppv => ppv.Category).GroupBy(ppc => ppc.Id).Select(g => g.First()).ToList());
            ppcs.AddRange(db.ProductPartDecimals.AsNoTracking().Where(ppv => productIds.Contains(ppv.ProductId)).Select(ppv => ppv.Category).GroupBy(ppc => ppc.Id).Select(g => g.First()).ToList());
            ppcs.AddRange(db.ProductPartEnums.AsNoTracking().Where(ppv => productIds.Contains(ppv.ProductId)).Select(ppv => ppv.Category).GroupBy(ppc => ppc.Id).Select(g => g.First()).ToList());
            ppcs.AddRange(db.ProductPartIntegers.AsNoTracking().Where(ppv => productIds.Contains(ppv.ProductId)).Select(ppv => ppv.Category).GroupBy(ppc => ppc.Id).Select(g => g.First()).ToList());
            ppcs.AddRange(db.ProductPartStrings.AsNoTracking().Where(ppv => productIds.Contains(ppv.ProductId)).Select(ppv => ppv.Category).GroupBy(ppc => ppc.Id).Select(g => g.First()).ToList());
            return ppcs.GroupBy(ppc => ppc.Id).Select(g => g.First());
        }

        public IEnumerable<(int, Item)> ReadonlySequenceItems(DatabaseContext db, List<int> productIds)
        {
            var items = db.ProductPartBooleans.AsNoTracking().Where(ppv => productIds.Contains(ppv.ProductId)).Select(ppv => new { ppv.CategoryId, ppv.ItemValue.Item }).GroupBy(i => new { i.CategoryId, i.Item.Id }).Select(g => g.First()).ToList();
            items.AddRange(db.ProductPartDates.AsNoTracking().Where(ppv => productIds.Contains(ppv.ProductId)).Select(ppv => new { ppv.CategoryId, ppv.ItemValue.Item }).GroupBy(i => new { i.CategoryId, i.Item.Id }).Select(g => g.First()).ToList());
            items.AddRange(db.ProductPartDecimals.AsNoTracking().Where(ppv => productIds.Contains(ppv.ProductId)).Select(ppv => new { ppv.CategoryId, ppv.ItemValue.Item }).GroupBy(i => new { i.CategoryId, i.Item.Id }).Select(g => g.First()).ToList());
            items.AddRange(db.ProductPartEnums.AsNoTracking().Where(ppv => productIds.Contains(ppv.ProductId)).Select(ppv => new { ppv.CategoryId, ppv.ItemValue.Item }).GroupBy(i => new { i.CategoryId, i.Item.Id }).Select(g => g.First()).ToList());
            items.AddRange(db.ProductPartIntegers.AsNoTracking().Where(ppv => productIds.Contains(ppv.ProductId)).Select(ppv => new { ppv.CategoryId, ppv.ItemValue.Item }).GroupBy(i => new { i.CategoryId, i.Item.Id }).Select(g => g.First()).ToList());
            items.AddRange(db.ProductPartStrings.AsNoTracking().Where(ppv => productIds.Contains(ppv.ProductId)).Select(ppv => new { ppv.CategoryId, ppv.ItemValue.Item }).GroupBy(i => new { i.CategoryId, i.Item.Id }).Select(g => g.First()).ToList());
            return items.Select(i => (i.CategoryId, i.Item));
        }

        public bool DeleteSequence(DatabaseContext db, int categoryId)
        {
            return db.Products.Any(p => p.CategoryId == categoryId) == false
                && db.ProductPartSequences.Where(pps => pps.CategoryId == categoryId).Delete() > 0;
        }
    }
}
