﻿using Backend.Data.Context;
using Backend.Data.Entities;
using Backend.Exceptions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Z.EntityFramework.Plus;

namespace Backend.Managers
{
    public class CategoryManager
    {
        public Category Create(DatabaseContext db, int? parentId, string index, string name, string nameRus)
        {
            if (db.Categories.Any(c => c.Index == index))
                throw new HttpBadRequestException($"Index \"{index}\" already exists.");

            var category = new Category() {
                ParentId = parentId,
                Index = index,
                Name = name,
                NameRus = nameRus
            };
            db.Categories.Add(category);
            db.SaveChanges();
            return category;
        }

        public bool Update(DatabaseContext db, int id, string index, string name, string nameRus)
        {
            return db.Categories.Any(c => c.Index == index && c.Id != id) == false
                && db.Categories.Where(c => c.Id == id).Update(c => new Category() { Index = index, Name = name, NameRus = nameRus }) > 0;
        }

        public Category ReadonlyOrNull(DatabaseContext db, int id)
        {
            var category = db.Categories.AsNoTracking().Where(c => c.Id == id).Include(c => c.Parent).Include(c => c.Subcategories).OrderBy(c => c.Id).FirstOrDefault();
            if (category?.Parent != null)
            {
                var parent = category.Parent;
                while (parent?.ParentId != null)
                {
                    parent.Parent = db.Categories.AsNoTracking().Where(c => c.Id == parent.ParentId.Value).OrderBy(c => c.Id).FirstOrDefault();
                    parent = parent.Parent;
                }
            }
            return category;
        }

        public List<Category> ReadonlyList(DatabaseContext db, int? parentId)
        {
            return db.Categories.AsNoTracking().Where(c => c.ParentId == parentId).Include(c => c.Subcategories).ToList();
        }

        public bool Delete(DatabaseContext db, int id)
        {
            return db.Categories.Any(c => c.ParentId == id) == false
                && db.Products.Any(p => p.CategoryId == id) == false
                && db.ProductPartSequences.Any(pps => pps.CategoryId == id) == false
                && db.Categories.Where(c => c.Id == id).Delete() > 0;
        }
    }
}
