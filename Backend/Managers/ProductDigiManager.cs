﻿using Backend.Data.Context;
using Backend.Data.Entities;
using Backend.Utils.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Z.EntityFramework.Plus;

namespace Backend.Managers
{
    public class ProductDigiManager
    {
        public ProductDigi Create(DatabaseContext db, int digiId, int productId, string name, byte agencyFee)
        {
            var productDigi = new ProductDigi() {
                Id = digiId,
                ProductId = productId,
                Allowed = true,
                Name = name,
                NameChanged = false,
                AgencyFee = agencyFee,
                AgencyFeeChanged = false,
                UpdatedAt = DateTime.UtcNow,
            };
            db.ProductDigis.Add(productDigi);
            db.SaveChanges();
            return productDigi;
        }

        public bool Update(DatabaseContext db, int digiId, bool allowed, string name, bool nameChanged, byte agencyFee, bool agencyFeeChanged)
        {
            return db.ProductDigis
                .Where(pd => pd.Id == digiId)
                .Update(pd => new ProductDigi() { Allowed = allowed, Name = name, NameChanged = nameChanged, AgencyFee = agencyFee, AgencyFeeChanged = agencyFeeChanged, UpdatedAt = DateTime.UtcNow }) > 0;
        }

        public bool ExistAny(DatabaseContext db, int digiId)
        {
            return db.ProductDigis.Any(pd => pd.Id == digiId);
        }

        public ProductDigi ReadOrNull(DatabaseContext db, int digiId)
        {
            return db.ProductDigis.Where(pd => pd.Id == digiId).OrderBy(pd => pd.Id).FirstOrDefault();
        }

        public int? ReadProductIdOrNull(DatabaseContext db, int digiId)
        {
            return db.ProductDigis.Where(pd => pd.Id == digiId).OrderBy(pd => pd.Id).Select(pd => new { pd.ProductId }).FirstOrDefault()?.ProductId;
        }

        public ProductDigi ReadonlyOrNull(DatabaseContext db, int digiId)
        {
            return db.ProductDigis.AsNoTracking().Where(pd => pd.Id == digiId).OrderBy(pd => pd.Id).FirstOrDefault();
        }

        public List<ProductDigi> ReadonlyList(DatabaseContext db, int productId)
        {
            return db.ProductDigis.AsNoTracking().Where(pd => pd.ProductId == productId).ToList();
        }

        public PaginatedCollection<ProductDigi> ReadCollection(DatabaseContext db, int? id, int? productId, bool allowed, bool nameChanged, bool? agencyFeeChanged, int page, int limit)
        {
            var query = db.ProductDigis.AsNoTracking();

            if (id.HasValue)
                query = query.Where(pd => pd.Id == id.Value);
            if (productId.HasValue)
                query = query.Where(pd => pd.ProductId == productId.Value);

            if (agencyFeeChanged.HasValue)
                query = query.Where(pd => pd.Allowed == allowed && pd.NameChanged == nameChanged && pd.AgencyFeeChanged == agencyFeeChanged.Value);
            else
                query = query.Where(pd => pd.Allowed == allowed && pd.NameChanged == nameChanged);

            return new PaginatedCollection<ProductDigi>(query.OrderBy(pd => pd.ProductId), page, limit);
        }

        public List<int> ReadonlyIdList(DatabaseContext db, int productId, bool onlyAvailable = false)
        {
            return onlyAvailable
                ? db.ProductDigis.Where(pd => pd.ProductId == productId && pd.Allowed && !pd.NameChanged && !pd.AgencyFeeChanged).Select(pd => pd.Id).ToList()
                : db.ProductDigis.Where(pd => pd.ProductId == productId).Select(pd => pd.Id).ToList();
        }

        public List<Product> ReadonlySortedByAgencyFeeProductList(DatabaseContext db, int? mainCategoryId, int limit)
        {
            var products = new List<Product>(limit);
            using (var command = db.Database.GetDbConnection().CreateCommand())
            {
                // https://stackoverflow.com/a/57646090
                var connectionWasOpen = command.Connection.State == ConnectionState.Open;
                if (!connectionWasOpen) command.Connection.Open();
                try
                {
                    command.CommandText = $@"
                        SELECT `pd.Product`.*, AVG(AgencyFee) AS Average
                        FROM `ProductDigi` AS `pd`
                        INNER JOIN `Product` AS `pd.Product` ON `pd`.ProductId = `pd.Product`.Id
                        INNER JOIN `Category` AS `pd.Product.Category` ON `pd.Product`.CategoryId = `pd.Product.Category`.Id
                        WHERE {(mainCategoryId > 0 ? "`pd.Product.Category`.ParentId = @categoryId AND " : "")}
                        `pd`.Allowed = TRUE AND `pd`.NameChanged = FALSE AND `pd`.AgencyFeeChanged = FALSE
                        GROUP BY `pd`.ProductId
                        ORDER BY Average DESC LIMIT @limit";
                    if (mainCategoryId > 0)
                    {
                        command.Parameters.Add(new MySqlParameter("@categoryId", mainCategoryId.Value));
                    }
                    command.Parameters.Add(new MySqlParameter("@limit", limit));
                    db.Database.OpenConnection();
                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            var oId = reader.GetOrdinal(nameof(Product.Id));
                            var oCategoryId = reader.GetOrdinal(nameof(Product.CategoryId));
                            var oIndex = reader.GetOrdinal(nameof(Product.Index));
                            var oName = reader.GetOrdinal(nameof(Product.Name));
                            var oNameRus = reader.GetOrdinal(nameof(Product.NameRus));
                            var oDescription = reader.GetOrdinal(nameof(Product.Description));
                            var oDescriptionRus = reader.GetOrdinal(nameof(Product.DescriptionRus));
                            var oPrice = reader.GetOrdinal(nameof(Product.Price));
                            while (reader.Read())
                            {
                                products.Add(new Product() {
                                    Id = reader.GetInt32(oId),
                                    CategoryId = reader.GetInt32(oCategoryId),
                                    Index = reader.GetString(oIndex),
                                    Name = reader.GetString(oName),
                                    NameRus = reader.GetString(oNameRus),
                                    Description = reader.GetString(oDescription),
                                    DescriptionRus = reader.GetString(oDescriptionRus),
                                    Price = reader.GetDecimal(oPrice)
                                });
                            }
                        }
                    }
                }
                finally
                {
                    if (connectionWasOpen == false) command.Connection.Close();
                }
            }
            return products;
        }

        public bool Delete(DatabaseContext db, int digiId, int productId)
        {
            return db.ProductDigis.Where(pd => pd.Id == digiId && pd.ProductId == productId).Delete() > 0;
        }
    }
}
