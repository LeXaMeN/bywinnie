﻿using Backend.Data.Context;
using Backend.Data.Entities;
using Backend.Utils.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Z.EntityFramework.Plus;

namespace Backend.Managers
{
    public class ProductFileManager
    {
        public ProductFile Create(DatabaseContext db, int productId, ProductFileType type, string url, short order)
        {
            var productFile = new ProductFile() {
                ProductId = productId,
                Type = type,
                Url = url,
                Order = order
            };
            db.ProductFiles.Add(productFile);
            db.SaveChanges();
            return productFile;
        }

        public bool Update(DatabaseContext db, int id, int productId, ProductFileType type, string url, short order)
        {
            return db.ProductFiles
                .Where(pf => pf.Id == id && pf.ProductId == productId)
                .Update(pf => new ProductFile() { Type = type, Url = url, Order = order }) > 0;
        }

        public ProductFile ReadonlyOrNull(DatabaseContext db, int id, int productId)
        {
            return db.ProductFiles.AsNoTracking().Where(pf => pf.Id == id && pf.ProductId == productId).OrderBy(p => p.Id).FirstOrDefault();
        }

        public List<ProductFile> ReadonlyList(DatabaseContext db, int productId, ProductFileType? type)
        {
            var query = db.ProductFiles.AsNoTracking()
                .Where(pf => pf.ProductId == productId);

            if (type.HasValue)
                query = query.Where(pf => pf.Type == type.Value);

            return query.OrderBy(p => p.Order).ToList();
        }

        public bool Delete(DatabaseContext db, int id, int productId)
        {
            return db.ProductFiles.Where(pf => pf.Id == id && pf.ProductId == productId).Delete() > 0;
        }
    }
}
