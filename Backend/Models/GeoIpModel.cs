﻿using Backend.Data.Context;
using Backend.Data.Entities;
using Backend.Enums;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Backend.Models
{
    public class GeoIpModel
    {
        public uint? Block { get; }

        public GeoIpModel(string ipAddress)
        {
            if (IPAddress.TryParse(ipAddress, out var address))
            {
                var bytes = address.GetAddressBytes();
                if (bytes.Length == 4)
                {
                    Block = (uint)bytes[0] * 256 * 256 * 256 + (uint)bytes[1] * 256 * 256 + (uint)bytes[2] * 256 + (uint)bytes[3];
                }
            }
        }

        public GeoIp TryGetGeoIp(DatabaseContext db)
        {
            if (Block == null)
                return null;

            // https://stackoverflow.com/questions/22125474/mysql-optimal-index-for-between-queries
            return db.GeoIps.AsNoTracking()
                //.FromSql("SELECT * FROM GeoIp WHERE {0} >= StartBlock AND {0} <= EndBlock LIMIT 1", Block.Value)
                .Where(g => Block.Value >= g.StartBlock && Block.Value <= g.EndBlock)
                .Include(g => g.GeoCountry)
                .OrderBy(g => g.Id)
                .FirstOrDefault();
        }

        public Country2Enum? TryGetCountryEnum(DatabaseContext db)
        {
            if (Block == null)
                return null;

            var country = db.GeoIps
                    .Where(g => Block.Value >= g.StartBlock && Block.Value <= g.EndBlock)
                    .Select(gIp => gIp.Country)
                    .FirstOrDefault();

            return Enum.TryParse(country, out Country2Enum country2) ? country2 : default(Country2Enum?);
        }
    }
}
