﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Enums
{
    /// https://ru.wikipedia.org/wiki/Коды_и_классификаторы_валют
    /// <summary>
    /// Коды валют.
    /// </summary>
    public enum Currency : Int16
    {
        /// <summary>
        /// Без валюты
        /// </summary>
        XXX = 999,
        /// <summary>
        /// Доллар США
        /// </summary>
        USD = 840,
        /// <summary>
        /// Российский рубль
        /// </summary>
        RUB = 643,
        /// <summary>
        /// Евро
        /// </summary>
        EUR = 978,
        /// <summary>
        /// Гривна (Украина)
        /// </summary>
        UAH = 980,
        ///// <summary>
        ///// Белорусский рубль
        ///// </summary>
        //BYN = 933,
        ///// <summary>
        ///// Тенге (Казахстан)
        ///// </summary>
        //KZT = 398,
        ///// <summary>
        ///// Золото (тройская унция)
        ///// </summary>
        //XAU = 959,
        ///// <summary>
        ///// Фунт стерлингов
        ///// </summary>
        //GBP = 826,
        ///// <summary>
        ///// Армянский драм
        ///// </summary>
        //AMD = 051,
        ///// <summary>
        ///// Юань (Китай)
        ///// </summary>
        //CNY = 156,
    }
}
