﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace Backend.Enums
{
    // https://en.wikipedia.org/wiki/ISO_3166-1
    public struct CountryEnum
    {
        public readonly Int16 Numeric;
        public Country2Enum Alpha2 => (Country2Enum)Numeric;
        public Country3Enum Alpha3 => (Country3Enum)Numeric;

        public CountryEnum(Int16 numeric)
        {
            Numeric = numeric;
        }

        public CountryEnum(Country2Enum alpha2)
        {
            Numeric = (Int16)alpha2;
        }

        public CountryEnum(Country3Enum alpha3)
        {
            Numeric = (Int16)alpha3;
        }

        // https://professorweb.ru/my/csharp/charp_theory/level6/6_7.php
        // неявная форма
        public static implicit operator Country2Enum(CountryEnum country) => country.Alpha2;
        public static implicit operator Country3Enum(CountryEnum country) => country.Alpha3;
        public static implicit operator CountryEnum(Country2Enum alpha2) => new CountryEnum(alpha2);
        public static implicit operator CountryEnum(Country3Enum alpha3) => new CountryEnum(alpha3);
        // явная форма
        public static explicit operator CountryEnum(Int16 numeric)
        {
            if (numeric < 0 || numeric > 999)
                throw new ArgumentOutOfRangeException(nameof(numeric), numeric, null);

            return new CountryEnum(numeric);
        }
        public static explicit operator int(CountryEnum country) => country.Numeric;

        public static bool operator !=(CountryEnum c1, CountryEnum c2) => c1.Numeric != c2.Numeric;
        public static bool operator ==(CountryEnum c1, CountryEnum c2) => c1.Numeric == c2.Numeric;

        public override string ToString()
        {
            return CountryEnumExtension.ToString(Numeric);
        }

        public override bool Equals(object obj)
        {
            if (obj is CountryEnum country && country.Numeric == Numeric)
                return true;
            if (obj is Country2Enum alpha2 && (int)alpha2 == Numeric)
                return true;
            if (obj is Country3Enum alpha3 && (int)alpha3 == Numeric)
                return true;

            return false;
        }

        public override int GetHashCode()
        {
            return Numeric;
        }
    }

    public enum Country2Enum : Int16
    {
        AF = 004,
        AX = 248,
        AL = 008,
        DZ = 012,
        AS = 016,
        AD = 020,
        AO = 024,
        AI = 660,
        AQ = 010,
        AG = 028,
        AR = 032,
        AM = 051,
        AW = 533,
        AU = 036,
        AT = 040,
        AZ = 031,
        BS = 044,
        BH = 048,
        BD = 050,
        BB = 052,
        BY = 112,
        BE = 056,
        BZ = 084,
        BJ = 204,
        BM = 060,
        BT = 064,
        BO = 068,
        BQ = 535,
        BA = 070,
        BW = 072,
        BV = 074,
        BR = 076,
        IO = 086,
        BN = 096,
        BG = 100,
        BF = 854,
        BI = 108,
        CV = 132,
        KH = 116,
        CM = 120,
        CA = 124,
        KY = 136,
        CF = 140,
        TD = 148,
        CL = 152,
        CN = 156,
        CX = 162,
        CC = 166,
        CO = 170,
        KM = 174,
        CG = 178,
        CD = 180,
        CK = 184,
        CR = 188,
        CI = 384,
        HR = 191,
        CU = 192,
        CW = 531,
        CY = 196,
        CZ = 203,
        DK = 208,
        DJ = 262,
        DM = 212,
        DO = 214,
        EC = 218,
        EG = 818,
        SV = 222,
        GQ = 226,
        ER = 232,
        EE = 233,
        SZ = 748,
        ET = 231,
        FK = 238,
        FO = 234,
        FJ = 242,
        FI = 246,
        FR = 250,
        GF = 254,
        PF = 258,
        TF = 260,
        GA = 266,
        GM = 270,
        GE = 268,
        DE = 276,
        GH = 288,
        GI = 292,
        GR = 300,
        GL = 304,
        GD = 308,
        GP = 312,
        GU = 316,
        GT = 320,
        GG = 831,
        GN = 324,
        GW = 624,
        GY = 328,
        HT = 332,
        HM = 334,
        VA = 336,
        HN = 340,
        HK = 344,
        HU = 348,
        IS = 352,
        IN = 356,
        ID = 360,
        IR = 364,
        IQ = 368,
        IE = 372,
        IM = 833,
        IL = 376,
        IT = 380,
        JM = 388,
        JP = 392,
        JE = 832,
        JO = 400,
        KZ = 398,
        KE = 404,
        KI = 296,
        KP = 408,
        KR = 410,
        KW = 414,
        KG = 417,
        LA = 418,
        LV = 428,
        LB = 422,
        LS = 426,
        LR = 430,
        LY = 434,
        LI = 438,
        LT = 440,
        LU = 442,
        MO = 446,
        MG = 450,
        MW = 454,
        MY = 458,
        MV = 462,
        ML = 466,
        MT = 470,
        MH = 584,
        MQ = 474,
        MR = 478,
        MU = 480,
        YT = 175,
        MX = 484,
        FM = 583,
        MD = 498,
        MC = 492,
        MN = 496,
        ME = 499,
        MS = 500,
        MA = 504,
        MZ = 508,
        MM = 104,
        NA = 516,
        NR = 520,
        NP = 524,
        NL = 528,
        NC = 540,
        NZ = 554,
        NI = 558,
        NE = 562,
        NG = 566,
        NU = 570,
        NF = 574,
        MK = 807,
        MP = 580,
        NO = 578,
        OM = 512,
        PK = 586,
        PW = 585,
        PS = 275,
        PA = 591,
        PG = 598,
        PY = 600,
        PE = 604,
        PH = 608,
        PN = 612,
        PL = 616,
        PT = 620,
        PR = 630,
        QA = 634,
        RE = 638,
        RO = 642,
        RU = 643,
        RW = 646,
        BL = 652,
        SH = 654,
        KN = 659,
        LC = 662,
        MF = 663,
        PM = 666,
        VC = 670,
        WS = 882,
        SM = 674,
        ST = 678,
        SA = 682,
        SN = 686,
        RS = 688,
        SC = 690,
        SL = 694,
        SG = 702,
        SX = 534,
        SK = 703,
        SI = 705,
        SB = 090,
        SO = 706,
        ZA = 710,
        GS = 239,
        SS = 728,
        ES = 724,
        LK = 144,
        SD = 729,
        SR = 740,
        SJ = 744,
        SE = 752,
        CH = 756,
        SY = 760,
        TW = 158,
        TJ = 762,
        TZ = 834,
        TH = 764,
        TL = 626,
        TG = 768,
        TK = 772,
        TO = 776,
        TT = 780,
        TN = 788,
        TR = 792,
        TM = 795,
        TC = 796,
        TV = 798,
        UG = 800,
        UA = 804,
        AE = 784,
        GB = 826,
        US = 840,
        UM = 581,
        UY = 858,
        UZ = 860,
        VU = 548,
        VE = 862,
        VN = 704,
        VG = 092,
        VI = 850,
        WF = 876,
        EH = 732,
        YE = 887,
        ZM = 894,
        ZW = 716
    }

    public enum Country3Enum : Int16
    {
        AFG = 004,
        ALA = 248,
        ALB = 008,
        DZA = 012,
        ASM = 016,
        AND = 020,
        AGO = 024,
        AIA = 660,
        ATA = 010,
        ATG = 028,
        ARG = 032,
        ARM = 051,
        ABW = 533,
        AUS = 036,
        AUT = 040,
        AZE = 031,
        BHS = 044,
        BHR = 048,
        BGD = 050,
        BRB = 052,
        BLR = 112,
        BEL = 056,
        BLZ = 084,
        BEN = 204,
        BMU = 060,
        BTN = 064,
        BOL = 068,
        BES = 535,
        BIH = 070,
        BWA = 072,
        BVT = 074,
        BRA = 076,
        IOT = 086,
        BRN = 096,
        BGR = 100,
        BFA = 854,
        BDI = 108,
        CPV = 132,
        KHM = 116,
        CMR = 120,
        CAN = 124,
        CYM = 136,
        CAF = 140,
        TCD = 148,
        CHL = 152,
        CHN = 156,
        CXR = 162,
        CCK = 166,
        COL = 170,
        COM = 174,
        COG = 178,
        COD = 180,
        COK = 184,
        CRI = 188,
        CIV = 384,
        HRV = 191,
        CUB = 192,
        CUW = 531,
        CYP = 196,
        CZE = 203,
        DNK = 208,
        DJI = 262,
        DMA = 212,
        DOM = 214,
        ECU = 218,
        EGY = 818,
        SLV = 222,
        GNQ = 226,
        ERI = 232,
        EST = 233,
        SWZ = 748,
        ETH = 231,
        FLK = 238,
        FRO = 234,
        FJI = 242,
        FIN = 246,
        FRA = 250,
        GUF = 254,
        PYF = 258,
        ATF = 260,
        GAB = 266,
        GMB = 270,
        GEO = 268,
        DEU = 276,
        GHA = 288,
        GIB = 292,
        GRC = 300,
        GRL = 304,
        GRD = 308,
        GLP = 312,
        GUM = 316,
        GTM = 320,
        GGY = 831,
        GIN = 324,
        GNB = 624,
        GUY = 328,
        HTI = 332,
        HMD = 334,
        VAT = 336,
        HND = 340,
        HKG = 344,
        HUN = 348,
        ISL = 352,
        IND = 356,
        IDN = 360,
        IRN = 364,
        IRQ = 368,
        IRL = 372,
        IMN = 833,
        ISR = 376,
        ITA = 380,
        JAM = 388,
        JPN = 392,
        JEY = 832,
        JOR = 400,
        KAZ = 398,
        KEN = 404,
        KIR = 296,
        PRK = 408,
        KOR = 410,
        KWT = 414,
        KGZ = 417,
        LAO = 418,
        LVA = 428,
        LBN = 422,
        LSO = 426,
        LBR = 430,
        LBY = 434,
        LIE = 438,
        LTU = 440,
        LUX = 442,
        MAC = 446,
        MDG = 450,
        MWI = 454,
        MYS = 458,
        MDV = 462,
        MLI = 466,
        MLT = 470,
        MHL = 584,
        MTQ = 474,
        MRT = 478,
        MUS = 480,
        MYT = 175,
        MEX = 484,
        FSM = 583,
        MDA = 498,
        MCO = 492,
        MNG = 496,
        MNE = 499,
        MSR = 500,
        MAR = 504,
        MOZ = 508,
        MMR = 104,
        NAM = 516,
        NRU = 520,
        NPL = 524,
        NLD = 528,
        NCL = 540,
        NZL = 554,
        NIC = 558,
        NER = 562,
        NGA = 566,
        NIU = 570,
        NFK = 574,
        MKD = 807,
        MNP = 580,
        NOR = 578,
        OMN = 512,
        PAK = 586,
        PLW = 585,
        PSE = 275,
        PAN = 591,
        PNG = 598,
        PRY = 600,
        PER = 604,
        PHL = 608,
        PCN = 612,
        POL = 616,
        PRT = 620,
        PRI = 630,
        QAT = 634,
        REU = 638,
        ROU = 642,
        RUS = 643,
        RWA = 646,
        BLM = 652,
        SHN = 654,
        KNA = 659,
        LCA = 662,
        MAF = 663,
        SPM = 666,
        VCT = 670,
        WSM = 882,
        SMR = 674,
        STP = 678,
        SAU = 682,
        SEN = 686,
        SRB = 688,
        SYC = 690,
        SLE = 694,
        SGP = 702,
        SXM = 534,
        SVK = 703,
        SVN = 705,
        SLB = 090,
        SOM = 706,
        ZAF = 710,
        SGS = 239,
        SSD = 728,
        ESP = 724,
        LKA = 144,
        SDN = 729,
        SUR = 740,
        SJM = 744,
        SWE = 752,
        CHE = 756,
        SYR = 760,
        TWN = 158,
        TJK = 762,
        TZA = 834,
        THA = 764,
        TLS = 626,
        TGO = 768,
        TKL = 772,
        TON = 776,
        TTO = 780,
        TUN = 788,
        TUR = 792,
        TKM = 795,
        TCA = 796,
        TUV = 798,
        UGA = 800,
        UKR = 804,
        ARE = 784,
        GBR = 826,
        USA = 840,
        UMI = 581,
        URY = 858,
        UZB = 860,
        VUT = 548,
        VEN = 862,
        VNM = 704,
        VGB = 092,
        VIR = 850,
        WLF = 876,
        ESH = 732,
        YEM = 887,
        ZMB = 894,
        ZWE = 716
    }

    public static class CountryEnumExtension
    {
        public static string ToString(this Country2Enum alpha2)
        {
            return ToString((int)alpha2);
        }

        public static string ToString(this Country3Enum alpha3)
        {
            return ToString((int)alpha3);
        }

        internal static string ToString(int numeric)
        {
            switch (numeric)
            {
                case 004: return "Afghanistan";
                case 248: return "Åland Islands";
                case 008: return "Albania";
                case 012: return "Algeria";
                case 016: return "American Samoa";
                case 020: return "Andorra";
                case 024: return "Angola";
                case 660: return "Anguilla";
                case 010: return "Antarctica";
                case 028: return "Antigua and Barbuda";
                case 032: return "Argentina";
                case 051: return "Armenia";
                case 533: return "Aruba";
                case 036: return "Australia";
                case 040: return "Austria";
                case 031: return "Azerbaijan";
                case 044: return "Bahamas";
                case 048: return "Bahrain";
                case 050: return "Bangladesh";
                case 052: return "Barbados";
                case 112: return "Belarus";
                case 056: return "Belgium";
                case 084: return "Belize";
                case 204: return "Benin";
                case 060: return "Bermuda";
                case 064: return "Bhutan";
                case 068: return "Bolivia";
                case 535: return "Bonaire, Sint Eustatius and Saba";
                case 070: return "Bosnia and Herzegovina";
                case 072: return "Botswana";
                case 074: return "Bouvet Island";
                case 076: return "Brazil";
                case 086: return "British Indian Ocean Territory";
                case 096: return "Brunei Darussalam";
                case 100: return "Bulgaria";
                case 854: return "Burkina Faso";
                case 108: return "Burundi";
                case 132: return "Cabo Verde";
                case 116: return "Cambodia";
                case 120: return "Cameroon";
                case 124: return "Canada";
                case 136: return "Cayman Islands";
                case 140: return "Central African Republic";
                case 148: return "Chad";
                case 152: return "Chile";
                case 156: return "China";
                case 162: return "Christmas Island";
                case 166: return "Cocos (Keeling) Islands";
                case 170: return "Colombia";
                case 174: return "Comoros";
                case 178: return "Congo (Congo-Brazzaville)";
                case 180: return "Congo (Congo-Kinshasa)";
                case 184: return "Cook Islands";
                case 188: return "Costa Rica";
                case 384: return "Côte d'Ivoire";
                case 191: return "Croatia";
                case 192: return "Cuba";
                case 531: return "Curaçao";
                case 196: return "Cyprus";
                case 203: return "Czechia";
                case 208: return "Denmark";
                case 262: return "Djibouti";
                case 212: return "Dominica";
                case 214: return "Dominican Republic";
                case 218: return "Ecuador";
                case 818: return "Egypt";
                case 222: return "El Salvador";
                case 226: return "Equatorial Guinea";
                case 232: return "Eritrea";
                case 233: return "Estonia";
                case 748: return "Eswatini";
                case 231: return "Ethiopia";
                case 238: return "Falkland Islands (Malvinas)";
                case 234: return "Faroe Islands";
                case 242: return "Fiji";
                case 246: return "Finland";
                case 250: return "France";
                case 254: return "French Guiana";
                case 258: return "French Polynesia";
                case 260: return "French Southern Territories";
                case 266: return "Gabon";
                case 270: return "Gambia";
                case 268: return "Georgia";
                case 276: return "Germany";
                case 288: return "Ghana";
                case 292: return "Gibraltar";
                case 300: return "Greece";
                case 304: return "Greenland";
                case 308: return "Grenada";
                case 312: return "Guadeloupe";
                case 316: return "Guam";
                case 320: return "Guatemala";
                case 831: return "Guernsey";
                case 324: return "Guinea";
                case 624: return "Guinea-Bissau";
                case 328: return "Guyana";
                case 332: return "Haiti";
                case 334: return "Heard Island and McDonald Islands";
                case 336: return "Holy See";
                case 340: return "Honduras";
                case 344: return "Hong Kong";
                case 348: return "Hungary";
                case 352: return "Iceland";
                case 356: return "India";
                case 360: return "Indonesia";
                case 364: return "Iran (Islamic Republic of Iran)";
                case 368: return "Iraq";
                case 372: return "Ireland";
                case 833: return "Isle of Man";
                case 376: return "Israel";
                case 380: return "Italy";
                case 388: return "Jamaica";
                case 392: return "Japan";
                case 832: return "Jersey";
                case 400: return "Jordan";
                case 398: return "Kazakhstan";
                case 404: return "Kenya";
                case 296: return "Kiribati";
                case 408: return "Korea (North Korea)";
                case 410: return "Korea (South Korea)";
                case 414: return "Kuwait";
                case 417: return "Kyrgyzstan";
                case 418: return "Lao People's Democratic Republic";
                case 428: return "Latvia";
                case 422: return "Lebanon";
                case 426: return "Lesotho";
                case 430: return "Liberia";
                case 434: return "Libya";
                case 438: return "Liechtenstein";
                case 440: return "Lithuania";
                case 442: return "Luxembourg";
                case 446: return "Macao";
                case 450: return "Madagascar";
                case 454: return "Malawi";
                case 458: return "Malaysia";
                case 462: return "Maldives";
                case 466: return "Mali";
                case 470: return "Malta";
                case 584: return "Marshall Islands";
                case 474: return "Martinique";
                case 478: return "Mauritania";
                case 480: return "Mauritius";
                case 175: return "Mayotte";
                case 484: return "Mexico";
                case 583: return "Micronesia";
                case 498: return "Moldova";
                case 492: return "Monaco";
                case 496: return "Mongolia";
                case 499: return "Montenegro";
                case 500: return "Montserrat";
                case 504: return "Morocco";
                case 508: return "Mozambique";
                case 104: return "Myanmar";
                case 516: return "Namibia";
                case 520: return "Nauru";
                case 524: return "Nepal";
                case 528: return "Netherlands";
                case 540: return "New Caledonia";
                case 554: return "New Zealand";
                case 558: return "Nicaragua";
                case 562: return "Niger";
                case 566: return "Nigeria";
                case 570: return "Niue";
                case 574: return "Norfolk Island";
                case 807: return "North Macedonia";
                case 580: return "Northern Mariana Islands";
                case 578: return "Norway";
                case 512: return "Oman";
                case 586: return "Pakistan";
                case 585: return "Palau";
                case 275: return "Palestine";
                case 591: return "Panama";
                case 598: return "Papua New Guinea";
                case 600: return "Paraguay";
                case 604: return "Peru";
                case 608: return "Philippines";
                case 612: return "Pitcairn";
                case 616: return "Poland";
                case 620: return "Portugal";
                case 630: return "Puerto Rico";
                case 634: return "Qatar";
                case 638: return "Réunion";
                case 642: return "Romania";
                case 643: return "Russian Federation";
                case 646: return "Rwanda";
                case 652: return "Saint Barthélemy";
                case 654: return "Saint Helena, Ascension and Tristan da Cunha";
                case 659: return "Saint Kitts and Nevis";
                case 662: return "Saint Lucia";
                case 663: return "Saint Martin (French part)";
                case 666: return "Saint Pierre and Miquelon";
                case 670: return "Saint Vincent and the Grenadines";
                case 882: return "Samoa";
                case 674: return "San Marino";
                case 678: return "Sao Tome and Principe";
                case 682: return "Saudi Arabia";
                case 686: return "Senegal";
                case 688: return "Serbia";
                case 690: return "Seychelles";
                case 694: return "Sierra Leone";
                case 702: return "Singapore";
                case 534: return "Sint Maarten (Dutch part)";
                case 703: return "Slovakia";
                case 705: return "Slovenia";
                case 090: return "Solomon Islands";
                case 706: return "Somalia";
                case 710: return "South Africa";
                case 239: return "South Georgia and the South Sandwich Islands";
                case 728: return "South Sudan";
                case 724: return "Spain";
                case 144: return "Sri Lanka";
                case 729: return "Sudan";
                case 740: return "Suriname";
                case 744: return "Svalbard and Jan Mayen";
                case 752: return "Sweden";
                case 756: return "Switzerland";
                case 760: return "Syrian Arab Republic";
                case 158: return "Taiwan, Province of China[a]";
                case 762: return "Tajikistan";
                case 834: return "Tanzania";
                case 764: return "Thailand";
                case 626: return "Timor-Leste";
                case 768: return "Togo";
                case 772: return "Tokelau";
                case 776: return "Tonga";
                case 780: return "Trinidad and Tobago";
                case 788: return "Tunisia";
                case 792: return "Turkey";
                case 795: return "Turkmenistan";
                case 796: return "Turks and Caicos Islands";
                case 798: return "Tuvalu";
                case 800: return "Uganda";
                case 804: return "Ukraine";
                case 784: return "United Arab Emirates";
                case 826: return "United Kingdom of Great Britain and Northern Ireland";
                case 840: return "United States of America";
                case 581: return "United States Minor Outlying Islands";
                case 858: return "Uruguay";
                case 860: return "Uzbekistan";
                case 548: return "Vanuatu";
                case 862: return "Venezuela";
                case 704: return "Viet Nam";
                case 092: return "Virgin Islands (British)";
                case 850: return "Virgin Islands (U.S.)";
                case 876: return "Wallis and Futuna";
                case 732: return "Western Sahara";
                case 887: return "Yemen";
                case 894: return "Zambia";
                case 716: return "Zimbabwe";
                default: return "Unknown";
            }
        }
    }
}
