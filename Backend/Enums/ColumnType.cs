﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Enums
{
    // https://www.devart.com/dotconnect/mysql/docs/DataTypeMapping.html
    // https://tableplus.com/blog/2018/08/mysql-the-difference-between-int-bigint-mediumint-smallint-tinyint.html
    public static class ColumnType
    {
        /// <summary>
        /// From 0 to 255 (1 byte)
        /// </summary>
        public const string Byte = "tinyint unsigned";
        /// <summary>
        /// From -128 to 127 (1 byte)
        /// </summary>
        public const string SByte = "tinyint";
        /// <summary>
        /// From -32,768 to 32,767 (2 bytes)
        /// </summary>
        public const string Int16 = "smallint";
        /// <summary>
        /// From 0 to 65,535 (2 bytes)
        /// </summary>
        public const string UInt16 = "smallint unsigned";
        /// <summary>
        /// From -8,388,608 to 8,388,607 (3 bytes)
        /// </summary>
        public const string Int24 = "mediumint";
        /// <summary>
        /// From 0 to 16,777,215 (3 bytes)
        /// </summary>
        public const string UInt24 = "mediumint unsigned";
        /// <summary>
        /// From -2,147,483,648 to 2,147,483,647 (4 bytes)
        /// </summary>
        public const string Int32 = "int";
        /// <summary>
        /// From 0 to 4,294,967,295 (4 bytes)
        /// </summary>
        public const string UInt32 = "int unsigned";

        // https://dev.mysql.com/doc/refman/5.7/en/precision-math-decimal-characteristics.html
        /// <summary>
        /// 9 integer and 9 fractional digits (8 bytes)
        /// </summary>
        public const string Money = "decimal(18,9)";
        /// <summary>
        /// 18 integer and 9 fractional digits (12 bytes)
        /// </summary>
        public const string MoneyBig = "decimal(27,9)";
        /// <summary>
        /// 2 integer and 2 fractional digits (2 bytes)
        /// </summary>
        public const string Percent = "decimal(4,2)";

        /// <summary>
        /// YYYY-MM-DD
        /// </summary>
        public const string Date = "date";

        // http://www.softtime.ru/forum/read.php?id_forum=3&id_theme=17431
        /// <summary>
        /// 64 kilobytes (65,535 bytes)
        /// </summary>
        public const string Text = "text";
        /// <summary>
        /// 16 megabytes (16,777,215 bytes)
        /// </summary>
        public const string TextMedium = "mediumtext";
        /// <summary>
        /// 4 gigabytes (4,294,967,295 bytes)
        /// </summary>
        public const string TextLong = "longtext";
    }
}
