﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.DTO.Product
{
    public class ProductDigiDTO
    {
        public bool? Allowed { get; set; }
        /// <summary>
        /// Имя без специальных html символов типа &nbsp;
        /// </summary>
        [Required]
        [StringLength(55, MinimumLength = 1)]
        public string Name { get; set; }
        public bool? NameChanged { get; set; }
        [Required]
        [Range(0, 100)]
        public byte AgencyFee { get; set; }
        public bool? AgencyFeeChanged { get; set; }
    }
}
