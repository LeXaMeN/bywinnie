﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace Backend.DTO.Product
{
    public class ProductDTO
    {
        [Required]
        [Range(1, int.MaxValue)]
        public int CategoryId { get; set; }
        [Required]
        [StringLength(55, MinimumLength = 1)]
        [RegularExpression(@"^[a-z0-9-]+$")]
        public string Index { get; set; }
        [Required]
        [StringLength(55, MinimumLength = 1)]
        public string Name { get; set; }
        [Required]
        [StringLength(55, MinimumLength = 1)]
        public string NameRus { get; set; }
        [Required]
        [StringLength(255, MinimumLength = 1)]
        public string Description { get; set; }
        [Required]
        [StringLength(255, MinimumLength = 1)]
        public string DescriptionRus { get; set; }

        [Range(-999999.99, 999999.99)]
        public decimal? Price { get; set; } // in rubles
    }
}
