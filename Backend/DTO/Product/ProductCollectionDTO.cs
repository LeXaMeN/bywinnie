﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.DTO.Product
{
    public class ProductCollectionDTO
    {
        [Range(1, int.MaxValue)]
        public int? Id { get; set; }
        [Range(1, int.MaxValue)]
        public int? CategoryId { get; set; }
        [StringLength(55)]
        public string Name { get; set; }

        [Range(0, 999999.99)]
        public decimal? PriceMin { get; set; }
        [Range(0, 999999.99)]
        public decimal? PriceMax { get; set; }

        /// <summary>
        /// List(int[ItemType, ...ItemValueId])
        /// </summary>
        public List<int[]> Filters { get; set; }

        public enum OrderEnum
        {
            Id = 0,
            Category = 1,
            Name = 2,
            NameRus = 3,
            Price = 4,
        }
        [EnumDataType(typeof(OrderEnum))]
        public OrderEnum? OrderBy { get; set; }
        public bool OrderByDesc { get; set; }
    }
}
