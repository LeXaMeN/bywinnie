﻿using Backend.Data.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.DTO.Product
{
    public class ProductFileDTO
    {
        [Required]
        [EnumDataType(typeof(ProductFileType))]
        public ProductFileType Type { get; set; }

        [Required]
        [StringLength(255, MinimumLength = 1)]
        public string Url { get; set; }

        [Required]
        [Range(short.MinValue, short.MaxValue)]
        public short Order { get; set; }
    }
}
