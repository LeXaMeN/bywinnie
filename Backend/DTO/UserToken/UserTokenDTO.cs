﻿using Destructurama.Attributed;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.DTO.UserToken
{
    public class UserTokenDTO
    {
        [NotLogged]
        [Required]
        [StringLength(64, MinimumLength = 8)]
        public string Password { get; set; }

        [Required]
        [StringLength(1024)]
        public string Captcha { get; set; }

        [Required]
        [StringLength(20, MinimumLength = 1)]
        [RegularExpression(@"^[a-zA-Z_]+$")]
        public string Login { get; set; }
    }
}
