﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.DTO.Item
{
    public class ItemDecimalDTO
    {
        [Required]
        [Range(-999999.99, 999999.99)]
        public decimal Value { get; set; }
    }
}