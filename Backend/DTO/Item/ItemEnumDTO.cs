﻿using Backend.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.DTO.Item
{
    public class ItemEnumDTO
    {
        [Required]
        [StringLength(55, MinimumLength = 1)]
        public string Value { get; set; }
        [Required]
        [StringLength(55, MinimumLength = 1)]
        public string ValueRus { get; set; }
        [Required]
        [Range(short.MinValue, short.MaxValue)]
        public short Order { get; set; }
    }
}