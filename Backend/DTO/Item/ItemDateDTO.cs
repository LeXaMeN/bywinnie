﻿using Backend.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.DTO.Item
{
    public class ItemDateDTO
    {
        [Required]
        [DataType(DataType.Date)]
        public DateTime? Value { get; set; }
    }
}
