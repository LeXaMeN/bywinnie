﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.DTO.Item
{
    public class ItemBooleanDTO
    {
        [Required]
        public bool Value { get; set; }
    }
}
