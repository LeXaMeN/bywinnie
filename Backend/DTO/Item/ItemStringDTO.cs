﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.DTO.Item
{
    public class ItemStringDTO
    {
        [Required]
        [StringLength(1000, MinimumLength = 1)]
        public string Value { get; set; }
        [Required]
        [StringLength(1000, MinimumLength = 1)]
        public string ValueRus { get; set; }
    }
}