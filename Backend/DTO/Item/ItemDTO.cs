﻿using Backend.Data.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.DTO.Item
{
    public class ItemDTO
    {
        [Required]
        [EnumDataType(typeof(ItemType))]
        public ItemType Type { get; set; }
        [Required]
        [StringLength(55, MinimumLength = 1)]
        public string Name { get; set; }
        [Required]
        [StringLength(55, MinimumLength = 1)]
        public string NameRus { get; set; }
    }
}
