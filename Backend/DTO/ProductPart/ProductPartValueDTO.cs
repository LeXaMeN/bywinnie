﻿using Backend.Data.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.DTO.ProductPart
{
    public class ProductPartValueDTO
    {
        [Required]
        [Range(1, int.MaxValue)]
        public int ProductId { get; set; }
        [Required]
        [Range(1, int.MaxValue)]
        public int ProductPartCategoryId { get; set; }
        [Required]
        [Range(1, int.MaxValue)]
        public int ItemValueId { get; set; }
        [Required]
        [EnumDataType(typeof(ItemType))]
        public ItemType ItemType { get; set; }
    }
}
