﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.DTO.ProductPart
{
    public class ProductPartSequenceDTO
    {
        [Required]
        [Range(1, int.MaxValue)]
        public int CategoryId { get; set; }

        /// <summary>
        /// List[<see cref="Data.Entities.ProductPartCategory.Id"/>]
        /// </summary>
        [Required]
        public List<int> Categories { get; set; }
        /// <summary>
        /// Dictionary(<see cref="Data.Entities.ProductPartCategory.Id"/>, List[<see cref="Data.Entities.Item.Id"/>])
        /// </summary>
        [Required]
        public Dictionary<int, List<int>> Dictionary { get; set; }
    }
}
