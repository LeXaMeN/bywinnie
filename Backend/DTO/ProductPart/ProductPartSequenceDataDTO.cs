﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.DTO.ProductPart
{
    /// <summary>
    /// Инфа чтобы создать ProductPartSequence.
    /// Нагрузка на бд, юзать только в админке.
    /// </summary>
    public class ProductPartSequenceDataDTO
    {
        public Data.Entities.Category Category { get; set; }
        public List<Data.Entities.ProductPartCategory> Categories { get; set; }
        public Dictionary<int, List<Data.Entities.Item>> Dictionary { get; set; }
    }
}
