﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.DTO.Category
{
    public class CategoryDTO
    {
        [Range(1, int.MaxValue)]
        public int? ParentId { get; set; }
        [Required]
        [StringLength(55, MinimumLength = 1)]
        [RegularExpression(@"^[a-z0-9-]+$")]
        public string Index { get; set; }
        [Required]
        [StringLength(55, MinimumLength = 1)]
        public string Name { get; set; }
        [Required]
        [StringLength(55, MinimumLength = 1)]
        public string NameRus { get; set; }
    }
}
