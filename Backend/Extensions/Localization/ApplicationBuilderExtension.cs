﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Localization;
using System.Globalization;

namespace Backend.Extensions.Localization
{
    public static class ApplicationBuilderExtension
    {
        public static IApplicationBuilder UseRequestLocalizationExtension(this IApplicationBuilder app)
        {
            // https://www.codeproject.com/Questions/790609/How-do-I-resolve-error-message-The-value-is-not-va
            var supportedCultures = new[] {
                new CultureInfo("en"),
                new CultureInfo("en-US"),
                new CultureInfo("en-GB"),
                new CultureInfo("ru"),
                new CultureInfo("ru-RU")
            };
            // https://www.basicdatepicker.com/samples/cultureinfo.aspx
            for (int i = 0; i < supportedCultures.Length; i++)
            {
                // for DTO Date validation
                //supportedCultures[i].DateTimeFormat.ShortDatePattern = "dd.MM.yyyy";
            }
            // https://stackoverflow.com/a/47341787
            return app.UseRequestLocalization(new RequestLocalizationOptions {
                DefaultRequestCulture = new RequestCulture(supportedCultures[0]),
                SupportedCultures = supportedCultures,
                SupportedUICultures = supportedCultures
            });
        }
    }
}
