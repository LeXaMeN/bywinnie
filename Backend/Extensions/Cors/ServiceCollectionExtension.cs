﻿using Backend.Enums;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Cors.Internal;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;

namespace Backend.Extensions.Cors
{
    public static class ServiceCollectionExtension
    {
        public static IServiceCollection AddCorsExtension(this IServiceCollection services, IConfiguration configuration)
        {
            return services
                .AddCors(options =>
                {
                    var origins = new List<string> { configuration["Api:Cors:Origins:Main"], configuration["Api:Cors:Origins:Admin"] };
                    options.AddPolicy(PolicyName.Default, builder => builder.WithOrigins(origins.ToArray()).AllowAnyHeader().AllowAnyMethod());

                    //origins.Add(Configuration["Api:Cors:Origins:Admin"]);
                    options.AddPolicy(PolicyName.Admin, builder => builder.WithOrigins(configuration["Api:Cors:Origins:Admin"]).AllowAnyHeader().AllowAnyMethod());

                    //origins.AddRange(configuration.GetSection("Api:Cors:Origins:Public").Get<string[]>());
                    //options.AddPolicy(PolicyName.Public, builder => builder.WithOrigins(origins.ToArray()).AllowAnyHeader().AllowAnyMethod());
                })
                // http://azuretrick.com/aspnet/core/security/cors bug fix ignore EnableCorsAttribute if use app.UseCors(PolicyName.Default)
                .Configure<MvcOptions>(options => options.Filters.Add(new CorsAuthorizationFilterFactory(PolicyName.Default)));
        }
    }
}
