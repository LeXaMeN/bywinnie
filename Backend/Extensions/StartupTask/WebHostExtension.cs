﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using System.Threading;
using System.Threading.Tasks;

namespace Backend.Extensions.StartupTask
{
    public static class WebHostExtension
    {
        //public static async Task RunWithTasksAsync(this IWebHost webHost, CancellationToken cancellationToken = default)
        //{
        //    // Load all tasks from DI
        //    var startupTasks = webHost.Services.GetServices<IStartupTask>();

        //    // Execute all the tasks
        //    foreach (var startupTask in startupTasks)
        //    {
        //        await startupTask.ExecuteAsync(cancellationToken);
        //    }

        //    // Start the tasks as normal
        //    await webHost.RunAsync(cancellationToken);
        //}

        public static IWebHost RunStartupTasks(this IWebHost webHost)
        {
            var startupTasks = webHost.Services.GetServices<IStartupTask>();

            foreach (var startupTask in startupTasks)
            {
                startupTask.Execute();
            }

            return webHost;
        }
    }
}
