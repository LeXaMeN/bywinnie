﻿using System.Threading;
using System.Threading.Tasks;

namespace Backend.Extensions.StartupTask
{
    public interface IStartupTask
    {
        //Task ExecuteAsync(CancellationToken cancellationToken);
        void Execute();
    }
}
