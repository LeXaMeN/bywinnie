﻿using Backend.Data.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Backend.Extensions.StartupTask.Tasks
{
    public class GeoIpsStartupTask : IStartupTask
    {
        private IServiceProvider ServiceProvider;

        public GeoIpsStartupTask(IServiceProvider serviceProvider)
        {
            ServiceProvider = serviceProvider;
        }

        private void ExecuteSqlCommand(DatabaseContext db, string table, string columns, List<string> parts, int limit = 0)
        {
            if (parts.Count >= limit && parts.Count > 0)
            {
#pragma warning disable EF1000 // Possible SQL injection vulnerability.
                db.Database.ExecuteSqlCommand("INSERT INTO " + table + " " + columns + " VALUES " + string.Join(',', parts) + ";");
#pragma warning restore EF1000 // Possible SQL injection vulnerability.
                parts.Clear();
            }
        }

        public void Execute()
        {
            using (var scope = ServiceProvider.CreateScope())
            {
                var db = scope.ServiceProvider.GetRequiredService<DatabaseContext>();
                var parts = new List<string>(capacity: 100);

                if (db.GeoCountries.Any() == false)
                {
                    using (var cities = new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Resources", "cities.txt"), Encoding.UTF8))
                    {
                        while (!cities.EndOfStream)
                        {
                            var w = cities.ReadLine().Split('\t', StringSplitOptions.RemoveEmptyEntries);
                            if (w.Length != 6) continue;

                            parts.Add($"({w[0]}, '{w[1]}', '{w[2]}', '{w[3]}', {w[4]}, {w[5]})");
                            ExecuteSqlCommand(db, "GeoCountry", "(Id, Name, Region, District, Latitude, Longitude)", parts, limit: 100);
                        }
                    }
                    ExecuteSqlCommand(db, "GeoCountry", "(Id, Name, Region, District, Latitude, Longitude)", parts);
                }

                if (db.GeoIps.Any() == false)
                {
                    using (var cidr_optim = new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Resources", "cidr_optim.txt"), Encoding.UTF8))
                    {
                        while (!cidr_optim.EndOfStream)
                        {
                            var w = cidr_optim.ReadLine().Split('\t', StringSplitOptions.RemoveEmptyEntries);
                            if (w.Length != 5) continue;

                            var ip1 = w[2].Substring(0, w[2].IndexOf(' '));
                            var ip2 = w[2].Substring(w[2].LastIndexOf(' ') + 1);
                            int? cid = null;
                            if (int.TryParse(w[4], out var geoCountryId))
                            {
                                cid = geoCountryId;
                            }

                            parts.Add($"({w[0]}, {w[1]}, '{ip1}', '{ip2}', '{w[3]}', {cid?.ToString() ?? "NULL"})");
                            ExecuteSqlCommand(db, "GeoIp", "(StartBlock, EndBlock, StartIp, EndIp, Country, GeoCountryId)", parts, limit: 100);
                        }
                    }
                    ExecuteSqlCommand(db, "GeoIp", "(StartBlock, EndBlock, StartIp, EndIp, Country, GeoCountryId)", parts);
                }
            }
        }
    }
}
