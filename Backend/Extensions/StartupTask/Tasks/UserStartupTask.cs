﻿using Backend.Data.Context;
using Backend.Data.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Extensions.StartupTask.Tasks
{
    public class UserStartupTask : IStartupTask
    {
        private IConfiguration Configuration;
        private IServiceProvider ServiceProvider;

        public UserStartupTask(IConfiguration configuration, IServiceProvider serviceProvider)
        {
            Configuration = configuration;
            ServiceProvider = serviceProvider;
        }

        public void Execute()
        {
            using (var scope = ServiceProvider.CreateScope())
            {
                var db = scope.ServiceProvider.GetRequiredService<DatabaseContext>();
                var login = Configuration["Api:Admin:Login"];

                if (db.Users.Any(u => u.Login == login))
                    return;
                if (db.Users.Any(u => EF.Functions.Like(u.RolesJson, $"%\"{UserRole.Admin}\"%")))
                    return;

                var user = new User() {
                    Login = login,
                    Password = new PasswordHasher<User>().HashPassword(null, Encoding.UTF8.GetString(Convert.FromBase64String(Configuration["Api:Admin:Secret"]))),
                    RolesJson = $"[\"{UserRole.User}\",\"{UserRole.Admin}\"]"
                };

                db.Users.Add(user);
                db.SaveChanges();
            }
        }
    }
}
