﻿using Backend.Data.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Backend.Extensions.StartupTask.Tasks
{
    public class MigrationStartupTask : IStartupTask
    {
        private IServiceProvider ServiceProvider;

        public MigrationStartupTask(IServiceProvider serviceProvider)
        {
            ServiceProvider = serviceProvider;
        }

        public void Execute()
        {
            using (var scope = ServiceProvider.CreateScope())
            {
                var db = scope.ServiceProvider.GetRequiredService<DatabaseContext>();

                //db.Database.EnsureDeleted();
                //db.Database.EnsureCreated();
                db.Database.Migrate();
            }
        }
    }
}
