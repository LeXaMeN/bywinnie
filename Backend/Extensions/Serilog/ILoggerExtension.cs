﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Backend.Extensions.Serilog
{
    public static class ILoggerExtension
    {
        //public static void LogRequest(this ILogger logger, LogLevel level, Exception exception, string method, string argNames, params object[] args)
        //{
        //    logger.Log(level, exception, method + ": " + argNames, args);
        //}

        private static string Message(Exception exception, string method, int argsCount)
        {
            var stackTrace = string.IsNullOrEmpty(exception.StackTrace) ? /*Environment.StackTrace*/null : exception.StackTrace.Split(Environment.NewLine);
            var match = stackTrace?.Length > 0 ? Regex.Match(stackTrace[stackTrace.Length - 1], @"[^\s.]+\.?[^\s.]+\(") : null;
            method = match?.Success == true ? match.Value : method + "(";

            switch (argsCount)
            {
                case 0:
                    return method + ")";
                case 1:
                    return method + "{@Request})";
                case 2:
                    return method + "{@Request}, {@arg1})";
                case 3:
                    return method + "{@Request}, {@arg1}, {@arg2})";
                default:
                {
                    var names = new string[argsCount - 1];
                    for (int i = 1; i < names.Length; i++)
                    {
                        names[i] = "{@arg" + i + "}";
                    }
                    return method + "{@Request}, " + string.Join(", ", names) + ")";
                }
            }
        }


        public static void LogRequestWarning(this ILogger logger, Exception exception, string method)
        {
            logger.Log(LogLevel.Warning, exception, Message(exception, method, 0));
        }

        public static void LogRequestWarning(this ILogger logger, Exception exception, string method, object request)
        {
            logger.Log(LogLevel.Warning, exception, Message(exception, method, 1), request);
        }

        public static void LogRequestWarning(this ILogger logger, Exception exception, string method, params object[] args)
        {
            logger.Log(LogLevel.Warning, exception, Message(exception, method, args.Length), args);
        }


        public static void LogRequestError(this ILogger logger, Exception exception, string method)
        {
            logger.Log(LogLevel.Error, exception, Message(exception, method, 0));
        }

        public static void LogRequestError(this ILogger logger, Exception exception, string method, object request)
        {
            logger.Log(LogLevel.Error, exception, Message(exception, method, 1), request);
        }

        public static void LogRequestError(this ILogger logger, Exception exception, string method, params object[] args)
        {
            logger.Log(LogLevel.Error, exception, Message(exception, method, args.Length), args);
        }


        public static void LogRequestCritical(this ILogger logger, Exception exception, string method)
        {
            logger.Log(LogLevel.Critical, exception, Message(exception, method, 0));
        }

        public static void LogRequestCritical(this ILogger logger, Exception exception, string method, object request)
        {
            logger.Log(LogLevel.Critical, exception, Message(exception, method, 1), request);
        }

        public static void LogRequestCritical(this ILogger logger, Exception exception, string method, params object[] args)
        {
            logger.Log(LogLevel.Critical, exception, Message(exception, method, args.Length), args);
        }
    }
}
