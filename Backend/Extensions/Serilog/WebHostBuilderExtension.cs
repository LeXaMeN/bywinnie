﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Destructurama;
using Serilog.Core;
using Serilog.Events;
using System.Reflection;
using System.Linq;

namespace Backend.Extensions.Serilog
{
    public static class WebHostBuilderExtension
    {
        public static IWebHostBuilder UseSerilogFromAppSettings(this IWebHostBuilder webHostBuilder)
        {
            // https://github.com/serilog/serilog-settings-configuration
            return webHostBuilder.UseSerilog((hostingContext, loggerConfiguration) => loggerConfiguration
                .ReadFrom.Configuration(hostingContext.Configuration)
                .Destructure.UsingAttributes() // https://github.com/destructurama/attributed
                //.Destructure.With<IncludePublicFieldsPolicy>() // https://stackoverflow.com/a/46165298
            );
        }
    }

    //class IncludePublicFieldsPolicy : IDestructuringPolicy
    //{
    //    public bool TryDestructure(object value, ILogEventPropertyValueFactory propertyValueFactory, out LogEventPropertyValue result)
    //    {
    //        //if (!(value is SomeBaseType))
    //        //{
    //        //    result = null;
    //        //    return false;
    //        //}

    //        var fields = value.GetType().GetFields();
    //        var logEventProperties = fields.Select(field => new LogEventProperty(
    //            name: field.Name,
    //            value: propertyValueFactory.CreatePropertyValue(field.GetValue(value)))
    //        );

    //        result = new StructureValue(logEventProperties);
    //        return true;
    //    }
    //}
}
