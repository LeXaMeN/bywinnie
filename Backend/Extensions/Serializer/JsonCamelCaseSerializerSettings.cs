﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Backend.Extensions.Serializer
{
    public class JsonCamelCaseSerializerSettings : JsonSerializerSettings
    {
        public JsonCamelCaseSerializerSettings(bool ignoreSerializableInterface = true, ReferenceLoopHandling referenceLoopHandling = ReferenceLoopHandling.Ignore)
        {
            // exception serialize bug fix
            ContractResolver = new CamelCasePropertyNamesContractResolver() { IgnoreSerializableInterface = ignoreSerializableInterface };
            // https://stackoverflow.com/a/18223985
            ReferenceLoopHandling = referenceLoopHandling;
        }
    }
}
