﻿using Backend.Data.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace Backend.Extensions.JwtBearer
{
    public static class ClaimsPrincipalExtension
    {
        public static int GetId(this ClaimsPrincipal principal)
        {
            return int.Parse(principal.FindFirstValue(ClaimTypes.NameIdentifier));
        }

        public static string GetLogin(this ClaimsPrincipal principal)
        {
            return principal.FindFirstValue(ClaimsIdentity.DefaultNameClaimType);
        }

        public static IEnumerable<string> GetRoles(this ClaimsPrincipal principal)
        {
            return principal.FindAll(ClaimsIdentity.DefaultRoleClaimType).Select(r => r.Value);
        }

        //public static User GetUser(this ClaimsPrincipal principal) => new User() {
        //    Id = principal.GetId(),
        //    Email = principal.GetLogin(),
        //    Roles = principal.GetRoles().ToList()
        //};
    }
}
