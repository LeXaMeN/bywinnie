﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query.Expressions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Backend.Extensions.Database
{
    /// <summary>
    /// https://dev.mysql.com/doc/refman/8.0/en/fulltext-search.html
    /// </summary>
    public static class MySqlTextFunctions
    {
        private static readonly InvalidOperationException _exception = new InvalidOperationException("This method is for use with Entity Framework Core only and has no in-memory implementation.");
        
        public static double MatchAgainst(object cols, string expr) => throw _exception;
        public static double MatchAgainstWithQueryExpansion(object cols, string expr) => throw _exception;
        public static double MatchAgainstInBooleanMode(object cols, string expr) => throw _exception;
    }

    /// <summary>
    /// https://dev.mysql.com/doc/refman/8.0/en/fulltext-search.html
    /// </summary>
    public enum MySqlMatchAgainstSearchModifier
    {
        Empty,
        InNaturalLanguageMode,
        InNaturalLanguageModeWithQueryExpansion,
        InBooleanMode,
        WithQueryExpansion
    }

    /// <summary>
    /// https://www.thinktecture.com/en/entity-framework-core/custom-functions-using-imethodcalltranslator-in-2-1/
    /// https://www.thinktecture.com/en/entity-framework-core/custom-functions-using-hasdbfunction-in-2-1/
    /// https://habr.com/ru/post/351556/
    /// </summary>
    public class MySqlMatchAgainstExpression : Expression
    {
        private readonly IEnumerable<Expression> _cols;
        private readonly Expression _expr;
        private readonly MySqlMatchAgainstSearchModifier _searchModifier;

        public override ExpressionType NodeType => ExpressionType.Extension;
        public override Type Type => typeof(double);
        public override bool CanReduce => false;

        public MySqlMatchAgainstExpression(IEnumerable<Expression> cols, Expression expr, MySqlMatchAgainstSearchModifier searchModifier)
        {
            _cols = cols?.Count() > 0 ? cols : throw new ArgumentNullException(nameof(cols));
            _expr = expr ?? throw new ArgumentNullException(nameof(expr));
            _searchModifier = searchModifier;
        }

        // https://dev.azure.com/pawelgerr/_git/Thinktecture.EntityFrameworkCore?path=%2Fsrc%2FThinktecture.EntityFrameworkCore.Relational%2FExtensions%2FRelationalExpressionVisitorExtensions.cs&version=GBmaster
        /*protected override Expression VisitChildren(ExpressionVisitor visitor)
        {
            var visitedOrderBy = visitor.VisitExpressions(_expressions);

            if (ReferenceEquals(_expressions, visitedOrderBy))
                return this;

            return new MatchAgainstExpression(visitedOrderBy);
        }*/

        protected override Expression Accept(ExpressionVisitor visitor)
        {
            //if (!(visitor is IQuerySqlGenerator))
            //    return base.Accept(visitor);

            visitor.Visit(new SqlFragmentExpression("MATCH ("));
            // массив cols: col1, col2...
            var insertComma = false;
            foreach (var col in _cols)
            {
                if (insertComma)
                    visitor.Visit(new SqlFragmentExpression(","));
                else
                    insertComma = true;

                visitor.Visit(col);
            }
            visitor.Visit(new SqlFragmentExpression(") AGAINST ("));
            visitor.Visit(_expr); // что мы ищем
            switch (_searchModifier)
            {
                case MySqlMatchAgainstSearchModifier.InNaturalLanguageMode:
                    visitor.Visit(new SqlFragmentExpression(" IN NATURAL LANGUAGE MODE"));
                    break;
                case MySqlMatchAgainstSearchModifier.InNaturalLanguageModeWithQueryExpansion:
                    visitor.Visit(new SqlFragmentExpression(" IN NATURAL LANGUAGE MODE WITH QUERY EXPANSION"));
                    break;
                case MySqlMatchAgainstSearchModifier.InBooleanMode:
                    visitor.Visit(new SqlFragmentExpression(" IN BOOLEAN MODE"));
                    break;
                case MySqlMatchAgainstSearchModifier.WithQueryExpansion:
                    visitor.Visit(new SqlFragmentExpression(" WITH QUERY EXPANSION"));
                    break;
            }
            visitor.Visit(new SqlFragmentExpression(")"));

            return this;
        }
    }
}
