﻿using Backend.Data.Context;
using Backend.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Z.EntityFramework.Plus;

namespace Backend.Providers
{
    public class UserRoleProvider
    {
        public static bool IsValid(string role)
        {
            switch (role)
            {
                case UserRole.User:
                //case UserRole.Partner:
                case UserRole.Admin:
                    return true;
                default:
                    return false;
            }
        }

        public bool Update(DatabaseContext db, int userId, List<string> roles)
        {
            var rolesJson = JsonConvert.SerializeObject(roles.Distinct(), Formatting.None);
            return db.Database.ExecuteSqlCommand($"UPDATE `User` SET RolesJson = {rolesJson} WHERE Id = {userId}") > 0;
        }

        public bool ExistAny(DatabaseContext db, int userId, string role)
        {
            return IsValid(role) && db.Users.FromSql("SELECT * FROM `User` WHERE Id = {0} AND RolesJson LIKE {1}", userId, $"%\"{role}\"%").Any();
        }

        public bool ExistAny(DatabaseContext db, string email, string role)
        {
            return IsValid(role) && db.Users.FromSql("SELECT * FROM `User` WHERE Email = {0} AND RolesJson LIKE {1}", email, $"%\"{role}\"%").Any();
        }

        public List<string> ReadonlyList(DatabaseContext db, int userId)
        {
            var rolesJson = db.Users.Where(u => u.Id == userId)/*.OrderBy(u => u.Id)*/.Select(u => u.RolesJson).FirstOrDefault();
            return JsonConvert.DeserializeObject<List<string>>(rolesJson ?? "[]");
        }

        private ReadOnlyCollection<int> _adminIds { get; set; }
        public ReadOnlyCollection<int> ReadAdminIds(DatabaseContext db)
        {
            return _adminIds ?? (_adminIds = db.Users.Where(u => EF.Functions.Like(u.RolesJson, $"%\"{UserRole.Admin}\"%")).Select(u => u.Id).ToList().AsReadOnly());
        }
    }
}
