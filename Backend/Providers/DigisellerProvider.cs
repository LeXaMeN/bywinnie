﻿using Backend.Utils.Security.Cryptography;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Providers
{
    public class DigisellerProvider
    {
        private IConfiguration Configuration { get; }

        public DigisellerProvider(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public class DigiProductShortInfo
        {
            public int id;
            public int id_seller;
            public string name;
            public string info;
            public string add_info;
            public string collection;
            public string base_currency;
            public decimal base_price;
            public decimal price_usd;
            public decimal price_rub;
            public decimal price_eur;
            public decimal price_uah;
            public byte in_stock;
        }

        /// <summary>
        /// Быстрое получение описаний товаров по списку ID
        /// https://my.digiseller.com/inside/api_catgoods.asp#products_list
        /// </summary>
        /// <param name="ids">идентификаторы товаров</param>
        public List<DigiProductShortInfo> GetDigiProductList(int id) => GetDigiProductList(new[] { id });
        public List<DigiProductShortInfo> GetDigiProductList(IEnumerable<int> ids)
        {
            var httpRequest = WebRequest.CreateHttp(QueryHelpers.AddQueryString("https://api.digiseller.ru/api/products/list", new Dictionary<string, string>() {
                { "ids", string.Join(',', ids) },
                { "lang", "ru-RU" }
            }));
            httpRequest.Method = WebRequestMethods.Http.Get;
            httpRequest.Accept = "application/json";
            httpRequest.ContentType = "application/json";

            using (var httpResponse = (HttpWebResponse)httpRequest.GetResponse())
            using (var stream = httpResponse.GetResponseStream())
            using (var reader = new StreamReader(stream, Encoding.UTF8))
            {
                var responseJson = reader.ReadToEnd();
                return JsonConvert.DeserializeObject<List<DigiProductShortInfo>>(responseJson);
            }
        }

        public class DigiProductInfo
        {
            public string id;
            public string id_prev;
            public string id_next;
            public string name;
            public string price;
            public string currency;
            public byte is_available;
            public string url;
            public string info;
            public string add_info;
            public string release_date;
            public string agency_fee;
            public string agency_sum;
            public string agency_id;
            public string collection;
        }
        public class DigiProductInfoResponse
        {
            public int? retval;
            public string retdesc;
            public string queryId;
            public DigiProductInfo product;
        }

        /// <summary>
        /// Описание товара.
        /// https://my.digiseller.com/inside/api_catgoods.asp#product_info
        /// </summary>
        /// <param name="productId">идентификатор товара</param>
        public DigiProductInfoResponse GetDigiProductInfo(int productId/*, int sellerId*/)
        {
            var httpRequest = WebRequest.CreateHttp(QueryHelpers.AddQueryString("https://api.digiseller.ru/api/products/info", new Dictionary<string, string>() {
                { "format", "json" },
                { "transp", "cors" },
                { "product_id", productId.ToString() },
                //{ "seller_id", sellerId.ToString() },
                { "partner_uid", Configuration["Digiseller:UId"] },
                { "currency", "RUR" },
                { "lang", "ru-RU" }
            }));
            httpRequest.Method = WebRequestMethods.Http.Get;
            httpRequest.Accept = "application/json";
            httpRequest.ContentType = "application/json";

            using (var httpResponse = (HttpWebResponse)httpRequest.GetResponse())
            using (var stream = httpResponse.GetResponseStream())
            using (var reader = new StreamReader(stream, Encoding.UTF8))
            {
                var responseJson = reader.ReadToEnd();
                return JsonConvert.DeserializeObject<DigiProductInfoResponse>(responseJson);
            }
        }

        public class DigiStatisticsAgentSales
        {
            public int id_seller;
            public long invoice_id;
            public int product_id;
            public string product_name;
            //public long product_entry_id;
            //public string product_entry;//seller-sells
            //public string date_put;//seller-sells
            public string date_pay;
            public string email;
            public string wmid;
            public decimal amount_in;
            //public decimal? amount_out;//seller-sells
            public string amount_currency;
            public decimal amount_in_usd;
            public string method_pay;
            public string ip;
            //public string partner_id;//seller-sells
            public decimal partner_percent;//agent-sales
            public decimal partner_amount;//agent-sales
            public string lang;
            public byte returned;
        }
        public class DigiStatisticsAgentSalesResponse
        {
            public int? retval;
            public string retdesc;
            public int total_rows;
            public int pages;
            public int page;
            public List<DigiStatisticsAgentSales> rows;
        }

        /// <summary>
        /// Статистика продаж.
        /// https://my.digiseller.com/inside/api_statistics.asp#statistics_agent_sales
        /// </summary>
        /// <param name="dateStart">Начальная дата</param>
        /// <param name="dateFinish">Конечная дата</param>
        /// <param name="returned">Возвраты: 0 - Включить возвраты; 1 - Исключить возвраты; 2 - Только возвраты</param>
        public DigiStatisticsAgentSalesResponse GetStatisticsAgentSales(DateTime dateStart, DateTime dateFinish, byte returned)
        {
            var page = 1;
            var response = default(DigiStatisticsAgentSalesResponse);
            var date_start = dateStart.ToString("yyyy-MM-dd HH:mm:ss");
            var date_finish = dateFinish.ToString("yyyy-MM-dd HH:mm:ss");
            var rows = 2000;

            do
            {
                var httpRequest = WebRequest.CreateHttp("https://api.digiseller.ru/api/agent-sales");
                httpRequest.Method = WebRequestMethods.Http.Post;
                httpRequest.Accept = "application/json";
                httpRequest.ContentType = "application/json";
                var sign = $"{Configuration["Digiseller:Id"]}{date_start}{date_finish}{returned}{page}{rows}{Configuration["Digiseller:GUID"]}";
                var sha256 = Hash.String(sign, Hash.Algorithm.SHA256).ToLower();
                var body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(new
                {
                    id_partner = Configuration.GetValue<int>("Digiseller:Id"),
                    //product_ids = new [] { 1954830, 2080515 },
                    date_start = date_start,
                    date_finish = date_finish,
                    returned = returned,
                    page = page,
                    rows = rows,
                    // Sha256 ({id_seller} {product_ids} {date_start} {date_finish} {возвращено} {page} {rows} {seller_secret}) (в нижнем регистре) 
                    sign = sha256//Hash.String($"{Configuration["Digiseller:Id"]}{date_start}{date_finish}{returned}{page}{2000}{Configuration["Digiseller:GUID"]}", Hash.Algorithm.SHA256).ToLower()
                }, Formatting.None));
                httpRequest.ContentLength = body.Length;
                using (var stream = httpRequest.GetRequestStream())
                {
                    stream.Write(body, 0, body.Length);
                }

                using (var httpResponse = (HttpWebResponse)httpRequest.GetResponse())
                using (var stream = httpResponse.GetResponseStream())
                using (var reader = new StreamReader(stream, Encoding.UTF8))
                {
                    var responseJson = reader.ReadToEnd();
                    var responseObject = JsonConvert.DeserializeObject<DigiStatisticsAgentSalesResponse>(responseJson);
                    if (response == null)
                    {
                        if (responseObject.retval != 0)
                            return responseObject;

                        response = responseObject;
                    }
                    else
                    {
                        if (responseObject.retval != 0)
                        {
                            response.retval = responseObject.retval;
                            response.retdesc = responseObject.retdesc;
                            return response;
                        }

                        response.total_rows = responseObject.total_rows;
                        response.pages = responseObject.pages;
                        response.page = responseObject.page;
                        response.rows.AddRange(responseObject.rows);
                    }
                }
            }
            while (response.pages > page++);

            return response;
        }
    }
}
