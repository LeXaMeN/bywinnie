﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using System;
using System.Net;

namespace Backend.Providers
{
    public enum ReCaptchaType
    {
        Visible,
        Invisible
    }

    // https://stackoverflow.com/questions/27764692/validating-recaptcha-2-no-captcha-recaptcha-in-asp-nets-server-side
    public class ReCaptchaProvider
    {
        private IConfiguration Configuration { get; }

        public ReCaptchaProvider(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        private string ReadKey(ReCaptchaType type)
        {
            switch (type)
            {
                case ReCaptchaType.Visible:
                    return Configuration["Google:ReCaptcha:Visible"];
                case ReCaptchaType.Invisible:
                    return Configuration["Google:ReCaptcha:Invisible"];
                default:
                    throw new ArgumentOutOfRangeException(nameof(ReCaptchaType), type, null);
            }
        }

        public bool IsValid(string reCaptchaResponse, ReCaptchaType type)
        {
            using (var client = new WebClient())
            {
                var response = client.DownloadString("https://www.google.com/recaptcha/api/siteverify?secret=" + ReadKey(type) + "&response=" + reCaptchaResponse);
                // new ArgumentException($"Failed to confirm captcha [invisible: {invisible}].", nameof(captchaResponse))
                return JObject.Parse(response)["success"].Value<bool>();
            }
        }
    }
}
