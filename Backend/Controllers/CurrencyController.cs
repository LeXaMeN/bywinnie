﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Controllers
{
    [Route("currencies")]
    [ApiController]
    public class CurrencyController : ControllerBase
    {
        private static string _quotesXml = string.Empty;
        private static DateTime _quotesRelevantUntil = DateTime.UtcNow.AddMinutes(30);

        [HttpGet("quotes")]
        public IActionResult GetQuotes()
        {
            try
            {
                if (_quotesXml.Length < 4000 || _quotesRelevantUntil < DateTime.UtcNow)
                {
                    var httpRequest = WebRequest.CreateHttp("http://www.cbr.ru/scripts/XML_daily_eng.asp");
                    httpRequest.Method = WebRequestMethods.Http.Get;
                    httpRequest.AllowAutoRedirect = true;
                    using (var httpResponse = (HttpWebResponse)httpRequest.GetResponse())
                    using (var stream = httpResponse.GetResponseStream())
                    using (var reader = new StreamReader(stream, Encoding.UTF8))
                    {
                        _quotesXml = reader.ReadToEnd();
                        _quotesRelevantUntil = DateTime.UtcNow.AddMinutes(30);

                        return new ContentResult() {
                            Content = _quotesXml,
                            ContentType = "application/xml",
                            StatusCode = (int)httpResponse.StatusCode
                        };
                    }
                }

                return new ContentResult() {
                    Content = _quotesXml,
                    ContentType = "application/xml",
                    StatusCode = (int)HttpStatusCode.OK
                };
            }
            catch (Exception exception)
            {
                if (exception is WebException webException && (webException.Response as HttpWebResponse)?.StatusCode == HttpStatusCode.Found)
                {
                    using (var stream = webException.Response.GetResponseStream())
                    using (var reader = new StreamReader(stream, Encoding.UTF8))
                    {
                        var quotesXml = reader.ReadToEnd();
                        if (quotesXml.Length > 4000)
                        {
                            _quotesXml = quotesXml;
                            _quotesRelevantUntil = DateTime.UtcNow.AddMinutes(30);
                        }
                        if (_quotesXml.Length > 4000)
                        {
                            return new ContentResult() {
                                Content = _quotesXml,
                                ContentType = "application/xml",
                                StatusCode = (int)HttpStatusCode.OK
                            };
                        }
                    }
                }

                return new ContentResult() {
                    Content = JsonConvert.SerializeObject(new { message = exception.Message }),
                    ContentType = "application/json",
                    StatusCode = (int)HttpStatusCode.InternalServerError
                };
            }
        }
    }
}
