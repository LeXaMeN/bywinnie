﻿using Backend.Data.Context;
using Backend.Data.Entities;
using Backend.DTO.Item;
using Backend.Enums;
using Backend.Exceptions;
using Backend.Extensions.JwtBearer;
using Backend.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Controllers
{
    [EnableCors(PolicyName.Admin)]
    [Authorize(Roles = UserRole.Admin)]
    [Route("[controller]s")]
    [ApiController]
    public class ItemController : ControllerBase
    {
        private DatabaseContext Database { get; }
        private ItemService ItemService { get; }

        public ItemController(DatabaseContext db, ItemService itemService)
        {
            Database = db;
            ItemService = itemService;
        }

        [HttpPost]
        public IActionResult Post([FromBody] ItemDTO dto)
        {
            return Ok(ItemService.Manager.Create(Database, dto.Type, dto.Name, dto.NameRus));
        }

        [HttpPut("{itemId}")]
        public IActionResult Put([FromRoute] int itemId, [FromBody] ItemDTO dto)
        {
            return Ok(ItemService.Manager.Update(Database, itemId, dto.Name, dto.NameRus));
        }

        [HttpGet("{itemId}")]
        public IActionResult Get([FromRoute] int itemId)
        {
            return Ok(ItemService.Manager.ReadonlyOrNull(Database, itemId));
        }

        [HttpGet("collection")]
        public IActionResult GetCollection(
            [FromQuery] int? id,
            [FromQuery] [EnumDataType(typeof(ItemType))] ItemType? type,
            [FromQuery] [StringLength(55)] string name,
            [FromQuery] [Required] int page,
            [FromQuery] [Required] [Range(1, 50)] int limit
        )
        {
            return Ok(ItemService.Manager.ReadCollection(Database, id, type, name, page, limit));
        }

        [HttpDelete("{itemId}")]
        public IActionResult Delete([FromRoute] int itemId, [FromQuery] [Required] [EnumDataType(typeof(ItemType))] ItemType type)
        {
            return Ok(ItemService.Manager.Delete(Database, itemId, type));
        }


        [HttpPost("{itemId}/" + nameof(ItemType.Boolean))]
        public IActionResult PostBoolean([FromRoute] int itemId, [FromBody] ItemBooleanDTO dto) => Ok(ItemService.Manager.CreateBooleanValue(Database, itemId, dto.Value));
        [HttpPut(nameof(ItemType.Boolean) + "/{itemValueId}")]
        public IActionResult PutBoolean([FromRoute] int itemValueId, [FromBody] ItemBooleanDTO dto) => Ok(ItemService.Manager.UpdateBooleanValue(Database, itemValueId, dto.Value));

        [HttpPost("{itemId}/" + nameof(ItemType.Date))]
        public IActionResult PostDate([FromRoute] int itemId, [FromBody] ItemDateDTO dto) => Ok(ItemService.Manager.CreateDateValue(Database, itemId, dto.Value.Value));
        [HttpPut(nameof(ItemType.Date) + "/{itemValueId}")]
        public IActionResult PutDate([FromRoute] int itemValueId, [FromBody] ItemDateDTO dto) => Ok(ItemService.Manager.UpdateDateValue(Database, itemValueId, dto.Value.Value));

        [HttpPost("{itemId}/" + nameof(ItemType.Decimal))]
        public IActionResult PostDecimal([FromRoute] int itemId, [FromBody] ItemDecimalDTO dto) => Ok(ItemService.Manager.CreateDecimalValue(Database, itemId, dto.Value));
        [HttpPut(nameof(ItemType.Decimal) + "/{itemValueId}")]
        public IActionResult PutDecimal([FromRoute] int itemValueId, [FromBody] ItemDecimalDTO dto) => Ok(ItemService.Manager.UpdateDecimalValue(Database, itemValueId, dto.Value));

        [HttpPost("{itemId}/" + nameof(ItemType.Enum))]
        public IActionResult PostEnum([FromRoute] int itemId, [FromBody] ItemEnumDTO dto) => Ok(ItemService.Manager.CreateEnumValue(Database, itemId, dto.Value, dto.ValueRus, dto.Order));
        [HttpPut(nameof(ItemType.Enum) + "/{itemValueId}")]
        public IActionResult PutEnum([FromRoute] int itemValueId, [FromBody] ItemEnumDTO dto) => Ok(ItemService.Manager.UpdateEnumValue(Database, itemValueId, dto.Value, dto.ValueRus, dto.Order));

        [HttpPost("{itemId}/" + nameof(ItemType.Integer))]
        public IActionResult PostInteger([FromRoute] int itemId, [FromBody] ItemIntegerDTO dto) => Ok(ItemService.Manager.CreateIntegerValue(Database, itemId, dto.Value));
        [HttpPut(nameof(ItemType.Integer) + "/{itemValueId}")]
        public IActionResult PutInteger([FromRoute] int itemValueId, [FromBody] ItemIntegerDTO dto) => Ok(ItemService.Manager.UpdateIntegerValue(Database, itemValueId, dto.Value));

        [HttpPost("{itemId}/" + nameof(ItemType.String))]
        public IActionResult PostString([FromRoute] int itemId, [FromBody] ItemStringDTO dto) => Ok(ItemService.Manager.CreateStringValue(Database, itemId, dto.Value, dto.ValueRus));
        [HttpPut(nameof(ItemType.String) + "/{itemValueId}")]
        public IActionResult PutString([FromRoute] int itemValueId, [FromBody] ItemStringDTO dto) => Ok(ItemService.Manager.UpdateStringValue(Database, itemValueId, dto.Value, dto.ValueRus));


        [HttpGet("{itemId}/values/list")]
        public IActionResult GetValueList([FromRoute] int itemId, [FromQuery] [Required] [EnumDataType(typeof(ItemType))] ItemType type)
        {
            return Ok(ItemService.Manager.ReadonlyValueList(Database, itemId, type));
        }

        [HttpDelete("values/{itemValueId}")]
        public IActionResult DeleteValue([FromRoute] int itemValueId, [FromQuery] [Required] [EnumDataType(typeof(ItemType))] ItemType type)
        {
            return Ok(ItemService.Manager.DeleteValue(Database, itemValueId, type));
        }
    }
}
