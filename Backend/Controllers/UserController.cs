﻿using Backend.Data.Context;
using Backend.Data.Entities;
//using Backend.DTO.User;
using Backend.Enums;
using Backend.Exceptions;
using Backend.Extensions.JwtBearer;
using Backend.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Controllers
{
    [Route("[controller]s")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private DatabaseContext Database { get; }
        private UserService UserService { get; }

        public UserController(DatabaseContext db, UserService userService)
        {
            Database = db;
            UserService = userService;
        }

        [HttpGet]
        [Authorize]
        public IActionResult Get()
        {
            try
            {
                return Ok(UserService.Manager.Readonly(Database, User.GetId()));
            }
            catch (Exception exception)
            {
                throw new HttpResponseException(exception, nameof(Get), new { UserId = User.GetId() });
            }
        }

        #region Admin
        [HttpGet("{userId}")]
        [EnableCors(PolicyName.Admin)]
        [Authorize(Roles = UserRole.Admin)]
        public IActionResult GetByAdmin([FromRoute] int userId)
        {
            try
            {
                return Ok(UserService.Manager.Readonly(Database, userId));
            }
            catch (Exception exception)
            {
                throw new HttpResponseException(exception, nameof(GetByAdmin), new { AdminId = User.GetId(), UserId = userId });
            }
        }

        [HttpGet("collection")]
        [EnableCors(PolicyName.Admin)]
        [Authorize(Roles = UserRole.Admin)]
        public IActionResult GetCollectionByAdmin(
            [FromQuery] int? userId,
            [FromQuery] string login,
            [FromQuery] string role,
            [FromQuery] [Required] int page,
            [FromQuery] [Required] [Range(1, 50)] int limit
        )
        {
            try
            {
                return Ok(UserService.Manager.ReadCollection(Database, userId, login, role, page, limit));
            }
            catch (Exception exception)
            {
                throw new HttpResponseException(exception, nameof(GetCollectionByAdmin), new { AdminId = User.GetId(), Login = login, Role = role, Page = page, Limit = limit });
            }
        }

        //[HttpPost("{userId}/roles/{role}")]
        //[EnableCors(PolicyName.Admin)]
        //[Authorize(Roles = UserRole.Admin)]
        //public IActionResult PostRole([FromRoute] int userId, [FromRoute] string role)
        //{
        //    try
        //    {
        //        return Ok(UserService.CreateRole(Database, userId, role));
        //    }
        //    catch (Exception exception)
        //    {
        //        throw new HttpResponseException(exception, nameof(PostRole), new { AdminId = User.GetId(), UserId = userId, Role = role });
        //    }
        //}

        //[HttpDelete("{userId}/roles/{role}")]
        //[EnableCors(PolicyName.Admin)]
        //[Authorize(Roles = UserRole.Admin)]
        //public IActionResult DeleteRole([FromRoute] int userId, [FromRoute] string role)
        //{
        //    try
        //    {
        //        return Ok(UserService.RemoveRole(Database, userId, role));
        //    }
        //    catch (Exception exception)
        //    {
        //        throw new HttpResponseException(exception, nameof(DeleteRole), new { AdminId = User.GetId(), UserId = userId, Role = role });
        //    }
        //}
        #endregion
    }
}
