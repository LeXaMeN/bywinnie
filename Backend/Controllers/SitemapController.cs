﻿using Backend.Data.Context;
using Backend.Managers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Controllers
{
    [Route("sitemap")]
    [ApiController]
    public class SitemapController : ControllerBase
    {
        private IConfiguration Configuration { get; }
        public DatabaseContext Database { get; }

        public SitemapController(IConfiguration configuration, DatabaseContext db)
        {
            Configuration = configuration;
            Database = db;
        }

        [HttpGet("xml")]
        public IActionResult GetXml()
        {
            var urlProduct = Configuration["Host:Url"] + Configuration["Host:Links:Product"];
            var products = Database.Products.Select(p => new { p.Id, p.Index }).ToList();
            var builder = new StringBuilder();
            // http://site-on.net/optimization/6-sitemap
            builder.AppendLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            builder.AppendLine("<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\" xmlns:xhtml=\"http://www.w3.org/1999/xhtml\">");
            for (int i = 0; i < products.Count; i++)
            {
                builder.AppendLine("\t<url>");
                builder.AppendLine($"\t\t<loc>{urlProduct}/{products[i].Index}/{products[i].Id}</loc>");
                builder.AppendLine("\t</url>");
            }
            builder.AppendLine("</urlset>");
            
            return new ContentResult() {
                Content = builder.ToString(),
                ContentType = "text/xml",
                StatusCode = (int)HttpStatusCode.OK
            };
        }

        [HttpGet("txt")]
        public IActionResult GetTxt()
        {
            var urlProduct = Configuration["Host:Url"] + Configuration["Host:Links:Product"];
            var products = Database.Products.Select(p => new { p.Id, p.Index }).ToList();
            var builder = new StringBuilder();
            for (int i = 0; i < products.Count; i++)
            {
                builder.AppendLine($"{urlProduct}/{products[i].Index}/{products[i].Id}");
            }

            return new ContentResult() {
                Content = builder.ToString(),
                ContentType = "text/plain",
                StatusCode = (int)HttpStatusCode.OK
            };
        }
    }
}
