﻿using Backend.Data.Context;
using Backend.Data.Entities;
using Backend.Enums;
using Backend.Managers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Controllers
{
    [EnableCors(PolicyName.Admin)]
    [Authorize(Roles = UserRole.Admin)]
    [Route("[controller]s")]
    [ApiController]
    public class SiteController : ControllerBase
    {
        private DatabaseContext Database { get; }
        private SiteManager SiteManager { get; }

        public SiteController(DatabaseContext db, SiteManager siteManager)
        {
            Database = db;
            SiteManager = siteManager;
        }

        [HttpPost]
        public IActionResult Post([FromQuery] [Required] [StringLength(255)] [RegularExpression(Site.DomainRegex)] string domain, [FromQuery] [Required] bool allowed)
        {
            return Ok(SiteManager.Create(Database, domain, allowed));
        }

        [HttpPut("{siteId}")]
        public IActionResult Put([FromRoute] int siteId, [FromQuery] [Required] [StringLength(255)] [RegularExpression(Site.DomainRegex)] string domain, [FromQuery] [Required] bool allowed)
        {
            return Ok(SiteManager.Update(Database, siteId, domain, allowed));
        }

        [AllowAnonymous]
        [EnableCors(PolicyName.Default)]
        [HttpGet("domains/list")]
        public IActionResult GetDomainList([FromQuery] [Required] bool allowed)
        {
            return Ok(SiteManager.ReadonlyDomainList(Database, allowed));
        }

        [HttpGet("collection")]
        public IActionResult GetCollection(
            [FromQuery] int? id,
            [FromQuery] string domain,
            [FromQuery] bool? allowed,
            [FromQuery] [Required] int page,
            [FromQuery] [Required] [Range(1, 50)] int limit
        )
        {
            return Ok(SiteManager.ReadCollection(Database, id, domain, allowed, page, limit));
        }

        [HttpDelete("{siteId}")]
        public IActionResult Delete([FromRoute] int siteId)
        {
            return Ok(SiteManager.Delete(Database, siteId));
        }
    }
}
