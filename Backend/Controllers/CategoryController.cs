﻿using Backend.Data.Context;
using Backend.Data.Entities;
using Backend.DTO.Category;
using Backend.Enums;
using Backend.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Controllers
{
    [EnableCors(PolicyName.Admin)]
    [Authorize(Roles = UserRole.Admin)]
    [Route("categories")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private DatabaseContext Database { get; }
        private CategoryService CategoryService { get; }

        public CategoryController(DatabaseContext db, CategoryService categoryService)
        {
            Database = db;
            CategoryService = categoryService;
        }

        [HttpPost]
        public IActionResult Post([FromBody] CategoryDTO dto)
        {
            return Ok(CategoryService.Manager.Create(Database, dto.ParentId, dto.Index, dto.Name, dto.NameRus));
        }

        [HttpPut("{categoryId}")]
        public IActionResult Put([FromRoute] int categoryId, [FromBody] CategoryDTO dto)
        {
            return Ok(CategoryService.Manager.Update(Database, categoryId, dto.Index, dto.Name, dto.NameRus));
        }

        [HttpGet("{categoryId}")]
        public IActionResult Get([FromRoute] int categoryId)
        {
            return Ok(CategoryService.Manager.ReadonlyOrNull(Database, categoryId));
        }

        [AllowAnonymous]
        [EnableCors(PolicyName.Default)]
        [HttpGet("list")]
        public IActionResult GetList([FromQuery] [Range(1, int.MaxValue)] int? parentId)
        {
            return Ok(CategoryService.Manager.ReadonlyList(Database, parentId));
        }

        [HttpDelete("{categoryId}")]
        public IActionResult Delete([FromRoute] int categoryId)
        {
            return Ok(CategoryService.Manager.Delete(Database, categoryId));
        }
    }
}
