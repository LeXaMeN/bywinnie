﻿using Backend.Data.Context;
using Backend.Data.Entities;
using Backend.DTO.Product;
using Backend.Enums;
using Backend.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Controllers
{
    [EnableCors(PolicyName.Admin)]
    [Authorize(Roles = UserRole.Admin)]
    [Route("[controller]s")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private DatabaseContext Database { get; }
        private ProductService ProductService { get; }

        public ProductController(DatabaseContext db, ProductService productService)
        {
            Database = db;
            ProductService = productService;
        }

        #region Products
        [HttpPost]
        public IActionResult Post([FromBody] ProductDTO dto)
        {
            return Ok(ProductService.Manager.Create(Database, dto.CategoryId, dto.Index, dto.Name, dto.NameRus, dto.Description, dto.DescriptionRus));
        }

        [HttpPut("{productId}")]
        public IActionResult Put([FromRoute] int productId, ProductDTO dto)
        {
            return Ok(ProductService.Manager.Update(Database, productId, dto.CategoryId, dto.Index, dto.Name, dto.NameRus, dto.Description, dto.DescriptionRus, dto.Price));
        }

        [AllowAnonymous]
        [EnableCors(PolicyName.Default)]
        [HttpPut("{productId}/price")]
        public IActionResult PutPrice([FromRoute] int productId,
            [FromQuery] [Required] [StringLength(55, MinimumLength = 1)] string index,
            [FromQuery] [Required] [Range(-999999.99, 999999.99)] decimal currentPrice
        )
        {
            return Ok(ProductService.UpdatePriceAnonymously(Database, productId, index, currentPrice));
        }

        [AllowAnonymous]
        [EnableCors(PolicyName.Default)]
        [HttpGet("{productId}")]
        public IActionResult Get([FromRoute] int productId)
        {
            return Ok(ProductService.ReadonlyOrNull(Database, productId));
        }

        [AllowAnonymous]
        [EnableCors(PolicyName.Default)]
        [HttpPost("collection")]
        public IActionResult GetCollection([FromBody] ProductCollectionDTO dto,
            [FromQuery] [Required] [Range(1, int.MaxValue)] int page,
            [FromQuery] [Required] [Range(1, 50)] int limit
        )
        {
            return Ok(ProductService.ReadCollection(Database, dto, page, limit));
        }

        [HttpDelete("{productId}")]
        public IActionResult Delete([FromRoute] int productId)
        {
            return Ok(ProductService.Manager.Delete(Database, productId));
        }
        #endregion

        #region Files
        [HttpPost("{productId}/files")]
        public IActionResult PostFile([FromRoute] int productId, [FromBody] ProductFileDTO dto)
        {
            return Ok(ProductService.FileManager.Create(Database, productId, dto.Type, dto.Url, dto.Order));
        }

        [HttpPut("{productId}/files/{productFileId}")]
        public IActionResult PutFile([FromRoute] int productId, [FromRoute] int productFileId, ProductFileDTO dto)
        {
            return Ok(ProductService.FileManager.Update(Database, productFileId, productId, dto.Type, dto.Url, dto.Order));
        }

        [HttpGet("{productId}/files/{productFileId}")]
        public IActionResult GetFile([FromRoute] int productId, [FromRoute] int productFileId)
        {
            return Ok(ProductService.FileManager.ReadonlyOrNull(Database, productFileId, productId));
        }

        [AllowAnonymous]
        [EnableCors(PolicyName.Default)]
        [HttpGet("{productId}/files/list")]
        public IActionResult GetFileList([FromRoute] int productId, [FromQuery] [EnumDataType(typeof(ProductFileType))] ProductFileType? type)
        {
            return Ok(ProductService.FileManager.ReadonlyList(Database, productId, type));
        }

        [HttpDelete("{productId}/files/{productFileId}")]
        public IActionResult DeleteFile([FromRoute] int productId, [FromRoute] int productFileId)
        {
            return Ok(ProductService.FileManager.Delete(Database, productFileId, productId));
        }
        #endregion
    }
}
