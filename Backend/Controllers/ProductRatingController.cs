﻿using Backend.Data.Context;
using Backend.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Controllers
{
    [Route("products/rating")]
    [ApiController]
    public class ProductRatingController : ControllerBase
    {
        private DatabaseContext Database { get; }
        private ProductRatingService ProductRatingService { get; }

        public ProductRatingController(DatabaseContext db, ProductRatingService productRatingService)
        {
            Database = db;
            ProductRatingService = productRatingService;
        }

        [HttpGet("list/sorted")]
        public IActionResult GetSortedProductList(
            [FromQuery] [Range(1, int.MaxValue)] int? categoryId,
            [FromQuery] [Range(1, 50)] int limit
        )
        {
            return Ok(ProductRatingService.ReadonlySortedByRatingList(Database, categoryId, limit));
        }
    }
}
