﻿using Backend.Data.Context;
using Backend.Data.Entities;
using Backend.DTO.ProductPart;
using Backend.Enums;
using Backend.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Controllers
{
    [EnableCors(PolicyName.Admin)]
    [Authorize(Roles = UserRole.Admin)]
    [Route("products")]
    [ApiController]
    public class ProductPartController : ControllerBase
    {
        private DatabaseContext Database { get; }
        private ProductService ProductService { get; }

        public ProductPartController(DatabaseContext database, ProductService productService)
        {
            Database = database;
            ProductService = productService;
        }

        #region Categories
        [HttpPost("parts/categories")]
        public IActionResult PostCategory([FromBody] ProductPartCategoryDTO dto)
        {
            return Ok(ProductService.PartManager.CreateCategory(Database, dto.Name, dto.NameRus));
        }

        [HttpPut("parts/categories/{productPartCategoryId}")]
        public IActionResult PutCategory([FromRoute] int productPartCategoryId, [FromBody] ProductPartCategoryDTO dto)
        {
            return Ok(ProductService.PartManager.UpdateCategory(Database, productPartCategoryId, dto.Name, dto.NameRus));
        }

        [HttpGet("parts/categories/{productPartCategoryId}")]
        public IActionResult GetCategory([FromRoute] int productPartCategoryId)
        {
            return Ok(ProductService.PartManager.ReadonlyCategoryOrNull(Database, productPartCategoryId));
        }

        [HttpGet("parts/categories/list")]
        public IActionResult GetCategoryList()
        {
            return Ok(ProductService.PartManager.ReadonlyCategoryList(Database));
        }

        [HttpDelete("parts/categories/{productPartCategoryId}")]
        public IActionResult DeleteCategory([FromRoute] int productPartCategoryId)
        {
            return Ok(ProductService.PartManager.DeleteCategory(Database, productPartCategoryId));
        }
        #endregion

        #region Values
        [HttpPost("parts/values")]
        public IActionResult PostValue([FromBody] ProductPartValueDTO dto)
        {
            return Ok(ProductService.PartManager.CreateValue(Database, dto.ProductId, dto.ProductPartCategoryId, dto.ItemValueId, dto.ItemType));
        }

        [AllowAnonymous]
        [EnableCors(PolicyName.Default)]
        [HttpGet("{productId}/parts/values/list")]
        public IActionResult GetValueList([FromRoute] int productId)
        {
            return Ok(ProductService.PartManager.ReadonlyValueList(Database, productId));
        }

        [HttpDelete("parts/values/{productPartValueId}")]
        public IActionResult DeleteValue([FromRoute] int productPartValueId, [FromBody] ProductPartValueDTO dto)
        {
            return Ok(ProductService.PartManager.DeleteValue(Database, productPartValueId, dto.ProductId, dto.ProductPartCategoryId, dto.ItemValueId, dto.ItemType));
        }
        #endregion

        #region Sequences
        [HttpPut("parts/sequences")]
        public IActionResult PutSequence([FromBody] ProductPartSequenceDTO dto)
        {
            return Ok(ProductService.PartManager.CreateOrUpdateSequence(Database, dto.CategoryId, dto.Categories, dto.Dictionary));
        }

        [AllowAnonymous]
        [EnableCors(PolicyName.Default)]
        [HttpGet("parts/sequences/{categoryId}")]
        public IActionResult GetSequence([FromRoute] int categoryId)
        {
            return Ok(ProductService.PartManager.ReadonlySequenceOrNull(Database, categoryId));
        }

        [AllowAnonymous]
        [EnableCors(PolicyName.Default)]
        [HttpGet("parts/sequences/{categoryId}/data/optional")]
        public IActionResult GetSequenceDataOrNull([FromRoute] int categoryId)
        {
            return Ok(ProductService.ReadonlySequenceDataOrNull(Database, categoryId));
        }

        [HttpGet("parts/sequences/{categoryId}/data")]
        public IActionResult GetSequenceData([FromRoute] int categoryId)
        {
            return Ok(ProductService.ReadonlySequenceData(Database, categoryId));
        }

        [HttpDelete("parts/sequences/{categoryId}")]
        public IActionResult DeleteSequence([FromRoute] int categoryId)
        {
            return Ok(ProductService.PartManager.DeleteSequence(Database, categoryId));
        }
        #endregion
    }
}
