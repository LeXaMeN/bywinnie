﻿using Backend.Data.Context;
using Backend.Data.Entities;
using Backend.DTO.Product;
using Backend.Enums;
using Backend.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Controllers
{
    [EnableCors(PolicyName.Admin)]
    [Authorize(Roles = UserRole.Admin)]
    [Route("products")]
    [ApiController]
    public class ProductDigiController : ControllerBase
    {
        private DatabaseContext Database { get; }
        private ProductDigiService ProductDigiService { get; }

        public ProductDigiController(DatabaseContext db, ProductDigiService productDigiService)
        {
            Database = db;
            ProductDigiService = productDigiService;
        }

        [HttpPost("{productId}/digis/{digiId}")]
        public IActionResult Post([FromRoute] int productId, [FromRoute] int digiId, [FromBody] ProductDigiDTO dto)
        {
            return Ok(ProductDigiService.Create(Database, digiId, productId, dto));
        }

        [HttpPut("{productId}/digis/{digiId}")]
        public IActionResult Put([FromRoute] int productId, [FromRoute] int digiId, [FromBody] ProductDigiDTO dto)
        {
            return Ok(ProductDigiService.Manager.Update(Database, digiId, dto.Allowed ?? true, dto.Name, dto.NameChanged ?? false, dto.AgencyFee, dto.AgencyFeeChanged ?? false));
        }

        [AllowAnonymous]
        [EnableCors(PolicyName.Default)]
        [HttpPost("{productId}/digis/{digiId}/changes")]
        public IActionResult PostChanges([FromRoute] int productId, [FromRoute] int digiId, [FromBody] ProductDigiDTO dto)
        {
            return Ok(ProductDigiService.UpdateAnonymously(Database, digiId, productId, dto));
        }

        [HttpGet("digis/{digiId}")]
        public IActionResult Get([FromRoute] int digiId)
        {
            return Ok(ProductDigiService.Manager.ReadonlyOrNull(Database, digiId));
        }

        [AllowAnonymous]
        [EnableCors(PolicyName.Default)]
        [HttpGet("{productId}/digis/list")]
        public IActionResult GetList([FromRoute] int productId)
        {
            return Ok(ProductDigiService.Manager.ReadonlyList(Database, productId));
        }

        [HttpGet("digis/collection")]
        public IActionResult GetCollection(
            [FromQuery] [Range(1, int.MaxValue)] int? id,
            [FromQuery] [Range(1, int.MaxValue)] int? productId,
            [FromQuery] [Required] bool allowed,
            [FromQuery] [Required] bool nameChanged,
            [FromQuery] bool? agencyFeeChanged,
            [FromQuery] [Required] [Range(1, int.MaxValue)] int page,
            [FromQuery] [Required] [Range(1, 50)] int limit
        )
        {
            return Ok(ProductDigiService.Manager.ReadCollection(Database, id, productId, allowed, nameChanged, agencyFeeChanged, page, limit));
        }

        [HttpDelete("{productId}/digis/{digiId}")]
        public IActionResult Delete([FromRoute] int productId, [FromRoute] int digiId)
        {
            return Ok(ProductDigiService.Manager.Delete(Database, digiId, productId));
        }
    }
}
