﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Data.Entities
{
    [Table(nameof(Category))]
    public class Category
    {
        public int Id { get; set; }

        public int? ParentId { get; set; }
        public Category Parent { get; set; }

        [MaxLength(55)]
        public string Index { get; set; }
        [MaxLength(55)]
        public string Name { get; set; }
        [MaxLength(55)]
        public string NameRus { get; set; }

        public List<Category> Subcategories { get; set; }
    }

    public class CategoryConfiguration : IEntityTypeConfiguration<Category>
    {
        public void Configure(EntityTypeBuilder<Category> builder)
        {
            builder.HasIndex(p => p.Index).IsUnique();
            //builder.HasIndex(p => p.Name);
        }
    }
}
