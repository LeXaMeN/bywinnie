﻿using Backend.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Data.Entities
{
    [Table(nameof(ItemDate))]
    public class ItemDate : IItemValue
    {
        public int Id { get; set; }
        public ItemType Type => ItemType.Date;

        public int ItemId { get; set; }
        public Item Item { get; set; }

        //[DataType(DataType.Date)]
        [Column(TypeName = ColumnType.Date)]
        public DateTime Value { get; set; }
    }
}
