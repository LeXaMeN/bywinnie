﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Data.Entities
{
    public interface IItemValue
    {
        int Id { get; set; }
        ItemType Type { get; }

        int ItemId { get; set; }
        Item Item { get; set; }
    }
}
