﻿using Backend.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Data.Entities
{
    public enum ItemType : sbyte
    {
        Boolean = TypeCode.Boolean,
        Date = -TypeCode.DateTime,
        Decimal = TypeCode.Decimal,
        Enum = -TypeCode.Int32,
        Integer = TypeCode.Int32,
        String = TypeCode.String,
    }

    [Table(nameof(Item))]
    public class Item
    {
        public int Id { get; set; }
        [Column(TypeName = ColumnType.SByte)]
        public ItemType Type { get; set; }
        [MaxLength(55)]
        public string Name { get; set; }
        [MaxLength(55)]
        public string NameRus { get; set; }

        [NotMapped]
        public IList Values { get; set; }
    }

    public class ItemConfiguration : IEntityTypeConfiguration<Item>
    {
        public void Configure(EntityTypeBuilder<Item> builder)
        {
            builder.HasIndex(i => i.Type);
        }
    }
}
