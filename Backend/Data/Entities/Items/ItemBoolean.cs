﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Data.Entities
{
    [Table(nameof(ItemBoolean))]
    public class ItemBoolean : IItemValue
    {
        public int Id { get; set; }
        public ItemType Type => ItemType.Boolean;

        public int ItemId { get; set; }
        public Item Item { get; set; }

        public bool Value { get; set; }
    }
}
