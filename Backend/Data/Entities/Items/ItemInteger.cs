﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Data.Entities
{
    [Table(nameof(ItemInteger))]
    public class ItemInteger : IItemValue
    {
        public int Id { get; set; }
        public ItemType Type => ItemType.Integer;

        public int ItemId { get; set; }
        public Item Item { get; set; }

        public int Value { get; set; }
    }
}