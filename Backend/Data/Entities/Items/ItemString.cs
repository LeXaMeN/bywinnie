﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Data.Entities
{
    [Table(nameof(ItemString))]
    public class ItemString : IItemValue
    {
        public int Id { get; set; }
        public ItemType Type => ItemType.String;

        public int ItemId { get; set; }
        public Item Item { get; set; }

        [MaxLength(1000)]
        public string Value { get; set; }
        [MaxLength(1000)]
        public string ValueRus { get; set; }
    }
}