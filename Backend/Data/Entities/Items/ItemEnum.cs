﻿using Backend.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Data.Entities
{
    [Table(nameof(ItemEnum))]
    public class ItemEnum : IItemValue
    {
        public int Id { get; set; }
        public ItemType Type => ItemType.Enum;

        public int ItemId { get; set; }
        public Item Item { get; set; }

        [MaxLength(55)]
        public string Value { get; set; }
        [MaxLength(55)]
        public string ValueRus { get; set; }
        [Column(TypeName = ColumnType.Int16)]
        public short Order { get; set; }
    }
}