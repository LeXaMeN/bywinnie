﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Data.Entities
{
    [Table(nameof(ItemDecimal))]
    public class ItemDecimal : IItemValue
    {
        public int Id { get; set; }
        public ItemType Type => ItemType.Decimal;

        public int ItemId { get; set; }
        public Item Item { get; set; }

        [Column(TypeName = "decimal(8,2)")]
        public decimal Value { get; set; }
    }
}