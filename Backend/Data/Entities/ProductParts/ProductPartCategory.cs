﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Data.Entities
{
    [Table(nameof(ProductPartCategory))]
    public class ProductPartCategory
    {
        public int Id { get; set; }
        [MaxLength(55)]
        public string Name { get; set; }
        [MaxLength(55)]
        public string NameRus { get; set; }
    }
}
