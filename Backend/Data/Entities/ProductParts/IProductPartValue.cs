﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Data.Entities
{
    public interface IProductPartValue
    {
        int Id { get; set; }

        int ProductId { get; set; }
        Product Product { get; set; }
        int CategoryId { get; set; }
        ProductPartCategory Category { get; set; }
        int ItemValueId { get; set; }
        //IItemValue ItemValue { get; set; }
    }
}
