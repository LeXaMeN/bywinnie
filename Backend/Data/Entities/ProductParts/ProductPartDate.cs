﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Data.Entities
{
    [Table(nameof(ProductPartDate))]
    public class ProductPartDate : IProductPartValue
    {
        public int Id { get; set; }

        public int ProductId { get; set; }
        public Product Product { get; set; }
        public int CategoryId { get; set; }
        public ProductPartCategory Category { get; set; }
        public int ItemValueId { get; set; }
        public ItemDate ItemValue { get; set; }
    }
}
