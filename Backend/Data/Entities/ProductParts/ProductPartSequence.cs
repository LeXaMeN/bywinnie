﻿using Backend.Enums;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Data.Entities
{
    [Table(nameof(ProductPartSequence))]
    public class ProductPartSequence
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [ForeignKey(nameof(Category))]
        public int CategoryId { get; set; }
        public Category Category { get; set; }

        [JsonIgnore]
        [Column(TypeName = ColumnType.Text)]
        public string CategoriesJson { get; set; }
        /// <summary>
        /// Dictionary(<see cref="ProductPartCategory.Id"/>, List[<see cref="Item.Id"/>]).
        /// </summary>
        [NotMapped]
        public List<int> Categories
        {
            get => JsonConvert.DeserializeObject<List<int>>(CategoriesJson ?? "[]");
            set => CategoriesJson = JsonConvert.SerializeObject(value, Formatting.None);
        }
        [JsonIgnore]
        [Column(TypeName = ColumnType.Text)]
        public string DictionaryJson { get; set; }
        /// <summary>
        /// Dictionary(<see cref="ProductPartCategory.Id"/>, List[<see cref="Item.Id"/>]).
        /// </summary>
        [NotMapped]
        public Dictionary<int, List<int>> Dictionary
        {
            get => JsonConvert.DeserializeObject<Dictionary<int, List<int>>>(DictionaryJson ?? "{}");
            set => DictionaryJson = JsonConvert.SerializeObject(value, Formatting.None);
        }
    }
}
