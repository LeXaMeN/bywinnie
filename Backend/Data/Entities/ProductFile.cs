﻿using Backend.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Data.Entities
{
    public enum ProductFileType : byte
    {
        Header = 0,
        Screenshot = 1,
        Youtube = 2
    }

    [Table(nameof(ProductFile))]
    public class ProductFile
    {
        public int Id { get; set; }

        public int ProductId { get; set; }
        public Product Product { get; set; }

        [Column(TypeName = ColumnType.Byte)]
        public ProductFileType Type { get; set; }

        [MaxLength(255)]
        public string Url { get; set; }

        [Column(TypeName = ColumnType.Int16)]
        public short Order { get; set; }
    }
}
