﻿using Backend.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Data.Entities
{
    [Table(nameof(Product))]
    public class Product
    {
        public int Id { get; set; }

        public int CategoryId { get; set; }
        public Category Category { get; set; }

        [MaxLength(55)]
        public string Index { get; set; }
        [MaxLength(55)]
        public string Name { get; set; }
        [MaxLength(55)]
        public string NameRus { get; set; }
        [MaxLength(255)]
        public string Description { get; set; }
        [MaxLength(255)]
        public string DescriptionRus { get; set; }
        /// <summary>
        /// Price in RUB
        /// </summary>
        [Column(TypeName = "decimal(8,2)")]
        public decimal Price { get; set; }

        public List<ProductDigi> Digis { get; set; }
        public List<ProductFile> Files { get; set; }
    }

    public class ProductConfiguration : IEntityTypeConfiguration<Product>
    {
        public void Configure(EntityTypeBuilder<Product> builder)
        {
            builder.HasIndex(p => p.Index).IsUnique();
            builder.HasIndex(p => new { p.Name, p.Description }).ForMySqlIsFullText();
            builder.HasIndex(p => new { p.NameRus, p.DescriptionRus }).ForMySqlIsFullText();
            builder.HasIndex(p => p.Price);
        }
    }
}
