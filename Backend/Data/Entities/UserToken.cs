﻿using Backend.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Data.Entities
{
    public enum UserTokenType : byte
    {
        //Registration = 1,
        RefreshToken = 2,
        //PasswordReset = 3
    }

    [Table(nameof(UserToken))]
    public class UserToken
    {
        public long Id { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }

        [Column(TypeName = ColumnType.Byte)]
        public UserTokenType Type { get; set; }

        [MaxLength(128)] // sha512
        public string Value { get; set; }

        [MaxLength(15)]
        public string IpAddress { get; set; }

        public DateTime ExpiresAt { get; set; }
    }
}
