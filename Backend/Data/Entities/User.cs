﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Data.Entities
{
    public static class UserRole
    {
        public const string User = "user";
        //public const string Partner = "partner";
        public const string Admin = "admin";

        public static bool IsValid(string role) => Providers.UserRoleProvider.IsValid(role);

        //public const string UserOrPartner = "user,partner";
        //public const string PartnerOrAdmin = "partner,admin";
        //public const string All = "user,partner,admin";
    }

    [Table(nameof(User))]
    public class User
    {
        [Key]
        public int Id { get; set; }

        [MaxLength(20)]
        public string Login { get; set; }

        [JsonIgnore]
        [MaxLength(84)]
        public string Password { get; set; }

        [JsonIgnore]
        [MaxLength(26)]
        public string RolesJson { get; set; }
        [NotMapped]
        public List<string> Roles
        {
            get => JsonConvert.DeserializeObject<List<string>>(RolesJson ?? "[]");
            /// Управление через <see cref="Providers.UserRoleProvider"/>
            //set => RolesJson = JsonConvert.SerializeObject(value, Formatting.None);
        }
    }

    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasIndex(u => u.Login).IsUnique();
        }
    }
}
