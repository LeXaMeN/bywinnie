﻿using Backend.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Data.Entities
{
    [Table(nameof(ProductDigi))]
    public class ProductDigi
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        public int ProductId { get; set; }
        public Product Product { get; set; }

        public bool Allowed { get; set; }

        [MaxLength(55)]
        public string Name { get; set; }
        public bool NameChanged { get; set; }

        //[Column(TypeName = ColumnType.Int16)]
        //public Currency BaseCurrency { get; set; }
        //[Column(TypeName = "decimal(8,2)")]
        //public decimal BasePrice { get; set; }
        //public bool InStock { get; set; }

        [Column(TypeName = ColumnType.Byte)]
        public byte AgencyFee { get; set; }
        public bool AgencyFeeChanged { get; set; }

        public DateTime UpdatedAt { get; set; }
    }

    public class ProductDigiConfiguration : IEntityTypeConfiguration<ProductDigi>
    {
        public void Configure(EntityTypeBuilder<ProductDigi> builder)
        {
            builder.HasIndex(p => new { p.Allowed, p.NameChanged, p.AgencyFeeChanged });
        }
    }
}
