﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Data.Entities
{
    [Table(nameof(Site))]
    public class Site
    {
        public int Id { get; set; }

        [MaxLength(255)]
        public string Domain { get; set; }
        public const string DomainRegex = @"^([a-zA-Z0-9][a-zA-Z0-9-_]*[a-zA-Z0-9]\.)+[a-zA-Z]{2,11}$";

        public bool Allowed { get; set; }
    }

    public class SiteConfiguration : IEntityTypeConfiguration<Site>
    {
        public void Configure(EntityTypeBuilder<Site> builder)
        {
            builder.HasIndex(u => u.Domain).IsUnique();
        }
    }
}
