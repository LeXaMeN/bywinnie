﻿using Backend.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Data.Entities
{
    [Table(nameof(ProductRating))]
    public class ProductRating
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [ForeignKey(nameof(Product))]
        public int ProductId { get; set; }
        public Product Product { get; set; }

        [Column(TypeName = ColumnType.Money)]
        public decimal ReceivedUSD { get; set; }
        [Column(TypeName = ColumnType.UInt16)]
        public ushort SalesCount { get; set; }

        public DateTime UpdatedAt { get; set; }
        //public DateTime? RemovedAt { get; set; }
    }

    public class ProductRatingDigiConfiguration : IEntityTypeConfiguration<ProductRating>
    {
        public void Configure(EntityTypeBuilder<ProductRating> builder)
        {
            builder.HasIndex(pr => pr.SalesCount);
        }
    }
}
