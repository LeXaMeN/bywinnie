﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Backend.Data.Entities
{
    [Table(nameof(GeoCountry))]
    public class GeoCountry
    {
        /// <summary>
        /// Идентификатор города
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        /// <summary>
        /// Название города
        /// </summary>
        [MaxLength(50)]
        public string Name { get; set; }
        /// <summary>
        /// Название региона
        /// </summary>
        [MaxLength(50)]
        public string Region { get; set; }
        /// <summary>
        /// Название округа
        /// </summary>
        [MaxLength(50)]
        public string District { get; set; }
        /// <summary>
        /// Широта центра города
        /// </summary>
        [Column(TypeName = "decimal(10,6)")]
        public decimal Latitude { get; set; }
        /// <summary>
        /// Долгота центра города
        /// </summary>
        [Column(TypeName = "decimal(10,6)")]
        public decimal Longitude { get; set; }
    }
}
