﻿using Backend.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Backend.Data.Entities
{
    [Table(nameof(GeoIp))]
    public class GeoIp
    {
        [Key]
        public int Id { get; set; }
        /// <summary>
        /// Начало блока
        /// </summary>
        [Column(TypeName = ColumnType.UInt32)]
        public uint StartBlock { get; set; }
        /// <summary>
        /// Конец блока
        /// </summary>
        [Column(TypeName = ColumnType.UInt32)]
        public uint EndBlock { get; set; }
        /// <summary>
        /// Блок адресов
        /// </summary>
        [MaxLength(15)]
        public string StartIp { get; set; }
        /// <summary>
        /// Блок адресов
        /// </summary>
        [MaxLength(15)]
        public string EndIp { get; set; }
        /// <summary>
        /// Страна
        /// </summary>
        [MaxLength(2)]
        public string Country { get; set; }
        [NotMapped]
        public Country2Enum? CountryEnum => Enum.TryParse(Country, out Country2Enum value) ? value : (Country2Enum?)null;
        /// <summary>
        /// Идентификатор города
        /// </summary>
        public int? GeoCountryId { get; set; }
        public GeoCountry GeoCountry { get; set; }
    }

    public class GeoIpConfiguration : IEntityTypeConfiguration<GeoIp>
    {
        public void Configure(EntityTypeBuilder<GeoIp> builder)
        {
            builder.HasIndex(ip => new { ip.StartBlock });
            builder.HasIndex(ip => new { ip.EndBlock });
        }
    }
}
