﻿using Backend.Data.Entities;
using Backend.Extensions.Database;
using Microsoft.EntityFrameworkCore;

namespace Backend.Data.Context
{
    public class DatabaseContext : DbContext
    {
        public DbSet<GeoCountry> GeoCountries { get; set; }
        public DbSet<GeoIp> GeoIps { get; set; }

        public DbSet<Category> Categories { get; set; }

        public DbSet<Item> Items { get; set; }
        public DbSet<ItemBoolean> ItemBooleans { get; set; }
        public DbSet<ItemDate> ItemDates { get; set; }
        public DbSet<ItemDecimal> ItemDecimals { get; set; }
        public DbSet<ItemEnum> ItemEnums { get; set; }
        public DbSet<ItemInteger> ItemIntegers { get; set; }
        public DbSet<ItemString> ItemStrings { get; set; }

        public DbSet<Product> Products { get; set; }
        public DbSet<ProductDigi> ProductDigis { get; set; }
        public DbSet<ProductFile> ProductFiles { get; set; }
        public DbSet<ProductRating> ProductRatings { get; set; }

        public DbSet<ProductPartBoolean> ProductPartBooleans { get; set; }
        public DbSet<ProductPartCategory> ProductPartCategories { get; set; }
        public DbSet<ProductPartDate> ProductPartDates { get; set; }
        public DbSet<ProductPartDecimal> ProductPartDecimals { get; set; }
        public DbSet<ProductPartEnum> ProductPartEnums { get; set; }
        public DbSet<ProductPartInteger> ProductPartIntegers { get; set; }
        public DbSet<ProductPartSequence> ProductPartSequences { get; set; }
        public DbSet<ProductPartString> ProductPartStrings { get; set; }

        public DbSet<Site> Sites { get; set; }

        public DbSet<User> Users { get; set; }
        public DbSet<UserToken> UserTokens { get; set; }

        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder
                .ApplyConfiguration(new CategoryConfiguration())
                .ApplyConfiguration(new ItemConfiguration())
                .ApplyConfiguration(new ProductConfiguration())
                .ApplyConfiguration(new ProductDigiConfiguration())
                .ApplyConfiguration(new SiteConfiguration())
                .ApplyConfiguration(new UserConfiguration())
                .ApplyConfiguration(new GeoIpConfiguration())
                .UseCoordinatedUniversalTimeConversion()
                .UseMySqlTextFunctions();
        }
    }
}
