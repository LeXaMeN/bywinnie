﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Backend.Data.Migrations
{
    public partial class _1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProductFiles_Product_ProductId",
                table: "ProductFiles");

            migrationBuilder.DropIndex(
                name: "IX_Product_Name",
                table: "Product");

            migrationBuilder.DropIndex(
                name: "IX_Product_NameRus",
                table: "Product");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ProductFiles",
                table: "ProductFiles");

            migrationBuilder.RenameTable(
                name: "ProductFiles",
                newName: "ProductFile");

            // BUG https://www.google.by/search?q=maria+db+rename+index+doesn%27t+work
            /*migrationBuilder.RenameIndex(
                name: "IX_ProductFiles_ProductId",
                table: "ProductFile",
                newName: "IX_ProductFile_ProductId");*/
            // FIX
            migrationBuilder.DropIndex(
                name: "IX_ProductFiles_ProductId",
                table: "ProductFile");
            migrationBuilder.CreateIndex(
                name: "IX_ProductFile_ProductId",
                table: "ProductFile",
                column: "ProductId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ProductFile",
                table: "ProductFile",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_Product_Name_Description",
                table: "Product",
                columns: new[] { "Name", "Description" })
                .Annotation("MySql:FullTextIndex", true);

            migrationBuilder.CreateIndex(
                name: "IX_Product_NameRus_DescriptionRus",
                table: "Product",
                columns: new[] { "NameRus", "DescriptionRus" })
                .Annotation("MySql:FullTextIndex", true);

            migrationBuilder.AddForeignKey(
                name: "FK_ProductFile_Product_ProductId",
                table: "ProductFile",
                column: "ProductId",
                principalTable: "Product",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProductFile_Product_ProductId",
                table: "ProductFile");

            migrationBuilder.DropIndex(
                name: "IX_Product_Name_Description",
                table: "Product");

            migrationBuilder.DropIndex(
                name: "IX_Product_NameRus_DescriptionRus",
                table: "Product");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ProductFile",
                table: "ProductFile");

            migrationBuilder.RenameTable(
                name: "ProductFile",
                newName: "ProductFiles");

            // BUG https://www.google.by/search?q=maria+db+rename+index+doesn%27t+work
            /*migrationBuilder.RenameIndex(
                name: "IX_ProductFile_ProductId",
                table: "ProductFiles",
                newName: "IX_ProductFiles_ProductId");*/
            // FIX
            migrationBuilder.DropIndex(
                name: "IX_ProductFile_ProductId",
                table: "ProductFiles");
            migrationBuilder.CreateIndex(
                name: "IX_ProductFiles_ProductId",
                table: "ProductFiles",
                column: "ProductId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ProductFiles",
                table: "ProductFiles",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_Product_Name",
                table: "Product",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_Product_NameRus",
                table: "Product",
                column: "NameRus");

            migrationBuilder.AddForeignKey(
                name: "FK_ProductFiles_Product_ProductId",
                table: "ProductFiles",
                column: "ProductId",
                principalTable: "Product",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
