﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Backend.Data.Migrations
{
    public partial class _0 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Category",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ParentId = table.Column<int>(nullable: true),
                    Index = table.Column<string>(maxLength: 55, nullable: true),
                    Name = table.Column<string>(maxLength: 55, nullable: true),
                    NameRus = table.Column<string>(maxLength: 55, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Category", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Category_Category_ParentId",
                        column: x => x.ParentId,
                        principalTable: "Category",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "GeoCountry",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    Region = table.Column<string>(maxLength: 50, nullable: true),
                    District = table.Column<string>(maxLength: 50, nullable: true),
                    Latitude = table.Column<decimal>(type: "decimal(10,6)", nullable: false),
                    Longitude = table.Column<decimal>(type: "decimal(10,6)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GeoCountry", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Item",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Type = table.Column<sbyte>(type: "tinyint", nullable: false),
                    Name = table.Column<string>(maxLength: 55, nullable: true),
                    NameRus = table.Column<string>(maxLength: 55, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Item", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ProductPartCategory",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 55, nullable: true),
                    NameRus = table.Column<string>(maxLength: 55, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductPartCategory", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Site",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Domain = table.Column<string>(maxLength: 255, nullable: true),
                    Allowed = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Site", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Login = table.Column<string>(maxLength: 20, nullable: true),
                    Password = table.Column<string>(maxLength: 84, nullable: true),
                    RolesJson = table.Column<string>(maxLength: 26, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Product",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CategoryId = table.Column<int>(nullable: false),
                    Index = table.Column<string>(maxLength: 55, nullable: true),
                    Name = table.Column<string>(maxLength: 55, nullable: true),
                    NameRus = table.Column<string>(maxLength: 55, nullable: true),
                    Description = table.Column<string>(maxLength: 255, nullable: true),
                    DescriptionRus = table.Column<string>(maxLength: 255, nullable: true),
                    Price = table.Column<decimal>(type: "decimal(8,2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Product", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Product_Category_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Category",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProductPartSequence",
                columns: table => new
                {
                    CategoryId = table.Column<int>(nullable: false),
                    CategoriesJson = table.Column<string>(type: "text", nullable: true),
                    DictionaryJson = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductPartSequence", x => x.CategoryId);
                    table.ForeignKey(
                        name: "FK_ProductPartSequence_Category_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Category",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "GeoIp",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    StartBlock = table.Column<uint>(type: "int unsigned", nullable: false),
                    EndBlock = table.Column<uint>(type: "int unsigned", nullable: false),
                    StartIp = table.Column<string>(maxLength: 15, nullable: true),
                    EndIp = table.Column<string>(maxLength: 15, nullable: true),
                    Country = table.Column<string>(maxLength: 2, nullable: true),
                    GeoCountryId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GeoIp", x => x.Id);
                    table.ForeignKey(
                        name: "FK_GeoIp_GeoCountry_GeoCountryId",
                        column: x => x.GeoCountryId,
                        principalTable: "GeoCountry",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ItemBoolean",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ItemId = table.Column<int>(nullable: false),
                    Value = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ItemBoolean", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ItemBoolean_Item_ItemId",
                        column: x => x.ItemId,
                        principalTable: "Item",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ItemDate",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ItemId = table.Column<int>(nullable: false),
                    Value = table.Column<DateTime>(type: "date", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ItemDate", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ItemDate_Item_ItemId",
                        column: x => x.ItemId,
                        principalTable: "Item",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ItemDecimal",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ItemId = table.Column<int>(nullable: false),
                    Value = table.Column<decimal>(type: "decimal(8,2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ItemDecimal", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ItemDecimal_Item_ItemId",
                        column: x => x.ItemId,
                        principalTable: "Item",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ItemEnum",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ItemId = table.Column<int>(nullable: false),
                    Value = table.Column<string>(maxLength: 55, nullable: true),
                    ValueRus = table.Column<string>(maxLength: 55, nullable: true),
                    Order = table.Column<short>(type: "smallint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ItemEnum", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ItemEnum_Item_ItemId",
                        column: x => x.ItemId,
                        principalTable: "Item",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ItemInteger",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ItemId = table.Column<int>(nullable: false),
                    Value = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ItemInteger", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ItemInteger_Item_ItemId",
                        column: x => x.ItemId,
                        principalTable: "Item",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ItemString",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ItemId = table.Column<int>(nullable: false),
                    Value = table.Column<string>(maxLength: 1000, nullable: true),
                    ValueRus = table.Column<string>(maxLength: 1000, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ItemString", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ItemString_Item_ItemId",
                        column: x => x.ItemId,
                        principalTable: "Item",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserToken",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<int>(nullable: false),
                    Type = table.Column<byte>(type: "tinyint unsigned", nullable: false),
                    Value = table.Column<string>(maxLength: 128, nullable: true),
                    IpAddress = table.Column<string>(maxLength: 15, nullable: true),
                    ExpiresAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserToken", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserToken_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProductDigi",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    ProductId = table.Column<int>(nullable: false),
                    Allowed = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(maxLength: 55, nullable: true),
                    NameChanged = table.Column<bool>(nullable: false),
                    AgencyFee = table.Column<byte>(type: "tinyint unsigned", nullable: false),
                    AgencyFeeChanged = table.Column<bool>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductDigi", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductDigi_Product_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Product",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProductFiles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ProductId = table.Column<int>(nullable: false),
                    Type = table.Column<byte>(type: "tinyint unsigned", nullable: false),
                    Url = table.Column<string>(maxLength: 255, nullable: true),
                    Order = table.Column<short>(type: "smallint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductFiles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductFiles_Product_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Product",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProductRating",
                columns: table => new
                {
                    ProductId = table.Column<int>(nullable: false),
                    ReceivedUSD = table.Column<decimal>(type: "decimal(18,9)", nullable: false),
                    SalesCount = table.Column<ushort>(type: "smallint unsigned", nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductRating", x => x.ProductId);
                    table.ForeignKey(
                        name: "FK_ProductRating_Product_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Product",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProductPartBoolean",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ProductId = table.Column<int>(nullable: false),
                    CategoryId = table.Column<int>(nullable: false),
                    ItemValueId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductPartBoolean", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductPartBoolean_ProductPartCategory_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "ProductPartCategory",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductPartBoolean_ItemBoolean_ItemValueId",
                        column: x => x.ItemValueId,
                        principalTable: "ItemBoolean",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductPartBoolean_Product_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Product",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProductPartDate",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ProductId = table.Column<int>(nullable: false),
                    CategoryId = table.Column<int>(nullable: false),
                    ItemValueId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductPartDate", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductPartDate_ProductPartCategory_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "ProductPartCategory",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductPartDate_ItemDate_ItemValueId",
                        column: x => x.ItemValueId,
                        principalTable: "ItemDate",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductPartDate_Product_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Product",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProductPartDecimal",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ProductId = table.Column<int>(nullable: false),
                    CategoryId = table.Column<int>(nullable: false),
                    ItemValueId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductPartDecimal", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductPartDecimal_ProductPartCategory_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "ProductPartCategory",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductPartDecimal_ItemDecimal_ItemValueId",
                        column: x => x.ItemValueId,
                        principalTable: "ItemDecimal",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductPartDecimal_Product_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Product",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProductPartEnum",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ProductId = table.Column<int>(nullable: false),
                    CategoryId = table.Column<int>(nullable: false),
                    ItemValueId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductPartEnum", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductPartEnum_ProductPartCategory_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "ProductPartCategory",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductPartEnum_ItemEnum_ItemValueId",
                        column: x => x.ItemValueId,
                        principalTable: "ItemEnum",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductPartEnum_Product_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Product",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProductPartInteger",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ProductId = table.Column<int>(nullable: false),
                    CategoryId = table.Column<int>(nullable: false),
                    ItemValueId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductPartInteger", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductPartInteger_ProductPartCategory_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "ProductPartCategory",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductPartInteger_ItemInteger_ItemValueId",
                        column: x => x.ItemValueId,
                        principalTable: "ItemInteger",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductPartInteger_Product_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Product",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProductPartString",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ProductId = table.Column<int>(nullable: false),
                    CategoryId = table.Column<int>(nullable: false),
                    ItemValueId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductPartString", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductPartString_ProductPartCategory_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "ProductPartCategory",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductPartString_ItemString_ItemValueId",
                        column: x => x.ItemValueId,
                        principalTable: "ItemString",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductPartString_Product_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Product",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Category_Index",
                table: "Category",
                column: "Index",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Category_ParentId",
                table: "Category",
                column: "ParentId");

            migrationBuilder.CreateIndex(
                name: "IX_GeoIp_EndBlock",
                table: "GeoIp",
                column: "EndBlock");

            migrationBuilder.CreateIndex(
                name: "IX_GeoIp_GeoCountryId",
                table: "GeoIp",
                column: "GeoCountryId");

            migrationBuilder.CreateIndex(
                name: "IX_GeoIp_StartBlock",
                table: "GeoIp",
                column: "StartBlock");

            migrationBuilder.CreateIndex(
                name: "IX_Item_Type",
                table: "Item",
                column: "Type");

            migrationBuilder.CreateIndex(
                name: "IX_ItemBoolean_ItemId",
                table: "ItemBoolean",
                column: "ItemId");

            migrationBuilder.CreateIndex(
                name: "IX_ItemDate_ItemId",
                table: "ItemDate",
                column: "ItemId");

            migrationBuilder.CreateIndex(
                name: "IX_ItemDecimal_ItemId",
                table: "ItemDecimal",
                column: "ItemId");

            migrationBuilder.CreateIndex(
                name: "IX_ItemEnum_ItemId",
                table: "ItemEnum",
                column: "ItemId");

            migrationBuilder.CreateIndex(
                name: "IX_ItemInteger_ItemId",
                table: "ItemInteger",
                column: "ItemId");

            migrationBuilder.CreateIndex(
                name: "IX_ItemString_ItemId",
                table: "ItemString",
                column: "ItemId");

            migrationBuilder.CreateIndex(
                name: "IX_Product_CategoryId",
                table: "Product",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Product_Index",
                table: "Product",
                column: "Index",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Product_Name",
                table: "Product",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_Product_NameRus",
                table: "Product",
                column: "NameRus");

            migrationBuilder.CreateIndex(
                name: "IX_Product_Price",
                table: "Product",
                column: "Price");

            migrationBuilder.CreateIndex(
                name: "IX_ProductDigi_ProductId",
                table: "ProductDigi",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductDigi_Allowed_NameChanged_AgencyFeeChanged",
                table: "ProductDigi",
                columns: new[] { "Allowed", "NameChanged", "AgencyFeeChanged" });

            migrationBuilder.CreateIndex(
                name: "IX_ProductFiles_ProductId",
                table: "ProductFiles",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductPartBoolean_CategoryId",
                table: "ProductPartBoolean",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductPartBoolean_ItemValueId",
                table: "ProductPartBoolean",
                column: "ItemValueId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductPartBoolean_ProductId",
                table: "ProductPartBoolean",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductPartDate_CategoryId",
                table: "ProductPartDate",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductPartDate_ItemValueId",
                table: "ProductPartDate",
                column: "ItemValueId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductPartDate_ProductId",
                table: "ProductPartDate",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductPartDecimal_CategoryId",
                table: "ProductPartDecimal",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductPartDecimal_ItemValueId",
                table: "ProductPartDecimal",
                column: "ItemValueId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductPartDecimal_ProductId",
                table: "ProductPartDecimal",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductPartEnum_CategoryId",
                table: "ProductPartEnum",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductPartEnum_ItemValueId",
                table: "ProductPartEnum",
                column: "ItemValueId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductPartEnum_ProductId",
                table: "ProductPartEnum",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductPartInteger_CategoryId",
                table: "ProductPartInteger",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductPartInteger_ItemValueId",
                table: "ProductPartInteger",
                column: "ItemValueId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductPartInteger_ProductId",
                table: "ProductPartInteger",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductPartString_CategoryId",
                table: "ProductPartString",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductPartString_ItemValueId",
                table: "ProductPartString",
                column: "ItemValueId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductPartString_ProductId",
                table: "ProductPartString",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_Site_Domain",
                table: "Site",
                column: "Domain",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_User_Login",
                table: "User",
                column: "Login",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserToken_UserId",
                table: "UserToken",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GeoIp");

            migrationBuilder.DropTable(
                name: "ProductDigi");

            migrationBuilder.DropTable(
                name: "ProductFiles");

            migrationBuilder.DropTable(
                name: "ProductPartBoolean");

            migrationBuilder.DropTable(
                name: "ProductPartDate");

            migrationBuilder.DropTable(
                name: "ProductPartDecimal");

            migrationBuilder.DropTable(
                name: "ProductPartEnum");

            migrationBuilder.DropTable(
                name: "ProductPartInteger");

            migrationBuilder.DropTable(
                name: "ProductPartSequence");

            migrationBuilder.DropTable(
                name: "ProductPartString");

            migrationBuilder.DropTable(
                name: "ProductRating");

            migrationBuilder.DropTable(
                name: "Site");

            migrationBuilder.DropTable(
                name: "UserToken");

            migrationBuilder.DropTable(
                name: "GeoCountry");

            migrationBuilder.DropTable(
                name: "ItemBoolean");

            migrationBuilder.DropTable(
                name: "ItemDate");

            migrationBuilder.DropTable(
                name: "ItemDecimal");

            migrationBuilder.DropTable(
                name: "ItemEnum");

            migrationBuilder.DropTable(
                name: "ItemInteger");

            migrationBuilder.DropTable(
                name: "ProductPartCategory");

            migrationBuilder.DropTable(
                name: "ItemString");

            migrationBuilder.DropTable(
                name: "Product");

            migrationBuilder.DropTable(
                name: "User");

            migrationBuilder.DropTable(
                name: "Item");

            migrationBuilder.DropTable(
                name: "Category");
        }
    }
}
