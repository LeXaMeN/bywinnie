﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleApp
{
    class Program
    {
        public class Item
        {   
            public decimal Price;
            public decimal Bonus;
        }

        public static void Main()
        {
            var items = new List<Item>(new[] {
                //new Item() { Price = 13.62m, Bonus = 0.54m }, // 7
                //new Item() { Price = 12.09m, Bonus = 0.6m }, // 6
                //new Item() { Price = 12.56m, Bonus = 1.26m }, // 1
                //new Item() { Price = 12.09m, Bonus = 1.21m }, // 2
                //new Item() { Price = 6m, Bonus = 0.5m }, // 5
                //new Item() { Price = 18m, Bonus = 1m }, // 4
                //new Item() { Price = 18m, Bonus = 2m }, // 3
                //new Item() { Price = 24m, Bonus = 2m } // 8
                new Item() { Price = 14.61m, Bonus = 1.24m }, // 7
                new Item() { Price = 13.62m, Bonus = 0.54m }, // 7
                new Item() { Price = 13.62m, Bonus = 0.64m }, // 7
                new Item() { Price = 13.62m, Bonus = 0.74m }, // 7
                new Item() { Price = 13.62m, Bonus = 0.84m }, // 7
                new Item() { Price = 13.62m, Bonus = 0.94m }, // 7
                new Item() { Price = 13.62m, Bonus = 1.04m }, // 7
                new Item() { Price = 13.62m, Bonus = 1.14m }, // 7
                new Item() { Price = 12.62m, Bonus = 1.14m }, // 7
            });

            var maxP = items.Max(i => i.Price);
            var minP = items.Min(i => i.Price);
            var medP = items.Sum(i => i.Price) / items.Count;

            var maxB = items.Max(i => i.Bonus);
            var minB = items.Min(i => i.Bonus);
            var medB = items.Sum(i => i.Bonus) / items.Count;

            foreach (var item in items)
            {
                Console.Write($"Price: {item.Price.ToString("00.00")}$ | Bonus: {item.Bonus.ToString("0.00")}$ | ");
                decimal points, pricePoints, bonusPoints;
                // если цена на 50% больше от средней
                var bigPrice = item.Price > medP;
                var bigPriceCoefP = bigPrice ? (0.9m + (0.1m * (medP / item.Price))) : 1m;
                var bigPriceCoefB = bigPrice ? (0.5m + (0.5m * (medP / item.Price))) : 1m;

                // чем выше, тем лучше для покупателя
                pricePoints = medP / item.Price * bigPriceCoefP;
                // чем выше, тем лучше для партнёра
                bonusPoints = item.Bonus / medB * bigPriceCoefB;

                points = pricePoints * bonusPoints;

                Console.WriteLine($"Points ({pricePoints.ToString("0.0000")}*{bonusPoints.ToString("0.0000")}): " + points.ToString("0.0000"));
            }


            Console.WriteLine($"Sum ({minP}-{maxP}): {medP}");
            Console.ReadKey();
        }
    }
}
